/**
 *	Connection oriented routing user space utils
 *	Copyright (C) 20
 *	Authors:
 * 		Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#include <errno.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <signal.h>


#include "../cor.h"

#define U32_MAX ((__u32) ((1LL << 32) - 1))

#define likely(x) __builtin_expect((x), 1)
#define unlikely(x) __builtin_expect((x), 0)

struct{
	int af;
	union{
		struct sockaddr_in addr_in;
		struct cor_sockaddr addr_cor;
	}bind_addr;

	__u32 blocksize;
}config;

static int send_fully(int fd, char *buf, size_t len, __u8 msg_more)
{
	size_t sent_total = 0;
	while (sent_total < len) {
		ssize_t sent = send(fd, buf + sent_total, len - sent_total,
				msg_more == 1 ? MSG_MORE : 0);

		if (unlikely(sent < 0)) {
			if (errno == EINTR) {
				continue;
			} else if (errno != ECONNRESET) {
				perror("send");
			}
			return 1;
		}

		sent_total += sent;
	}

	return 0;
}

static int disable_powersave(char *filename, __s32 value)
{
	int fd = open(filename, O_WRONLY);

 	if (fd < 0) {
		fprintf(stderr, "Warning: cannot open %s: ", filename);
		perror("");
		return fd;
	}

	write(fd, &value, sizeof(value));

	return fd;
}

static void echoloop(int fd)
{
	int fd_ps1 = disable_powersave("/dev/cpu_dma_latency", 0);
	int fd_ps2 = disable_powersave("/dev/network_latency", 0);

	__u32 blocksize = config.blocksize;

	char *buf = malloc(blocksize);
	bzero(buf, blocksize);
	__u8 msg_more = 0;

	while (1) {
		ssize_t rcvd = recv(fd, buf + msg_more, blocksize - msg_more,
				msg_more == 1 ? MSG_DONTWAIT : 0);

		if (unlikely(rcvd <= 0)) {
			if (rcvd == 0) {
				break;
			} else if (errno == EAGAIN || errno == EWOULDBLOCK) {
				rcvd = 0;
			} else if (errno == EINTR) {
				continue;
			} else if (errno == ECONNRESET) {
				break;
			} else {
				perror("recv");
				break;
			}
		}

		rcvd += msg_more;
		msg_more = rcvd > 1 ? 1 : 0;
		if (unlikely(send_fully(fd, buf, (size_t) rcvd - msg_more,
				msg_more) != 0))
			break;

		if (msg_more == 1)
			buf[0] = buf[rcvd-1];
	}

	if (fd_ps1 >= 0)
		close(fd_ps1);

	if (fd_ps2 >= 0)
		close(fd_ps2);
}

#warning todo move into utils file
static int parse_u32(char *arg, __u32 *ret)
{
	long long tmpret = 0;

	tmpret = strtoll(arg, 0, 10);
	if (tmpret < 0 || tmpret > U32_MAX)
		return 1;

	*ret = (__u32) tmpret;
	return 0;
}

static int parse_config(int argc, char *argv[])
{
	int bindaddr_found = 0;

	__u32 argsconsumed = 0;

	bzero(&config, sizeof(config));
	config.blocksize = 512;

	if (argc > 10000)
		goto usage;

	while (1) {
		if (argc <= argsconsumed + 1)
			break;

		if (strcmp(argv[argsconsumed + 1], "--ip4") == 0) {
			config.af = AF_INET;
			config.bind_addr.addr_in.sin_family = AF_INET;
			config.bind_addr.addr_in.sin_port = htons(7);

			bindaddr_found = 1;

			argsconsumed += 1;
		} else if (strcmp(argv[argsconsumed + 1],
				"--cor") == 0) {
			config.af = AF_COR;
			config.bind_addr.addr_cor.sin_family = AF_COR;
			config.bind_addr.addr_in.sin_port = htonl(7);

			bindaddr_found = 1;

			argsconsumed += 1;
		} else if (strcmp(argv[argsconsumed + 1],
				"--bs") == 0) {
			if (argc <= argsconsumed + 2)
				goto usage;

			if (parse_u32(argv[argsconsumed + 2],
					&(config.blocksize)) != 0)
				goto usage;

			if (config.blocksize < 1 || config.blocksize > 1048576)
				goto usage;

			config.blocksize += 1;

			argsconsumed += 2;
		} else {
			goto usage;
		}
	}

	if (bindaddr_found == 0)
		goto usage;

	return 0;

usage:
	fprintf(stderr, "usage: test_echo\n"
			"\t--ip4 or --cor\n"
			"[--bs num] (blocksize, default = 512)\n");

	return 1;
}

int main(int argc, char *argv[])
{
	int fd, rc;

	int optval;

	parse_config(argc, argv);

	signal(SIGCHLD,SIG_IGN);

	fd = socket(config.af, SOCK_STREAM, 0);

	if (fd < 0) {
		perror("socket");
		goto early_out;
	}

	optval = 1;
	if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &optval,
			sizeof(optval)) != 0) {
		perror("setsockopt");
		goto out;
	}

	if (config.af == AF_INET)
		rc = bind(fd, (struct sockaddr *) &(config.bind_addr.addr_in),
				sizeof(config.bind_addr.addr_in));
	else if (config.af == AF_COR)
		rc = bind(fd, (struct sockaddr *) &(config.bind_addr.addr_cor),
				sizeof(config.bind_addr.addr_cor));
	else
		rc = 1;

	if (rc < 0) {
		perror("bind");
		goto out;
	}

	rc = listen(fd, 100);
	if (rc < 0) {
                perror("listen");
                goto out;
        }

	while (1) {
		int fd2 = accept(fd, 0, 0);

		if (fd2 == -1) {
			if (errno == EINTR) {
				continue;
			}
			perror("accept");
			goto out;
		}

		if (config.af == AF_INET) {
			optval = 1;
			if (setsockopt(fd2, IPPROTO_TCP, TCP_NODELAY, &optval,
					sizeof(optval)) != 0) {
				perror("setsockopt");
				goto out;
			}
		}


		rc = fork();
		if (rc == -1) {
			perror("fork");
			goto out;
		} else if (rc == 0) {
			//child process
			close(fd);
			echoloop(fd2);
			close(fd2);
			return 0;
		}
	}

out:
	close(fd);
early_out:

	return 0;
}


