/**
 *	Connection oriented routing user space utils
 *	Copyright (C) 20
 *	Authors:
 * 		Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#include <stdlib.h>
#include <arpa/inet.h>
#include <linux/types.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/timerfd.h>
#include <sys/epoll.h>
#include <assert.h>
#include <netinet/tcp.h>

#include "../utils.h"
#include "../cor.h"

#define EPOLL_EVENTS 512
#define PINGS_PER_SEC 10

#define U32_MAX ((__u32) ((1LL << 32) - 1))


#define NONBLOCK_BUF_SIZE 16384

struct nonblock_recvstate{
	char buf[NONBLOCK_BUF_SIZE];
	__u32 rcvd;
};

struct nonblock_sendstate{
	char buf[NONBLOCK_BUF_SIZE];
	__u32 sent;
	__u8 send_inprogress;
};


#define EPOLLDATA_TIMER 1
#define EPOLLDATA_TEST_LATENCY 2
#define EPOLLDATA_TEST_BANDWIDTH 3

struct epoll_privdata{
	int fd;

	__u8 type;
	union{
		struct{
			struct nonblock_recvstate recvstate;

			struct nonblock_sendstate sendstate;

			__u8 send_ping_needed;

			__u32 last_pong_rcvd;

			__u32 pongs_rcvd;
			__u64 pings_latency_sum;
			__u64 pings_latency_worst;

			__u64 bytes_rcvd;
		}test;
	}data;
};

struct{
	int af;
	union{
		struct sockaddr_in addr_in;
		struct cor_sockaddr addr_cor;
	}addr;

	__u32 conns_latency;
	__u32 conns_bandwidth;
	__u32 blocksize;
}config;

struct epoll_privdata **allsocks = 0;
__u32 allsocks_cnt = 0;


__u8 time_offset_set = 0;
__u64 time_offset_ns;

__u64 total_timer_expirations = 0;
__u64 total_timer_expirations_last_print = 0;

__u32 stats_prints_since_last_header = U32_MAX;


#define TIME_NET_LEN 4


#define RC_OK 0
#define RC_CONNBROKEN 1
#define RC_WOULDBLOCK 2

static __u32 get_time_usec(void)
{
	__u64 time_in_ns;
	struct timespec ts;

	clock_gettime(CLOCK_BOOTTIME, &ts);

	time_in_ns = 1000000000L * ((__u64) ts.tv_sec) + ((__u64) ts.tv_nsec);

	if (time_offset_set == 0) {
		time_offset_ns = time_in_ns;
		time_offset_set = 1;
	}

	if (time_in_ns < time_offset_ns) {
		fprintf(stderr, "CLOCK_BOOTTIME went backwards!?");
		exit(1);
	}

	return (__u32) ((time_in_ns - time_offset_ns)/1000);
}

static void set_nonblock(int fd, int value)
{
	int flags;

	flags = fcntl(fd, F_GETFL, 0);
    	if (flags == -1) {
		perror("set_nonblock F_GETFL");
		exit(1);
	}

	flags = (flags & (~(O_NONBLOCK)));
	if (value)
		flags = flags | O_NONBLOCK;

    	if (fcntl(fd, F_SETFL, flags) == -1) {
		perror("set_nonblock F_SETFL");
		exit(1);
	}
}

static int nonblock_recv(int fd, struct nonblock_recvstate *recvstate,
		__u32 rcvlen)
{
	ASSERT(sizeof(recvstate->buf) == NONBLOCK_BUF_SIZE);
	ASSERT(recvstate->rcvd <= NONBLOCK_BUF_SIZE);
	ASSERT(recvstate->rcvd <= rcvlen);

	likely(recvstate->rcvd < rcvlen);
	while (recvstate->rcvd < rcvlen) {
		int rcvd = recv(fd, recvstate->buf + recvstate->rcvd,
				rcvlen - recvstate->rcvd, 0);

		if (rcvd < 0 && (errno == EAGAIN ||
				errno == EWOULDBLOCK))
			return RC_WOULDBLOCK;

		if (unlikely(rcvd <= 0)) {
			if (unlikely(errno == EINTR))
				continue;

			perror("recv");
			return RC_CONNBROKEN;
		}

		recvstate->rcvd += (__u32) rcvd;
	}

	return RC_OK;
}

static void reset_nonblock_recvstate(struct nonblock_recvstate *recvstate)
{
	//bzero(recvstate, sizeof(struct nonblock_recvstate));
	recvstate->rcvd = 0;
}

static int nonblock_send(int fd, struct nonblock_sendstate *sendstate,
		__u32 sndlen, __u8 msg_more)
{
	if (unlikely(sendstate->send_inprogress == 0))
		return RC_OK;

	ASSERT(sizeof(sendstate->buf) == NONBLOCK_BUF_SIZE);
	ASSERT(sendstate->sent <= NONBLOCK_BUF_SIZE);
	ASSERT(sendstate->sent <= sndlen);

	while (sendstate->sent < sndlen) {
		int sent = send(fd, sendstate->buf + sendstate->sent,
				sndlen - sendstate->sent,
				msg_more == 1 ? MSG_MORE : 0);

		if (sent < 0 && (errno == EAGAIN ||
				errno == EWOULDBLOCK)) {
			//printf("send returned wouldblock\n");
			return RC_WOULDBLOCK;
		}

		if (unlikely(sent <= 0)) {
			if (unlikely(errno == EINTR))
				continue;

			perror("send");
			return RC_CONNBROKEN;
		}

		sendstate->sent += (__u32) sent;
	}
	sendstate->send_inprogress = 0;

	return RC_OK;
}

static void measure_latency(struct epoll_privdata *epd)
{
	__u32 time_now;
	__u32 time_sent;
	__u32 latency;

	ASSERT(epd->type == EPOLLDATA_TEST_LATENCY ||
			likely(epd->type == EPOLLDATA_TEST_BANDWIDTH));

	ASSERT(TIME_NET_LEN == 4);
	ASSERT(epd->data.test.recvstate.rcvd >= 4);


	memcpy((char *) &time_sent, &(epd->data.test.recvstate.buf[0]), 4);
	time_sent = ntohl(time_sent);

	if (likely(likely(epd->type == EPOLLDATA_TEST_BANDWIDTH) &&
			likely(time_sent == 0)))
		return;

	time_now = get_time_usec();
	if (unlikely(time_sent > time_now)) {
		printf("error: negative latency measured\n");
		printf("This probably means you are not connecting to an echo "
				"server.\n");
		exit(1);
		return;
	}

	latency = time_now - time_sent;

	epd->data.test.last_pong_rcvd = time_now;
	epd->data.test.pongs_rcvd++;
	epd->data.test.pings_latency_sum += latency;
	if (latency > epd->data.test.pings_latency_worst) {
		epd->data.test.pings_latency_worst = latency;
	}
}

static int proc_testfd(struct epoll_privdata *epd, __u32 eventflags)
{
	__u32 len;
	__u32 chunks_rcvd = 0;
	__u32 chunks_sent = 0;
	__u8 msg_more = likely(epd->type == EPOLLDATA_TEST_BANDWIDTH) ? 1 : 0;


	ASSERT(epd->type == EPOLLDATA_TEST_LATENCY ||
			likely(epd->type == EPOLLDATA_TEST_BANDWIDTH));

	if (epd->type == EPOLLDATA_TEST_LATENCY)
		len = TIME_NET_LEN;
	else if (likely(epd->type == EPOLLDATA_TEST_BANDWIDTH))
		len = config.blocksize;
	else
		ASSERT_ERR();

	while ((eventflags & EPOLLIN) != 0) {
		__u32 rcvd_old = epd->data.test.recvstate.rcvd;

		int rc = nonblock_recv(epd->fd, &(epd->data.test.recvstate),
				len);

		if (unlikely(rc == RC_CONNBROKEN))
			return 1;
		else if (rc == RC_WOULDBLOCK)
			eventflags = eventflags & (~EPOLLIN);
		else if (unlikely(rc != RC_OK))
			ASSERT_ERR();

		ASSERT(epd->data.test.recvstate.rcvd >= rcvd_old);
		epd->data.test.bytes_rcvd +=
				epd->data.test.recvstate.rcvd - rcvd_old;

		/* do not duplicate function calls (inlining) */
		if ((unlikely(epd->type == EPOLLDATA_TEST_LATENCY) &&
				epd->data.test.recvstate.rcvd == TIME_NET_LEN
				) || (
				likely(epd->type == EPOLLDATA_TEST_BANDWIDTH) &&
				rcvd_old < TIME_NET_LEN &&
				epd->data.test.recvstate.rcvd >= TIME_NET_LEN))
			measure_latency(epd);

		if (unlikely(unlikely(epd->type == EPOLLDATA_TEST_LATENCY) &&
				epd->data.test.recvstate.rcvd == TIME_NET_LEN))
			reset_nonblock_recvstate(&(epd->data.test.recvstate));

		if (likely(epd->type == EPOLLDATA_TEST_BANDWIDTH) &&
				epd->data.test.recvstate.rcvd ==
				config.blocksize) {
			reset_nonblock_recvstate(&(epd->data.test.recvstate));

			chunks_rcvd++;
			if (unlikely(chunks_rcvd >= 16))
				break;
		}
	}

	while ((eventflags & EPOLLOUT) != 0) {
		int rc;

		if (epd->data.test.sendstate.send_inprogress == 0) {
			__u32 time_sent;

			if (unlikely(epd->type == EPOLLDATA_TEST_LATENCY &&
					epd->data.test.send_ping_needed == 0))
				break;

			chunks_sent++;
			if (likely(epd->type ==
					EPOLLDATA_TEST_BANDWIDTH) &&
					chunks_sent >= 8)
				break;

			if (likely(epd->data.test.send_ping_needed == 0))
				time_sent = htonl(0);
			else
				time_sent = htonl(get_time_usec());

			ASSERT(TIME_NET_LEN == 4);
			memcpy(&(epd->data.test.sendstate.buf[0]),
					(char *) &time_sent, 4);

			epd->data.test.sendstate.sent = 0;
			epd->data.test.sendstate.send_inprogress = 1;
			epd->data.test.send_ping_needed = 0;
		}

		rc = nonblock_send(epd->fd, &(epd->data.test.sendstate), len,
				msg_more);

		if (unlikely(rc == RC_CONNBROKEN))
			return 1;
		else if (rc == RC_WOULDBLOCK)
			eventflags = eventflags & (~EPOLLOUT);
		else if (unlikely(rc != RC_OK))
			ASSERT_ERR();
	}

	return 0;
}

static char *get_filler_spaces(__u64 number, __u32 len)
{
	__u32 number_len = 1;
	while (number >= 10) {
		number_len++;
		number = number /10;
	}

	if (number_len >= len)
		return "";
	else if (number_len + 1 == len)
		return " ";
	else if (number_len + 2 == len)
		return "  ";
	else if (number_len + 3 == len)
		return "   ";
	else if (number_len + 4 == len)
		return "    ";
	else if (number_len + 5 == len)
		return "     ";
	else if (number_len + 6 == len)
		return "      ";
	else if (number_len + 7 == len)
		return "       ";
	else
		return "        ";
}

static void print_stats(void)
{
	__u32 time_now = get_time_usec();

	__u32 latency_pongs_rcvd = 0;
	__u64 latency_pings_latency_sum = 0;
	__u64 latency_pings_latency_worst = 0;
	__u64 latency_pings_latency_avg = 0;

	__u32 bandwidth_conns = 0;
	__u64 bandwidth_bytes_rcvd_sum = 0;
	__u64 bandwidth_bytes_rcvd_worst = 0;
	__u64 bandwidth_bytes_rcvd_avg = 0;
	__u32 bandwidth_pongs_rcvd = 0;
	__u64 bandwidth_pings_latency_sum = 0;
	__u64 bandwidth_pings_latency_worst = 0;
	__u64 bandwidth_pings_latency_avg = 0;

	__u32 i;
	for (i=0;i<allsocks_cnt;i++) {
		struct epoll_privdata *epd = allsocks[i];

		__u64 latency_worst = 0;
		__u32 time_since_last_ping;
		__u32 time_since_last_ping_tolerance;

		if (epd->type != EPOLLDATA_TEST_LATENCY &&
				epd->type != EPOLLDATA_TEST_BANDWIDTH)
			continue;

		time_since_last_ping = time_now - epd->data.test.last_pong_rcvd;
		time_since_last_ping_tolerance = 2 * 1000000 /
				(PINGS_PER_SEC > 10 ? 10 : PINGS_PER_SEC);
		if (time_since_last_ping > time_since_last_ping_tolerance)
			latency_worst = time_since_last_ping -
					time_since_last_ping_tolerance;
		if (epd->data.test.pings_latency_worst > latency_worst)
			latency_worst = epd->data.test.pings_latency_worst;

		if (epd->type == EPOLLDATA_TEST_LATENCY) {
			latency_pongs_rcvd += epd->data.test.pongs_rcvd;
			latency_pings_latency_sum +=
					epd->data.test.pings_latency_sum;
			if (latency_worst > latency_pings_latency_worst)
				latency_pings_latency_worst = latency_worst;

		} else if (epd->type == EPOLLDATA_TEST_BANDWIDTH) {
			bandwidth_bytes_rcvd_sum += epd->data.test.bytes_rcvd;
			if (bandwidth_conns == 0 ||
					epd->data.test.bytes_rcvd <
					bandwidth_bytes_rcvd_worst)
				bandwidth_bytes_rcvd_worst =
						epd->data.test.bytes_rcvd;

			bandwidth_pongs_rcvd += epd->data.test.pongs_rcvd;
			bandwidth_pings_latency_sum +=
					epd->data.test.pings_latency_sum;
			if (latency_worst > bandwidth_pings_latency_worst)
				bandwidth_pings_latency_worst = latency_worst;

			bandwidth_conns++;
		}

		epd->data.test.pongs_rcvd = 0;
		epd->data.test.pings_latency_sum = 0;
		epd->data.test.pings_latency_worst = 0;
		epd->data.test.bytes_rcvd = 0;
	}

	latency_pings_latency_avg = latency_pongs_rcvd == 0 ? 0 :
			(latency_pings_latency_sum + latency_pongs_rcvd/2) /
			latency_pongs_rcvd;

	bandwidth_pings_latency_avg = bandwidth_pongs_rcvd == 0 ? 0 :
			(bandwidth_pings_latency_sum + bandwidth_pongs_rcvd/2) /
			bandwidth_pongs_rcvd;

	bandwidth_bytes_rcvd_avg = bandwidth_conns == 0 ? 0 :
			(bandwidth_bytes_rcvd_sum + bandwidth_conns/2) /
			bandwidth_conns;

	if (stats_prints_since_last_header >= 15) {
		printf("+------+------+------+------+------+------+------+\n");
		printf("|   latency   |             bandwidth            |\n");
		printf("| latency(us) | latency(us) |  throughput(kb/s)  |\n");
		printf("|  avg | worst|  avg | worst|  avg | worst| total|\n");
		printf("+------+------+------+------+------+------+------+\n");

		stats_prints_since_last_header = 0;
	}

	printf("|%s%llu|%s%llu|%s%llu|%s%llu|%s%llu|%s%llu|%s%llu|\n",
			get_filler_spaces(latency_pings_latency_avg, 6),
			latency_pings_latency_avg,
			get_filler_spaces(latency_pings_latency_worst, 6),
			latency_pings_latency_worst,
			get_filler_spaces(bandwidth_pings_latency_avg, 6),
			bandwidth_pings_latency_avg,
			get_filler_spaces(bandwidth_pings_latency_worst, 6),
			bandwidth_pings_latency_worst,
			get_filler_spaces((bandwidth_bytes_rcvd_avg+512)/1024, 6),
			(bandwidth_bytes_rcvd_avg+512)/1024,
			get_filler_spaces((bandwidth_bytes_rcvd_worst+512)/1024, 6),
			(bandwidth_bytes_rcvd_worst+512)/1024,
			get_filler_spaces((bandwidth_bytes_rcvd_sum+512)/1024, 6),
			(bandwidth_bytes_rcvd_sum+512)/1024
			);

	stats_prints_since_last_header++;
}

static int send_pings(void)
{
	__u32 i;
	int rc;

	for (i=0;i<allsocks_cnt;i++) {
		struct epoll_privdata *epd = allsocks[i];

		if ((epd->type == EPOLLDATA_TEST_LATENCY ||
				epd->type == EPOLLDATA_TEST_BANDWIDTH) &&
				epd->data.test.send_ping_needed == 0) {
			epd->data.test.send_ping_needed = 1;
			rc = proc_testfd(epd, EPOLLOUT);
			if (rc != 0)
				return 1;
		}
	}

	return 0;
}

static int proc_timerfd(struct epoll_privdata *epd, __u32 eventflags)
{
	int rc;
	__u64 expirations;

	rc = read(epd->fd, (char *) &expirations, 8);
	if (rc != 8) {
		perror("timerfd read");
		return 1;
	}

	//printf("timerfd %llu %u\n", expirations, get_time_usec());

	rc = send_pings();

	total_timer_expirations += expirations;
	if (total_timer_expirations - total_timer_expirations_last_print >=
			PINGS_PER_SEC) {
		total_timer_expirations_last_print = total_timer_expirations;

		print_stats();
	}

	return rc;
}

static int proc_epollevent(struct epoll_privdata *epd, __u32 eventflags)
{
	if (epd->type == EPOLLDATA_TIMER) {
		return proc_timerfd(epd, eventflags);
	} else if (epd->type == EPOLLDATA_TEST_LATENCY ||
			likely(epd->type == EPOLLDATA_TEST_BANDWIDTH)) {
		return proc_testfd(epd, eventflags);
	} else {
		ASSERT(0);
	}

	return 0;
}


static void _disable_powersave(char *filename, __s32 value)
{
	int fd = open(filename, O_WRONLY);

 	if (fd < 0) {
		fprintf(stderr, "Warning: cannot open %s: ", filename);
		perror("");
		return;
	}

	write(fd, &value, sizeof(value));

	/* file descriptor needs to stay open */
}

static void disable_powersave(void)
{
	_disable_powersave("/dev/cpu_dma_latency", 0);
	_disable_powersave("/dev/network_latency", 0);
}

static int open_socket(void)
{
	int fd;
	int rc;

	fd = socket(config.af, SOCK_STREAM, 0);
	if (fd == -1) {
		perror("socket");
		return -1;
	}

	if (config.af == AF_INET)
		rc = connect(fd, (struct sockaddr *) &(config.addr.addr_in),
				sizeof(struct sockaddr_in));
	else if (config.af == AF_COR)
		rc = connect(fd, (struct sockaddr *) &(config.addr.addr_cor),
				sizeof(struct cor_sockaddr));
	else
		rc = 1;

	if (rc != 0) {
		perror("connect");
		close(fd);
		return -1;
	}

	if (config.af == AF_INET) {
		int optval = 1;
		if (setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, &optval,
				sizeof(optval)) != 0) {
			perror("setsockopt");
			return -1;
		}
	}

	set_nonblock(fd, 1);

	return fd;
}

static int open_sockets(int epoll_fd)
{
	__u32 i;

	allsocks_cnt = config.conns_latency + config.conns_bandwidth;

	allsocks = (struct epoll_privdata **) malloc(allsocks_cnt *
			sizeof(struct epoll_privdata *));

	bzero(allsocks, allsocks_cnt * sizeof(struct epoll_privdata *));

	for (i=0;i<allsocks_cnt;i++) {
		struct epoll_event epe;
		struct epoll_privdata *epd =
				malloc(sizeof(struct epoll_privdata));

		bzero(epd, sizeof(struct epoll_privdata));
		allsocks[i] = epd;

		epd->fd = open_socket();
		if (epd->fd == -1)
			return 1;

		if (i < config.conns_latency)
			epd->type = EPOLLDATA_TEST_LATENCY;
		else
			epd->type = EPOLLDATA_TEST_BANDWIDTH;

		bzero(&epe, sizeof(struct epoll_event));
		epe.events = EPOLLIN | EPOLLOUT;
		if (epd->type == EPOLLDATA_TEST_LATENCY)
			epe.events = epe.events | EPOLLET;
		epe.data.ptr = epd;

		epoll_ctl(epoll_fd, EPOLL_CTL_ADD, epd->fd, &epe);
	}

	return 0;
}

static int open_timerfd(int epoll_fd)
{
	int timerfd;
	int rc;

	struct itimerspec itimerspec;

	struct epoll_privdata *epd;
	struct epoll_event epe;

	timerfd = timerfd_create(CLOCK_BOOTTIME, TFD_NONBLOCK);
	if (timerfd <= 0) {
		perror("timerfd_create");
		return 1;
	}

	bzero(&itimerspec, sizeof(struct itimerspec));
	itimerspec.it_value.tv_nsec = 1000000000 / PINGS_PER_SEC;
	itimerspec.it_interval.tv_nsec = 1000000000 / PINGS_PER_SEC;

	rc = timerfd_settime(timerfd, 0, &itimerspec, 0);
	if (rc != 0) {
		perror("timerfd_settime");
		return 1;
	}

	epd = (struct epoll_privdata *) malloc(sizeof(struct epoll_privdata));
	bzero(epd, sizeof(struct epoll_privdata));
	epd->fd = timerfd;
	epd->type = EPOLLDATA_TIMER;

	bzero(&epe, sizeof(struct epoll_event));
	epe.events = EPOLLIN;
	epe.data.ptr = epd;

	epoll_ctl(epoll_fd, EPOLL_CTL_ADD, timerfd, &epe);

	return 0;
}

static int parse_addr_inet(char *arg)
{
	bzero(&config.addr.addr_in, sizeof(struct sockaddr_in));
	config.af = AF_INET;
	config.addr.addr_in.sin_family = AF_INET;
	config.addr.addr_in.sin_port = htons(7);
	if (inet_pton(AF_INET, arg, &(config.addr.addr_in.sin_addr)) != 1) {
		fprintf(stderr, "error: unable to parse addr: %s\n", arg);
		return 1;
	}

	return 0;
}

static int parse_addr_cor(char *arg)
{
	__be64 addr;

	if (parse_cor_addr(&addr, arg, 0) != 0)
		goto out_err;

	bzero(&config.addr.addr_cor, sizeof(struct cor_sockaddr));
	config.af = AF_COR;
	config.addr.addr_cor.sin_family = AF_COR;
	config.addr.addr_cor.port = htonl(7);
	config.addr.addr_cor.addr = htonll(addr);


	return 0;

out_err:
	fprintf(stderr, "error: unable to parse cor addr: %s\n", arg);
	fprintf(stderr, "A cor address must be an even number of hex digits."
			"\n");
	return 1;
}

#warning todo move into utils file
static int parse_u32(char *arg, __u32 *ret)
{
	long long tmpret = 0;

	tmpret = strtoll(arg, 0, 10);
	if (tmpret < 0 || tmpret > U32_MAX)
		return 1;

	*ret = (__u32) tmpret;
	return 0;
}

static int parse_config(int argc, char *argv[])
{
	int addr_found = 0;
	int numconns_found = 0;

	__u32 argsconsumed = 0;

	bzero(&config, sizeof(config));
	config.blocksize = 512;

	if (argc > 10000)
		goto usage;

	while (1) {
		if (argc <= argsconsumed + 1)
			break;

		if (strcmp(argv[argsconsumed + 1], "--ip4") == 0) {
			if (argc <= argsconsumed + 2)
				goto usage;

			if (parse_addr_inet(argv[argsconsumed + 2]) != 0)
				goto usage;

			addr_found = 1;

			argsconsumed += 2;
		} else if (strcmp(argv[argsconsumed + 1],
				"--cor") == 0) {
			if (argc <= argsconsumed + 2)
				goto usage;

			if (parse_addr_cor(argv[argsconsumed + 2]) != 0)
				goto usage;

			addr_found = 1;

			argsconsumed += 2;
		} else if (strcmp(argv[argsconsumed + 1],
				"--conns-latency") == 0) {
			if (argc <= argsconsumed + 2)
				goto usage;

			if (parse_u32(argv[argsconsumed + 2],
					&(config.conns_latency)) != 0)
				goto usage;

			numconns_found++;

			argsconsumed += 2;
		} else if (strcmp(argv[argsconsumed + 1],
				"--conns-bandwidth") == 0) {
			if (argc <= argsconsumed + 2)
				goto usage;

			if (parse_u32(argv[argsconsumed + 2],
					&(config.conns_bandwidth)) != 0)
				goto usage;

			numconns_found++;

			argsconsumed += 2;
		} else if (strcmp(argv[argsconsumed + 1],
				"--bs") == 0) {
			if (argc <= argsconsumed + 2)
				goto usage;

			if (parse_u32(argv[argsconsumed + 2],
					&(config.blocksize)) != 0)
				goto usage;

			if (config.blocksize < 4 || config.blocksize > 16384)
				goto usage;

			ASSERT(TIME_NET_LEN == 4);
			ASSERT(NONBLOCK_BUF_SIZE == 16384);

			argsconsumed += 2;
		} else {
			goto usage;
		}
	}

	if (addr_found == 0 || numconns_found == 0)
		goto usage;

	return 0;

usage:
	fprintf(stderr, "usage: test_perf\n"
			"\t--ip4 addr or --cor addr\n"
			"\t--conns-latency num and/or --conns-bandwidth num\n"
			"\t[--bs 4-16384] "
			"(blocksize in bytes for conns-bandwidth; "
			"\t\tlow values increase cpu load, "
			"\t\thigh values decrease measurement accuracy on slow "
			"\t\tconnections, default = 512)\n");

	return 1;
}

int main(int argc, char *argv[])
{
	int epoll_fd;
	int rc;

	struct itimerspec;

	rc = parse_config(argc, argv);
	if (rc != 0)
		goto out_err;


	epoll_fd = epoll_create(1);
	if (epoll_fd <= 0) {
		perror("epoll_create");
		goto out_err;
	}

	rc = open_timerfd(epoll_fd);
	if (rc != 0)
		goto out_err;

	rc = open_sockets(epoll_fd);
	if (rc != 0)
		goto out_err;

	disable_powersave();

	while (1) {
		struct epoll_event events[EPOLL_EVENTS];
		int rdycnt;
		int u;

		rdycnt = epoll_wait(epoll_fd, events, EPOLL_EVENTS, -1);

		for (u=0;u<rdycnt;u++) {
			struct epoll_privdata *epd = events[u].data.ptr;
			__u32 eventflags = events[u].events;
			rc = proc_epollevent(epd, eventflags);
			if (rc != 0) {
				goto out_err;
			}
		}
	}


	return 0;

out_err:
	return 1;
}
