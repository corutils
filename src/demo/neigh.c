/**
 *	Connection oriented routing user space utils
 *	Copyright (C) 2009-2011
 *	Authors:
 * 		Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#include <stdio.h>
#include <unistd.h>

#include "../utils.h"
#include "../cor.h"
#include "libcor.h"

void init_neighlist(void *ptr, __u32 numneighs)
{
	printf("numneighs = %u\n", numneighs);
}

void add_neigh(void *ptr, __be64 addr)
{
	printf("addr: %llx\n", ntohll(addr));
}

int main(void)
{
	int fd, rc;

	fd = socket(PF_COR, 0, 0);
	printf("socket\n");
	if (fd < 0) {
		perror("socket");
		goto early_out;
	}

	rc = connect(fd, 0, 0);
	printf("connect\n");
	if (rc != 0) {
		perror("connect");
		goto out;
	}

	rc = send_list_neigh(fd);
	printf("readresp %d\n", rc);
	rc = read_resp(fd, 1);

	printf("read_neigh_list %d\n", rc);

	rc = read_neigh_list(fd, 0, init_neighlist, add_neigh);

out:
	close(fd);

early_out:

	return 0;
}
