
/**
 *	Connection oriented routing user space utils
 *	Copyright (C) 2009-2011
 *	Authors:
 * 		Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>

#include "../cor.h"
#include "../utils.h"

int main(void)
{
	struct cor_sockaddr addr;

	int fd, rc;

	struct linger so_linger;

	bzero(&addr, sizeof(struct cor_sockaddr));

	addr.sin_family = AF_COR;

	addr.addr = htonll(1);
	addr.port = htonl(1);

	fd = socket(PF_COR, SOCK_STREAM, 0);
	printf("socket\n");
	if(fd < 0) {
		perror("socket");
		goto early_out;
	}

	rc = connect(fd, (struct sockaddr *) &addr,
			sizeof(struct cor_sockaddr));
	printf("connect\n");
	if(rc < 0) {
		perror("connect");
		goto out;
	}

	so_linger.l_onoff = 1;
	so_linger.l_linger = 30;
	rc = setsockopt(fd, SOL_SOCKET, SO_LINGER, &so_linger, sizeof so_linger);
	printf("setsockopt\n");
	if(rc < 0) {
		perror("setsockopt");
		goto out;
	}

	/* rc = shutdown(fd, SHUT_RDWR);
	printf("shutdown\n");
	if(rc < 0) {
		perror("shutdown");
		goto out;
	}

	sleep(10);*/

out:
	close(fd);

early_out:

	return 0;
}
