/**
 *	Connection oriented routing user space utils
 *	Copyright (C) 2009-2020
 *	Authors:
 * 		Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#include "routed.h"


static struct nonblock_resumeinfo *_init_fwd(int fd, __u8 is_connected)
{
	struct nonblock_resumeinfo *nr =
			malloc(sizeof(struct nonblock_resumeinfo));

	struct epoll_event epe;

	bzero(nr, sizeof(struct nonblock_resumeinfo));
	nr->fd = fd;
	nr->type = EPOLLDATA_FORWARD;
	nr->data.forward.buf = malloc(FORWARD_BUF_SIZE);
	bzero(nr->data.forward.buf, FORWARD_BUF_SIZE);
	nr->data.forward.is_connected = is_connected;
	bzero(&epe, sizeof(struct epoll_event));
	epe.events = (EPOLLIN | EPOLLOUT | EPOLLRDHUP | EPOLLERR | EPOLLET);
	epe.data.ptr = nr;

	epoll_ctl(epoll_fd, EPOLL_CTL_ADD, nr->fd, &epe);

	return nr;
}

static void init_fwd(int fd0, __u8 fd0connected, int fd1, __u8 fd1connected)
{
	struct nonblock_resumeinfo *dir0 = _init_fwd(fd0, fd0connected);
	struct nonblock_resumeinfo *dir1 = _init_fwd(fd1, fd1connected);

	dir0->data.forward.reversedir = dir1;
	dir1->data.forward.reversedir = dir0;
}

void fwd_to_r_accept(int listenerfd, __u8 fwd_to_host, __be64 hostaddr,
		__be32 targetport)
{
	while (1) {
		int localfd;
		struct node *node;
		struct cor_sockaddr addr;
		int remotefd;
		__u8 remote_fd_isconnected = 0;

		#warning todo remote_fd_isconnected = 1 bug

		localfd = accept(listenerfd, 0, 0);
		if (localfd < 0)
			return;

		set_nonblock(localfd, 1);

		#warning todo noaddr should disable localhost connectivity (other places too)

		if (fwd_to_host != 0) {
			if (hostaddr == 0 ||
					(has_localaddr != 0 &&
					hostaddr == localaddr)) {
				node = 0;
			} else if ((node = try_find_neigh_byaddr(hostaddr)) == 0) {
				if (verbosity >= 1)
					printf("fwd_to_r_accept host not found\n");
				goto failed;
			}
		} else {
			if (service_list_contains(&(localservices), targetport)
					!= 0) {
				node = 0;
			} else if ((node = try_find_neigh_byservice(targetport))
					== 0) {
				if (verbosity >= 1)
					printf("fwd_to_r_accept no host with service found\n");
				goto failed;
			}
		}

		bzero(&addr, sizeof(struct cor_sockaddr));
		addr.sin_family = AF_COR;
		addr.port = targetport;

		if (node == 0)
			addr.addr = 0;
		else
			addr.addr = node->addr;


		remotefd = socket(PF_COR, SOCK_STREAM, 0);
		if (remotefd == -1) {
			perror("socket");
			goto failed;
		}

		set_nonblock(remotefd, 1);

		if (connect(remotefd, (struct sockaddr *) &addr,
				sizeof(struct cor_sockaddr)) != 0) {
			if (errno == EINPROGRESS) {
				remote_fd_isconnected = 0;
			} else {
				close(remotefd);
				perror("connect");
				goto failed;
			}
		}

		init_fwd(localfd, 1, remotefd, remote_fd_isconnected);

		if (0) {
failed:
			close(localfd);
		}
	}
}

void fwd_to_lservice_accept(int listenerfd, int af,
		struct sockaddr *saddr, socklen_t saddrlen)
{
	while (1) {
		int localfd;
		int remotefd;
		__u8 remote_fd_isconnected = 1;

		localfd = accept(listenerfd, 0, 0);

		if (localfd < 0)
			return;

		set_nonblock(localfd, 1);

		remotefd = socket(af, SOCK_STREAM, 0);
		if (remotefd == -1) {
			perror("socket");
			goto failed;
		}

		set_nonblock(remotefd, 1);

		if (connect(remotefd, saddr, saddrlen) != 0) {
			if (errno == EINPROGRESS) {
				remote_fd_isconnected = 0;
			} else {
				perror("connect");
				close(remotefd);
				goto failed;
			}
		}

		init_fwd(localfd, 1, remotefd, remote_fd_isconnected);

		if (0) {
failed:
			close(localfd);
		}
	}
}



#define RC_FORWARD_OK 1
#define RC_FORWARD_WOULDBLOCK 2
#define RC_FORWARD_ERROR 3

static int forward_write(struct nonblock_resumeinfo *nr)
{
	__u32 buffill = nr->data.forward.buffill;
	int more = 0;

	ASSERT(buffill <= FORWARD_BUF_SIZE);
	ASSERT(nr->data.forward.bufwritten <= buffill);

	if (buffill > 1 && FORWARD_BUF_SIZE > 1) {
		buffill -= 1;
		more = 1;
	}

	while (nr->data.forward.bufwritten < buffill) {
		int rc = send(nr->fd, nr->data.forward.buf +
				nr->data.forward.bufwritten,
				buffill - nr->data.forward.bufwritten,
				more == 0 ? 0 : MSG_MORE);

		if (rc < 0 && (errno == EAGAIN ||
				errno == EWOULDBLOCK)) {
			return RC_FORWARD_WOULDBLOCK;
		} else if (rc <= 0) {
			if (errno == EINTR)
				continue;

			perror("forward_write");
			return RC_FORWARD_ERROR;
		} else {
			nr->data.forward.bufwritten += rc;
			ASSERT(nr->data.forward.bufwritten <= buffill);
		}
	}

	return RC_FORWARD_OK;
}

static int forward_read(struct nonblock_resumeinfo *nr)
{
	int readfd = nr->data.forward.reversedir->fd;
	int rc;

	ASSERT(nr->data.forward.bufwritten <= nr->data.forward.buffill);

	if (nr->data.forward.bufwritten == nr->data.forward.buffill) {
		nr->data.forward.buffill = 0;
		nr->data.forward.bufwritten = 0;
	} else if (nr->data.forward.bufwritten * 3 > nr->data.forward.buffill) {
		memmove(nr->data.forward.buf, nr->data.forward.buf +
				nr->data.forward.bufwritten,
				nr->data.forward.buffill -
				nr->data.forward.bufwritten);
		nr->data.forward.buffill -= nr->data.forward.bufwritten;
		nr->data.forward.bufwritten = 0;
	}

	if (nr->data.forward.buffill == FORWARD_BUF_SIZE)
		return RC_FORWARD_OK;

	while (nr->data.forward.buffill < FORWARD_BUF_SIZE) {
		rc = recv(readfd, nr->data.forward.buf +
				nr->data.forward.buffill,
				FORWARD_BUF_SIZE - nr->data.forward.buffill, 0);

		if (rc == 0) {
			#warning todo eof
			return RC_FORWARD_OK;
		} else if (rc < 0) {
			if (errno == EAGAIN || errno == EWOULDBLOCK) {
				return RC_FORWARD_WOULDBLOCK;
			} else if (errno == EINTR) {
				continue;
			} else {
				perror("forward_read");
				return RC_FORWARD_ERROR;
			}
		} else {
			nr->data.forward.buffill += rc;
			ASSERT(nr->data.forward.buffill <= FORWARD_BUF_SIZE);
		}
	}

	return RC_FORWARD_OK;
}

static void _forward(struct nonblock_resumeinfo *nr)
{
	while (1) {
		int rc;

		rc = forward_read(nr);
		if (rc == RC_FORWARD_ERROR)
			goto err;
		if (nr->data.forward.bufwritten == nr->data.forward.buffill)
			break;

		rc = forward_write(nr);
		if (rc == RC_FORWARD_ERROR)
			goto err;
		else if (rc == RC_FORWARD_WOULDBLOCK)
			return;
	}

	#warning todo call EPOLL_CTL_DEL + free where needed (some places calling close do not)
	if (0) {
err:
		epoll_ctl(epoll_fd, EPOLL_CTL_DEL,
				nr->data.forward.reversedir->fd, 0);
		close(nr->data.forward.reversedir->fd);
		epoll_ctl(epoll_fd, EPOLL_CTL_DEL, nr->fd, 0);
		close(nr->fd);

		#warning todo free after all events are processed
		/* free(nr->data.forward.reversedir);
		free(nr); */
	}

}

void forward(struct nonblock_resumeinfo *nr, __u32 eventflags)
{
	ASSERT(nr->type == EPOLLDATA_FORWARD);
	ASSERT(nr->data.forward.reversedir->type == EPOLLDATA_FORWARD);

	if ((eventflags & EPOLLOUT) != 0)
		nr->data.forward.is_connected = 1;

	if (nr->data.forward.is_connected == 0 ||
			nr->data.forward.reversedir->data.forward.is_connected
			== 0)
		return;

	_forward(nr);
	_forward(nr->data.forward.reversedir);
}

void epoll_add_fwd_to_r(struct list_head *r_fwds)
{
	while (list_empty(r_fwds) == 0) {
		struct fwd_to_r_item *item = container_of(r_fwds->next,
				struct fwd_to_r_item, allfwds);

		struct nonblock_resumeinfo *nr;

		struct epoll_event epe;

		set_nonblock(item->bindfd, 1);

		nr = malloc(sizeof(struct nonblock_resumeinfo));
		bzero(nr, sizeof(struct nonblock_resumeinfo));
		nr->fd = item->bindfd;
		nr->type = EPOLLDATA_FWD_TO_R;
		nr->data.fwd_to_r.fwd_to_host = item->fwd_to_host;
		nr->data.fwd_to_r.addr = item->addr;
		nr->data.fwd_to_r.targetport = item->targetport;
		bzero(&epe, sizeof(struct epoll_event));
		epe.events = (EPOLLIN | EPOLLERR);
		epe.data.ptr = nr;

		epoll_ctl(epoll_fd, EPOLL_CTL_ADD, item->bindfd, &epe);

		list_del(&(item->allfwds));
		free(item);
	}
}

void epoll_add_fwd_to_lservice(struct list_head *lservice_fwds)
{
	while (list_empty(lservice_fwds) == 0) {
		struct fwd_to_lservice_item *item = container_of(
				lservice_fwds->next,
				struct fwd_to_lservice_item,
				allfwds);

		struct nonblock_resumeinfo *nr;

		struct epoll_event epe;

		set_nonblock(item->bindfd, 1);

		nr = malloc(sizeof(struct nonblock_resumeinfo));
		bzero(nr, sizeof(struct nonblock_resumeinfo));
		nr->fd = item->bindfd;
		nr->type = EPOLLDATA_FWD_TO_LSERVICE;
		nr->data.fwd_to_lservice.af = item->af;
		nr->data.fwd_to_lservice.saddr = item->saddr;
		nr->data.fwd_to_lservice.saddrlen = item->saddrlen;
		bzero(&epe, sizeof(struct epoll_event));
		epe.events = (EPOLLIN | EPOLLERR);
		epe.data.ptr = nr;

		epoll_ctl(epoll_fd, EPOLL_CTL_ADD, item->bindfd, &epe);

		list_del(&(item->allfwds));
		free(item);
	}
}
