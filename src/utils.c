/**
 *	Connection oriented routing user space utils
 *	Copyright (C) 2009-2022
 *	Authors:
 * 		Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#include <string.h>

#include "utils.h"


static inline int parse_hexchar(char c)
{
	if (c == '0')
		return 0;
	else if (c == '1')
		return 1;
	else if (c == '2')
		return 2;
	else if (c == '3')
		return 3;
	else if (c == '4')
		return 4;
	else if (c == '5')
		return 5;
	else if (c == '6')
		return 6;
	else if (c == '7')
		return 7;
	else if (c == '8')
		return 8;
	else if (c == '9')
		return 9;
	else if (c == 'A' || c == 'a')
		return 10;
	else if (c == 'B' || c == 'b')
		return 11;
	else if (c == 'C' || c == 'c')
		return 12;
	else if (c == 'D' || c == 'd')
		return 13;
	else if (c == 'E' || c == 'e')
		return 14;
	else if (c == 'F' || c == 'f')
		return 15;
	else
		return -1;
}

static inline int parse_hex(char *target, char *src, int srclen)
{
	int u;

	ASSERT(src != 0);
	ASSERT(target != 0);
	ASSERT(srclen > 0);
	ASSERT((srclen%2) == 0);

	for(u=0;u<(srclen/2);u++) {
		int high = parse_hexchar(src[u*2]);
		int low = parse_hexchar(src[u*2+1]);

		if (high == -1 || low == -1)
			return -1;

		 target[u] = (char) ((high << 4) + low);
	}

	return 0;
}

int parse_cor_addr(__be64 *addr, char *saddr, char terminator)
{
	size_t saddr_len = strlen(saddr);

	if (terminator == 0) {
		if (saddr_len != 16) {
			return 1;
		}
	} else {
		if (saddr_len < 17 || saddr[16] != terminator) {
			return 1;
		}
	}

	if (parse_hex((char *) addr, saddr, 16) != 0)
		return 1;

	return 0;
}
