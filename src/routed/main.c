/**
 *	Connection oriented routing user space utils
 *	Copyright (C) 2009-2020
 *	Authors:
 * 		Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#include "routed.h"

#include <sys/timerfd.h>

__u64 start_time_nsec = 0;

int epoll_fd = 0;

int rdsock_fd = 0;
struct epoll_event rds_epe;

#define EPOLL_EVENTS 16


static void send_unreach_error(__u64 cookie)
{
	int rc;
	// printf("send_rdsock_connecterror\n");
	set_nonblock(rdsock_fd, 0);
	rc = send_rdsock_connecterror(rdsock_fd, cookie,
			CONNECTERROR_NETUNREACH);
	set_nonblock(rdsock_fd, 1);
	ASSERT(rc == RC_OK);
}

static void send_connrefused_error(__u64 cookie)
{
	int rc;
	// printf("send_connrefused_error\n");
	set_nonblock(rdsock_fd, 0);
	rc = send_rdsock_connecterror(rdsock_fd, cookie,
			CONNECTERROR_REFUSED);
	set_nonblock(rdsock_fd, 1);
	ASSERT(rc == RC_OK);
}



static void __rdscmd_connect(struct nonblock_resumeinfo *nr)
{
	int rc = RC_OK;
	__u8 reasoncode;
	__u8 reasoncode_set = 0;

	struct libcor_nonblock_resumeinfo *lnr = &(nr->data.rds_connect.lnr);

	ASSERT(nr->type == EPOLLDATA_RDSCONNECT);

	if (nr->data.rds_connect.state == 1) {
		goto state_1;
	} else if (nr->data.rds_connect.state == 2) {
		goto state_2;
	} else if (nr->data.rds_connect.state == 3) {
		goto state_3;
	} else if (nr->data.rds_connect.state != 0) {
		ASSERT(0);
	}

	rc = connect_to_host_send(nr->fd, lnr,
				&(nr->data.rds_connect.connect_send),
				nr->data.rds_connect.e2er, 0);
	if (check_rc(rc, "__rdscmd_connect: connect_to_host_send error"))
		goto out;

	nr->data.rds_connect.state = 1;
state_1:
	rc = send_connect_port_nonblock(nr->fd, lnr, nr->data.rds_connect.port,
			1);
	nr->data.rds_connect.state = 2;
	if (check_rc(rc, "__rdscmd_connect: send_connect_port error"))
		goto out;

state_2:
	rc = connect_to_host_recv(nr->fd, lnr,
			&(nr->data.rds_connect.connect_resp),
			nr->data.rds_connect.e2er);
	if (check_rc(rc, "__rdscmd_void_connect: connect_to_host_recv error"))
		goto out;

	nr->data.rds_connect.state = 3;
state_3:
	rc = read_resp_nonblock(nr->fd, 0, lnr, &reasoncode, &reasoncode_set,
			0);
	if (check_rc(rc, "__rdscmd_connect: read_resp error"))
		goto out;

	rc = pass_socket(nr->fd, nr->data.rds_connect.cookie);
	if (rc != RC_OK) {
		printf("pass_socket error\n");
		goto err;
	}

	return;

out:
	if (rc == RC_WOULDBLOCK)
		return;

err:
	close(nr->fd);

	if (reasoncode_set != 0 && reasoncode == CDR_EXECFAILED_PORTCLOSED)
		send_connrefused_error(nr->data.rds_connect.cookie);
	else
		send_unreach_error(nr->data.rds_connect.cookie);
}

#warning todo setsockopt __u32
static void _rdscmd_connect(__u64 cookie, struct node *node, __be32 port,
		__u32 tos)
{
	int fd, rc;
	struct nonblock_resumeinfo *nr;

	struct epoll_event epe;


	fd = socket(PF_COR, SOCK_RAW, PROTO_COR_RAW);
	if (fd < 0) {
		perror("socket");
		goto out_error;
	}

	if (setsockopts_tos(fd, tos) != 0) {
		perror("setsockopt");
		close(fd);
		goto out_error;
	}

	if (connect(fd, 0, 0) != 0) {
		perror("connect");
		close(fd);
out_error:
		send_unreach_error(cookie);
		return;
	}


	set_nonblock(fd, 1);

	nr = (struct nonblock_resumeinfo *)
			malloc(sizeof(struct nonblock_resumeinfo));
	bzero(nr, sizeof(struct nonblock_resumeinfo));
	nr->fd = fd;
	nr->type = EPOLLDATA_RDSCONNECT;
	nr->data.rds_connect.cookie = cookie;
	if (node == 0) {
		nr->data.rds_connect.e2er = get_e2eroute_localhost();
	} else {
		nr->data.rds_connect.e2er = get_e2eroute(node);
	}

	nr->data.rds_connect.port = port;

	bzero(&epe, sizeof(struct epoll_event));
	epe.events = (EPOLLIN | EPOLLOUT | EPOLLRDHUP | EPOLLERR |
			EPOLLET);
	epe.data.ptr = nr;
	epoll_ctl(epoll_fd, EPOLL_CTL_ADD, fd, &epe);

	__rdscmd_connect(nr);
}

static int rdscmd_connect(void *ptr, __u64 cookie,
		struct cor_sockaddr *addr, __u32 tos)
{
	int fd, rc;
	struct nonblock_resumeinfo *nr;

	struct epoll_event epe;
	struct node *node;

	if (addr->sin_family != AF_COR) {
		printf("rds_connect %llu not af_cor\n", cookie);
		goto out_error;
	}

	if (verbosity >= 1)
		printf("rds_connect cookie: %llu, addr: %llu port: %u\n", cookie, ntohll(addr->addr), ntohs(addr->port));

	if (addr->addr == 0) {
		node = 0;
	} else if ((node = try_find_neigh_byaddr(addr->addr)) == 0) {
		if (verbosity >= 1)
			printf("connect_to_host host not found\n");
		goto out_error;
	}

	_rdscmd_connect(cookie, node, addr->port, tos);

	if (0) {
out_error:
		send_unreach_error(cookie);
	}
	return 0;
}

static int proc_rdsock_cmd(int fd, struct rdsock_cmd *cmd)
{
	if (cmd->cmd == CRD_KTU_CONNECT) {
		return parse_rdsock_connect(0, cmd, rdscmd_connect);
	} else {
		printf("error in proc_rdsock_cmd: unknown cmd: %u\n", cmd->cmd);
		ASSERT(0);
		return 1;
	}
}

static void nonblock_resume(struct nonblock_resumeinfo *nr, __u32 eventflags)
{
	if (nr->type == EPOLLDATA_TIMER) {
		int rc;
		__u64 expirations;

		rc = read(nr->fd, (char *) &expirations, 8);
		ASSERT(rc == 8);

		discover_network();
	} else if (nr->type == EPOLLDATA_DISCOVERNETWORK) {
		int rc = resume_send_ifneeded(nr->fd,
				&(nr->data.discover_network.lnr));

		//printf("load_neigh_list state = %d rc = %d\n", nr->data.discover_network.state, rc);

		if (rc != RC_OK)
			return;

		_discover_network(nr->fd, nr);
	} else if (nr->type == EPOLLDATA_RDSCONNECT) {
		int rc = resume_send_ifneeded(nr->fd,
				&(nr->data.rds_connect.lnr));

		if (rc != RC_OK)
			return;

		__rdscmd_connect(nr);
	} else if (nr->type == EPOLLDATA_RDSOCKCMD) {
		struct rdsock_cmd cmd;

		int rc = resume_send_ifneeded(nr->fd,
				&(nr->data.rdsock_cmd.lnr));

		if (rc != RC_OK)
			return;

		rc = read_rdsock_cmd_nonblock(rdsock_fd,
				&(nr->data.rdsock_cmd.lnr), &cmd);
		ASSERT(rc != RC_CONNBROKEN);
		if (rc == RC_WOULDBLOCK)
			return;

		rc = proc_rdsock_cmd(rdsock_fd, &cmd);
		free_rdsockcmd_data(&cmd);
		ASSERT(rc == 0);
	} else if (nr->type == EPOLLDATA_FWD_TO_R) {
		fwd_to_r_accept(nr->fd, nr->data.fwd_to_r.fwd_to_host,
				nr->data.fwd_to_r.addr,
				nr->data.fwd_to_r.targetport);
	} else if (nr->type == EPOLLDATA_FWD_TO_LSERVICE) {
		fwd_to_lservice_accept(nr->fd, nr->data.fwd_to_lservice.af,
				nr->data.fwd_to_lservice.saddr,
				nr->data.fwd_to_lservice.saddrlen);
	} else if (nr->type == EPOLLDATA_FORWARD) {
		forward(nr, eventflags);
	} else {
		ASSERT(0);
	}
}

static int open_timerfd()
{
	int timerfd;
	int rc;

	struct itimerspec itimerspec;

	struct nonblock_resumeinfo *nr;
	struct epoll_event epe;

	timerfd = timerfd_create(CLOCK_BOOTTIME, TFD_NONBLOCK);
	if (timerfd <= 0) {
		perror("timerfd_create");
		return 1;
	}

	bzero(&itimerspec, sizeof(struct itimerspec));
	itimerspec.it_value.tv_sec = 2;
	itimerspec.it_interval.tv_sec = 2;
	itimerspec.it_value.tv_nsec = 500000000;
	itimerspec.it_interval.tv_nsec = 500000000;

	rc = timerfd_settime(timerfd, 0, &itimerspec, 0);
	if (rc != 0) {
		perror("timerfd_settime");
		return 1;
	}

	nr = (struct nonblock_resumeinfo *)
			malloc(sizeof(struct nonblock_resumeinfo));
	bzero(nr, sizeof(struct nonblock_resumeinfo));
	nr->fd = timerfd;
	nr->type = EPOLLDATA_TIMER;

	bzero(&epe, sizeof(struct epoll_event));
	epe.events = EPOLLIN;
	epe.data.ptr = nr;

	epoll_ctl(epoll_fd, EPOLL_CTL_ADD, timerfd, &epe);

	return 0;
}

#warning todo move this to libcor (in demo/ too)
static int rdsock_negotiate_version(void)
{
	struct rdsock_cmd cmd;
	__u32 versionmin = 1000;
	__u32 versionmax = 0;
	int rc;

	rc = read_rdsock_cmd(rdsock_fd, &cmd);
	if (rc != RC_OK) {
		printf("read_rdsock_cmd rc = %d\n", rc);
		return 1;
	}

	if (cmd.cmd != CRD_KTU_SUPPORTEDVERSIONS) {
		printf("rhsock supportedversions not sent\n", rc);
		return 1;
	}

	rc = parse_rdsock_supported_versions(&cmd, &versionmin, &versionmax);
	if (rc != 0) {
		printf("parse_rdsock_supported_versions rc = %d\n", rc);
		return 1;
	}

	/* printf("rdsock_negotiate_version versionmin %u versionmax %u\n",
			versionmin, versionmax); */

	if (versionmin != 0) {
		printf("rdsock_negotiate_version versionmin of kernel is %u, "
				"but needs to be 0\n"
				"You probably need to upgrade corutils or "
				"downgrade the kernel\n", versionmin);
		return 1;
	}

	rc = send_rdsock_version(rdsock_fd, 0);
	ASSERT(rc == RC_OK);

	return 0;
}

void discover_network_loop(void)
{
	__u64 start_time_ms = get_time_nsec()/1000000;

	while (1) {
		int u;
		struct epoll_event events[EPOLL_EVENTS];
		int rdycnt;

		__u64 time_since_start_ms;

		discover_network();

		time_since_start_ms = get_time_nsec()/1000000 - start_time_ms;

		if (time_since_start_ms >= 30000 ||
				!initial_discovery_inprogress()) {
			if (verbosity >= 1)
				printf("discover finished\n");
			return;
		}

		rdycnt = epoll_wait(epoll_fd, events, EPOLL_EVENTS,
				(int) (30000 - time_since_start_ms));


		for (u=0;u<rdycnt;u++) {
			struct nonblock_resumeinfo *nr = events[u].data.ptr;
			__u32 eventflags = events[u].events;
			nonblock_resume(nr, eventflags);
		}
	}
}

void main_loop(void)
{
	while (1) {
		int u;
		struct epoll_event events[EPOLL_EVENTS];
		int rdycnt = epoll_wait(epoll_fd, events, EPOLL_EVENTS, -1);

		for (u=0;u<rdycnt;u++) {
			struct nonblock_resumeinfo *nr = events[u].data.ptr;
			__u32 eventflags = events[u].events;
			nonblock_resume(nr, eventflags);
		}
	}
}

int main(int argc, char *argv[])
{
	int rc;
	struct list_head r_fwds;
	struct list_head lservice_fwds;

	struct nonblock_resumeinfo rds_nr;

	int discover_finished = 0;

	epoll_fd = 0;

	init_list_head(&r_fwds);
	init_list_head(&lservice_fwds);

	umask(0);

	rc = parse_args(argc, argv, &r_fwds, &lservice_fwds);
	if (rc != 0)
		goto out;


	init_nodedb();

	epoll_fd = epoll_create(1);
	if (epoll_fd <= 0) {
		perror("epoll_create");
		goto out;
	}


	rdsock_fd = socket(PF_COR, SOCK_RAW, PROTO_COR_RDEAMON);
	if (rdsock_fd < 0) {
		perror("socket");
		goto out;
	}

	if (connect(rdsock_fd, 0, 0) != 0) {
		perror("connect");
		goto out;
	}

	rc = rdsock_negotiate_version();
	if (rc != 0)
		goto out;

	rc = send_rdsock_up(rdsock_fd, has_localaddr, localaddr,
			interfaces, num_interfaces, all_interfaces);
	ASSERT(rc == RC_OK);

	set_nonblock(rdsock_fd, 1);

	epoll_add_fwd_to_lservice(&lservice_fwds);

	sleep(sleep_before_discover);

	discover_network_loop();

	bzero(&rds_nr, sizeof(struct nonblock_resumeinfo));
	rds_nr.fd = rdsock_fd;
	rds_nr.type = EPOLLDATA_RDSOCKCMD;
	bzero(&rds_epe, sizeof(struct epoll_event));
	rds_epe.events = (EPOLLIN | EPOLLRDHUP | EPOLLERR);
	rds_epe.data.ptr = &rds_nr;

	epoll_ctl(epoll_fd, EPOLL_CTL_ADD, rdsock_fd, &rds_epe);

	epoll_add_fwd_to_r(&r_fwds);

	rc = open_timerfd();
	if (rc != 0)
		goto out;


	main_loop();

out:
	return 1;
}
