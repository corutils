/**
 *	Connection oriented routing user space utils
 *	Copyright (C) 20
 *	Authors:
 * 		Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#include <errno.h>
#include <sys/socket.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <signal.h>


#include "../cor.h"
#include "../utils.h"

#define U32_MAX ((__u32) ((1LL << 32) - 1))

#define likely(x) __builtin_expect((x), 1)
#define unlikely(x) __builtin_expect((x), 0)

struct{
	int af;
	union{
		struct sockaddr_in addr_in;
		struct cor_sockaddr addr_cor;
	}bind_addr;
}config;

static int send_fully(int fd, char *buf, size_t len, __u8 msg_more)
{
	size_t sent_total = 0;
	while (sent_total < len) {
		ssize_t sent = send(fd, buf + sent_total, len - sent_total,
				msg_more == 1 ? MSG_MORE : 0);

		if (unlikely(sent < 0)) {
			if (errno == EINTR) {
				continue;
			} else if (errno != ECONNRESET) {
				perror("send");
			}
			return 1;
		}

		sent_total += sent;
	}

	return 0;
}

static void serve_echo(int fd, __u32 blocksize)
{
	char *buf = malloc(blocksize+1);
	bzero(buf, blocksize+1);
	__u8 msg_more = 0;

	while (1) {
		ssize_t rcvd = recv(fd, buf + msg_more, blocksize,
				msg_more == 1 ? MSG_DONTWAIT : 0);

		if (unlikely(rcvd <= 0)) {
			if (rcvd == 0) {
				break;
			} else if (errno == EAGAIN || errno == EWOULDBLOCK) {
				rcvd = 0;
			} else if (errno == EINTR) {
				continue;
			} else if (errno == ECONNRESET) {
				break;
			} else {
				perror("recv");
				break;
			}
		}

		ASSERT(rcvd <= 1024*1024*1024);

		rcvd += msg_more;
		msg_more = rcvd > 1 ? 1 : 0;
		if (unlikely(send_fully(fd, buf, (size_t) rcvd - msg_more,
				msg_more) != 0))
			break;

		if (msg_more == 1)
			buf[0] = buf[rcvd-1];
	}
}

static void serve_discard(int fd, __u32 blocksize)
{
	char *buf = malloc(blocksize);
	bzero(buf, blocksize);

	__u32 buf_rcvd = 0;

	ASSERT(blocksize >= 4);

	while (1) {
		ssize_t rcvd = recv(fd, buf + buf_rcvd, blocksize - buf_rcvd,
				0);

		if (unlikely(rcvd <= 0)) {
			if (rcvd == 0) {
				break;
			} else if (errno == EINTR) {
				continue;
			} else if (errno == ECONNRESET) {
				break;
			} else {
				perror("recv");
				break;
			}
		}

		ASSERT(rcvd <= 1024*1024*1024);
		ASSERT(buf_rcvd <= 1024*1024*1024);

		if (buf_rcvd < 4 && buf_rcvd + rcvd >= 4 && unlikely(
				buf[0] != 0 || buf[1] != 0 ||
				buf[2] != 0 || buf[3] != 0)) {
			if (unlikely(send_fully(fd, buf, 4, 0) != 0)) {
				break;
			}
		}

		buf_rcvd += rcvd;
		if (buf_rcvd == blocksize)
			buf_rcvd = 0;
	}
}

static void serve_chargen(int fd, __u32 blocksize)
{
	char *rcvbuf = malloc(4);
	__u32 rcvbuf_rcvd = 0;

	char *buf = malloc(blocksize);
	bzero(buf, blocksize);

	__u32 buf_rcvd = 0;

	ASSERT(blocksize >= 4);

	while (1) {
		ssize_t rcvd = recv(fd, rcvbuf + rcvbuf_rcvd, 4 - rcvbuf_rcvd,
				MSG_DONTWAIT);

		if (unlikely(rcvd == 0)) {
			break;
		} else if (rcvd < 0) {
			if (errno == EAGAIN || errno == EWOULDBLOCK) {
			} else if (errno == EINTR) {
				continue;
			} else if (errno == ECONNRESET) {
				break;
			} else {
				perror("recv");
				break;
			}
		} else {
			ASSERT(rcvd <= 4);
			rcvbuf_rcvd += rcvd;
			ASSERT(rcvbuf_rcvd <= 4);
		}

		if (unlikely(rcvbuf_rcvd == 4)) {
			memcpy(buf, rcvbuf, 4);
			rcvbuf_rcvd = 0;
		} else {
			bzero(buf, 4);
		}

		if (unlikely(send_fully(fd, buf, blocksize, 1) != 0)) {
			break;
		}
	}
}


#define FUNC_ECHO 0
#define FUNC_DISCARD 1
#define FUNC_CHARGEN 2

static int read_clientreq(int fd, __u8 *func, __u32 *blocksize)
{
	char buf[5];
	__u32 buf_rcvd = 0;

	bzero(buf, 5);

	while (buf_rcvd < 5) {
		ssize_t rcvd = recv(fd, buf + buf_rcvd, 5 - buf_rcvd, 0);

		if (unlikely(rcvd <= 0)) {
			if (rcvd == 0) {
				return 1;
			} else if (errno == EINTR) {
				continue;
			} else if (errno == ECONNRESET) {
				return 1;
			} else {
				perror("recv");
				return 1;
			}
		}

		ASSERT(rcvd <= 5);
		buf_rcvd += rcvd;
		ASSERT(buf_rcvd <= 5);
	}

	ASSERT(buf_rcvd == 5);

	*func = buf[0];

	memcpy((char *) blocksize, &(buf[1]), 4);
	*blocksize = ntohl(*blocksize);

	return 0;
}

static int disable_powersave(char *filename, __s32 value)
{
	int fd = open(filename, O_WRONLY);

 	if (fd < 0) {
		fprintf(stderr, "Warning: cannot open %s: ", filename);
		perror("");
		return fd;
	}

	write(fd, &value, sizeof(value));

	return fd;
}

static void serve(int fd)
{
	int fd_ps1 = disable_powersave("/dev/cpu_dma_latency", 0);
	int fd_ps2 = disable_powersave("/dev/network_latency", 0);

	__u8 func;
	__u32 blocksize;

	if (read_clientreq(fd, &func, &blocksize) != 0)
		goto out;

	printf("func %u blocksize %u\n", (__u32) func, blocksize);

	if (blocksize < 4 || blocksize > 65536)
		goto out;

	if (func == FUNC_ECHO)
		serve_echo(fd, blocksize);
	else if (func == FUNC_DISCARD)
		serve_discard(fd, blocksize);
	else if (func == FUNC_CHARGEN)
		serve_chargen(fd, blocksize);

out:
	close(fd);

	if (fd_ps1 >= 0)
		close(fd_ps1);

	if (fd_ps2 >= 0)
		close(fd_ps2);
}

static int parse_config(int argc, char *argv[])
{
	int bindaddr_found = 0;

	__u32 argsconsumed = 0;

	bzero(&config, sizeof(config));

	if (argc > 10000)
		goto usage;

	while (1) {
		if (argc <= argsconsumed + 1)
			break;

		if (strcmp(argv[argsconsumed + 1], "--ip4") == 0) {
			config.af = AF_INET;
			config.bind_addr.addr_in.sin_family = AF_INET;
			config.bind_addr.addr_in.sin_port = htons(2372);

			bindaddr_found = 1;

			argsconsumed += 1;
		} else if (strcmp(argv[argsconsumed + 1],
				"--cor") == 0) {
			config.af = AF_COR;
			config.bind_addr.addr_cor.sin_family = AF_COR;
			config.bind_addr.addr_cor.port = htonl(2372);

			bindaddr_found = 1;

			argsconsumed += 1;
		} else {
			goto usage;
		}
	}

	if (bindaddr_found == 0)
		goto usage;

	return 0;

usage:
	fprintf(stderr, "usage: test_perfs\n"
			"\t--ip4 or --cor\n");

	return 1;
}

int main(int argc, char *argv[])
{
	int fd, rc;

	int optval;

	parse_config(argc, argv);

	signal(SIGCHLD, SIG_IGN);

	fd = socket(config.af, SOCK_STREAM, 0);

	if (fd < 0) {
		perror("socket");
		goto early_out;
	}

	optval = 1;
	if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &optval,
			sizeof(optval)) != 0) {
		perror("setsockopt");
		goto out;
	}

	if (config.af == AF_INET)
		rc = bind(fd, (struct sockaddr *) &(config.bind_addr.addr_in),
				sizeof(config.bind_addr.addr_in));
	else if (config.af == AF_COR)
		rc = bind(fd, (struct sockaddr *) &(config.bind_addr.addr_cor),
				sizeof(config.bind_addr.addr_cor));
	else
		rc = 1;

	if (rc < 0) {
		perror("bind");
		goto out;
	}

	rc = listen(fd, 100);
	if (rc < 0) {
                perror("listen");
                goto out;
        }

	while (1) {
		int fd2 = accept(fd, 0, 0);

		if (fd2 == -1) {
			if (errno == EINTR) {
				continue;
			}
			perror("accept");
			goto out;
		}

		if (config.af == AF_INET) {
			optval = 1;
			if (setsockopt(fd2, IPPROTO_TCP, TCP_NODELAY, &optval,
					sizeof(optval)) != 0) {
				perror("setsockopt");
				goto out;
			}
		}


		rc = fork();
		if (rc == -1) {
			perror("fork");
			goto out;
		} else if (rc == 0) {
			//child process
			close(fd);
			serve(fd2);
			return 0;
		}
	}

out:
	close(fd);
early_out:

	return 0;
}


