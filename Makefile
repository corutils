#CFLAGS=-g -O99
CFLAGS=-Os

#todo headerfiles

all: benchmark bin/cor_routed

clean:
	rm bin/*

demo: bin/demo_connect bin/demo_connect2 bin/demo_invalidconnect bin/demo_listen bin/demo_neigh \
	bin/demo_route bin/demo_routed bin/demo_routed2 bin/demo_echo bin/demo_perf

benchmark: bin/cor_perfs bin/cor_perfc


#src/demo/libcor.o: src/demo/libcor.c src/demo/libcor.h
#	$(CC) $(CFLAGS) -c -o src/demo/libcor.o src/demo/libcor.c


bin/demo_connect: src/demo/connect.c src/demo/libcor.c src/demo/libcor.h \
		src/cor.h src/list.h src/utils.h
	$(CC) $(CFLAGS) -o bin/demo_connect src/demo/connect.c src/demo/libcor.c

bin/demo_connect2: src/demo/connect2.c src/cor.h \
		src/list.h src/utils.h
	$(CC) $(CFLAGS) -o bin/demo_connect2 src/demo/connect2.c

bin/demo_invalidconnect: src/demo/invalidconnect.c \
		src/cor.h src/list.h src/utils.h
	$(CC) $(CFLAGS) -o bin/demo_invalidconnect src/demo/invalidconnect.c src/demo/libcor.c

bin/demo_listen: src/demo/listen.c \
		src/cor.h src/list.h src/utils.h
	$(CC) $(CFLAGS) -o bin/demo_listen src/demo/listen.c

bin/demo_neigh: src/demo/neigh.c src/demo/libcor.c src/demo/libcor.h \
		src/cor.h src/list.h src/utils.h
	$(CC) $(CFLAGS) -o bin/demo_neigh src/demo/neigh.c src/demo/libcor.c

bin/demo_route: src/demo/route.c src/demo/libcor.c src/demo/libcor.h \
		src/cor.h src/list.h src/utils.h
	$(CC) $(CFLAGS) -o bin/demo_route src/demo/route.c src/demo/libcor.c

bin/demo_routed: src/demo/routed.c src/demo/libcor.c src/demo/libcor.h \
		src/cor.h src/list.h src/utils.h src/utils.c
	$(CC) $(CFLAGS) -o bin/demo_routed src/demo/routed.c src/demo/libcor.c src/utils.c

bin/demo_routed2: src/demo/routed2.c \
		src/cor.h src/list.h src/utils.h src/utils.c
	$(CC) $(CFLAGS) -o bin/demo_routed2 src/demo/routed2.c src/demo/libcor.c src/utils.c

bin/demo_echo: src/demo/echo.c \
		src/cor.h src/list.h src/utils.h 
	$(CC) $(CFLAGS) -o bin/demo_echo src/demo/echo.c

bin/demo_perf: src/demo/perf.c \
		src/cor.h src/list.h src/utils.h src/utils.c
	$(CC) $(CFLAGS) -o bin/demo_perf src/demo/perf.c src/utils.c


bin/cor_perfs: src/benchmark/server.c \
		src/cor.h src/list.h src/utils.h
	$(CC) $(CFLAGS) -o bin/cor_perfs src/benchmark/server.c

bin/cor_perfc: src/benchmark/client.c \
		src/cor.h src/list.h src/utils.h
	$(CC) $(CFLAGS) -o bin/cor_perfc src/benchmark/client.c src/utils.c

bin/cor_routed: src/routed/routed.h src/routed/main.c \
		src/routed/util.c src/routed/conf.c src/routed/nodedb.c \
		src/routed/forward.c src/routed/libcor.c src/routed/libcor.h \
		src/cor.h src/list.h src/utils.h src/utils.c
	$(CC) $(CFLAGS) -o bin/cor_routed src/routed/main.c \
		src/routed/util.c src/routed/conf.c src/routed/nodedb.c \
		src/routed/forward.c src/routed/libcor.c src/utils.c
