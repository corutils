/**
 *	Connection oriented routing user space utils
 *	Copyright (C) 2009-2019
 *	Authors:
 * 		Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <assert.h>
#include <arpa/inet.h>
#include <unistd.h>

#include "../utils.h"
#include "../cor.h"
#include "libcor.h"


#define MAX_ADDRLEN 128


struct route{
	struct node *dst;
};

struct route_list{
	struct route *routes;
	__u16 rows_alloc;
	__u16 numroutes;
};

struct service_list{
	__be32 *ports;
	__u16 rows_alloc;
	__u16 numports;
};

struct node{
	__be64 addr;

	struct route_list routes;

	struct service_list services;

	__u16 minhops;
	__u8 neighs_queried;
};

#define ROUTE_MAXHOPS 7

struct e2e_route{
	struct node *result;
	int routes[ROUTE_MAXHOPS + 1];
	int pos;
};

#define SEARCHQUERY_NEIGHSNOTQUERIED 1
#define SEARCHQUERY_NODEBYADDRESS 2
struct search_query{
	int type;
	union {
		__be64 addr
	}query;
};


int has_addr = 0;
__be64 localaddr;

struct route_list localneighs;
struct service_list localservices;


static int node_matches_searchquery(struct node *n, struct search_query *q)
{
	if (q->type == SEARCHQUERY_NEIGHSNOTQUERIED) {
		return n->neighs_queried == 0;
	} else if (q->type == SEARCHQUERY_NODEBYADDRESS) {
		return n->addr == q->query.addr;
	} else {
		assert(0);
		return 0;
	}
}

static int try_find_neigh(struct e2e_route *route, struct search_query *q,
		struct route_list *curr)
{
	int *pos = &(route->routes[route->pos]);

	for (*pos=0;*pos<curr->numroutes;(*pos)++) {
		struct node *n = curr->routes[*pos].dst;
		if (node_matches_searchquery(n, q)) {
			route->result = n;
			return 1;
		}
	}

	if (route->pos >= ROUTE_MAXHOPS)
		return 0;

	route->pos++;

	for (*pos=0;*pos<curr->numroutes;(*pos)++) {
		int rc = try_find_neigh(route, q,
				&(curr->routes[*pos].dst->routes));
		if (rc)
			return rc;
	}

	route->pos--;

	return 0;
}

static int try_find_neigh_byaddr(struct e2e_route *route, __be64 addr)
{
	struct search_query q;
	bzero(&q, sizeof(struct search_query));
	q.type = SEARCHQUERY_NODEBYADDRESS;
	q.query.addr = addr;

	return try_find_neigh(route, &q, &localneighs);
}

static int connect_to_host_recv(int fd, struct e2e_route *route)
{
	int u;
	printf("1\n");
	for (u=0;route != 0 && u<=route->pos;u++) {
		int rc = read_resp(fd, 0);
		if (rc != RC_OK)
			return RC_CONNBROKEN;
	}
	return RC_OK;
}

static int connect_to_host_send(int fd, struct e2e_route *route)
{
	struct route_list *routes = &localneighs;
	int u;
	for (u=0;u<=route->pos;u++) {
		struct node *n = routes->routes[route->routes[u]].dst;
		int rc = send_connect_neigh(fd,  n->addr);
		if (rc != RC_OK)
			return RC_CONNBROKEN;
		routes = &(n->routes);
	}

	return RC_OK;
}

static int connect_to_host(int fd, __be64 addr)
{
	int rc;
	struct e2e_route route;
	bzero(&route, sizeof(struct e2e_route));

	if (try_find_neigh_byaddr(&route, addr) == 0)
		return RC_CONNBROKEN;

	rc = connect_to_host_send(fd, &route);
	if (rc != RC_OK)
		goto out;

	rc = connect_to_host_recv(fd, &route);

out:
	return rc;
}

void add_neigh(void *ptr, __be64 addr)
{
	struct route_list *list = (struct route_list *) ptr;
	struct node *node;
	struct e2e_route route;

	if (addr == 0)
		return;

	if (list->numroutes >= list->rows_alloc)
		return;

	assert(list->routes[list->numroutes].dst == 0);

	bzero(&route, sizeof(struct e2e_route));
	if (try_find_neigh_byaddr(&route, addr) == 0) {
		node = calloc(1, sizeof(struct node));
		node->addr = addr;
	} else {
		node = route.result;
	}

	list->routes[list->numroutes].dst = node;
	list->numroutes++;
}

void init_neighlist(void *ptr, __u32 numneighs)
{
	struct route_list *list = (struct route_list *) ptr;
	if (numneighs > 16)
		numneighs = 16;
	#warning todo limit
	list->rows_alloc = (__u16) numneighs;
	list->routes = calloc(numneighs, sizeof(struct route));
}

void add_service(void *ptr, __be32 port)
{
	struct service_list *list = (struct service_list *) ptr;

	printf("add service %u\n", ntohs(port));

	if (list->numports >= list->rows_alloc)
		return;

	list->ports[list->numports] = port;
	list->numports++;
}

void init_servicelist(void *ptr, __u32 numservices)
{
	struct service_list *list = (struct service_list *) ptr;

	printf("init servicelist %u\n", numservices);

	if (numservices > 16)
		numservices = 16;
	list->rows_alloc = (__u16) numservices;
	list->ports = calloc(numservices, 4);
}

void load_neigh_list(struct e2e_route *route)
{
	struct route_list *list;
	struct service_list *service;
	int fd, rc;
	int u;

	fd = socket(PF_COR, SOCK_RAW, PROTO_COR_RAW);
	if (fd < 0) {
		perror("socket");
		goto early_out;
	}

	if (connect(fd, 0, 0) != 0) {
		perror("connect");
		goto out;
	}

	if (route == 0) {
		list = &localneighs;
		service = &localservices;
	} else {
		printf("addr %llx\n", ntohll(route->result->addr));
		list = &(route->result->routes);
		service = &(route->result->services);
		rc = connect_to_host_send(fd, route);

		if (rc != RC_OK) {
			printf("load_neigh_list: connect_to_host_send error\n");
			goto out;
		}
	}

	rc = send_list_neigh(fd);
	if (rc != RC_OK) {
		printf("load_neigh_list: send_list_neigh error\n");
		goto out;
	}

	rc = send_list_services(fd);
	if (rc != RC_OK) {
		printf("load_neigh_list: send_list_services error\n");
		goto out;
	}

	if (route != 0) {
		rc = connect_to_host_recv(fd, route);
		if (rc != RC_OK) {
			printf("load_neigh_list: connect_to_host_recv error\n");
			goto out;
		}
	}

	printf("2\n");
	rc = read_resp(fd, 1);
	if (rc != RC_OK)
		printf("load_neigh_list: read_resp error\n");

	rc = read_neigh_list(fd, list, init_neighlist, add_neigh);
	if (rc != RC_OK)
		printf("load_neigh_list: read_neigh_list error\n");

	printf("3\n");
	rc = read_resp(fd, 1);
	if (rc != RC_OK)
		printf("load_neigh_list: read_resp error\n");

	rc = read_service_list(fd, service, init_servicelist, add_service);
	if (rc != RC_OK)
		printf("load_neigh_list: read_service_list error\n");
out:
	printf("load_neigh_list rc %d\n", rc);
	close(fd);

early_out:
	return;
}

static void discover_network(void)
{
	struct search_query q;
	bzero(&q, sizeof(struct search_query));
	q.type = SEARCHQUERY_NEIGHSNOTQUERIED;
	load_neigh_list(0);
	while (1) {
		struct e2e_route nextroute;
		bzero(&nextroute, sizeof(struct e2e_route));
		int rc = try_find_neigh(&nextroute, &q, &localneighs);
		if (rc == 0)
			break;
		load_neigh_list(&nextroute);
		nextroute.result->neighs_queried = 1;
	}
}

struct rdscmd_connect_data{
	int rds_fd;
};

static int rdscmd_connect(void *ptr, __u64 cookie,
		struct cor_sockaddr *addr, __u32 tos)
{
	int fd, rc;

	int rds_fd = ((struct rdscmd_connect_data *) ptr)->rds_fd;

	printf("rds_connect %llu\n", cookie);

	if (addr->sin_family != AF_COR)
		goto out_noclose;

	printf("rds_connect %llu, %u\n", cookie, ntohs(addr->port));

	fd = socket(PF_COR, SOCK_RAW, PROTO_COR_RAW);
	if (fd < 0) {
		perror("socket");
		goto out_noclose;
	}

	if (connect(fd, 0, 0) != 0) {
		perror("connect");
		goto out;
	}


	printf("rds_connect addr: %llx port %u\n", ntohll(addr->addr), ntohs(addr->port));

	rc = connect_to_host(fd, addr->addr);

	if (rc != RC_OK) {
		printf("connect_to_host error\n");
		goto out;
	}


	rc = send_connect_port(fd, addr->port);
	if (rc != RC_OK) {
		printf("send_connect_port error\n");
		goto out;
	}

	printf("4\n");
	rc = read_resp(fd, 0);
	if (rc != RC_OK) {
		printf("read_resp error\n");
		goto out;
	}

	rc = pass_socket(fd, cookie);
	if (rc != RC_OK) {
		printf("pass_socket error\n");
		goto out;
	}

	if (0) {
out:
		close(fd);
out_noclose:
		printf("send_rdsock_connecterror\n");
		rc = send_rdsock_connecterror(rds_fd, cookie,
				CONNECTERROR_NETUNREACH);
		if (rc != RC_OK) {
			printf("send_rdsock_connecterror returned %d\n", rc);
			return 1;
		}
	}

	return 0;
}

static int proc_rdsock_cmd(int fd, struct rdsock_cmd *cmd)
{
	if (cmd->cmd == CRD_KTU_CONNECT) {
		struct rdscmd_connect_data data;
		data.rds_fd = fd;
		return parse_rdsock_connect(&data, cmd, rdscmd_connect);
	} else {
		printf("error in proc_rdsock_cmd: unknown cmd: %u\n", cmd->cmd);
		assert(0);
		exit(1);
		return RC_CONNBROKEN;
	}
}

int parse_addr(char *argv_addr)
{
	if (strcmp(argv_addr, "noaddr") == 0) {
		has_addr = 0;
		localaddr = 0;
		return 0;
	} else if (parse_cor_addr(&localaddr, argv_addr, 0) == 0) {
		has_addr = 1;
		return 0;
	} else {
		fprintf(stderr, "error: \"%s\" is not a valid address (expected noaddr or 16 hex digits)\n", argv_addr);
		return -1;
	}
}

int main(int argc, char *argv[])
{
	int rc;
	int rdsock;

	if (argc <= 1)
		goto usage;

	rc = parse_addr(argv[1]);
	if (rc != 0)
		goto usage;

	rdsock = socket(PF_COR, SOCK_RAW, PROTO_COR_RDEAMON);
	if (rdsock < 0) {
		perror("socket");
		goto out;
	}

	if (connect(rdsock, 0, 0) != 0) {
		perror("connect");
		goto out;
	}

	send_rdsock_version(rdsock, 0);
	send_rdsock_up(rdsock, has_addr, localaddr);

	sleep(30);

	discover_network();

	while(1) {
		struct rdsock_cmd cmd;

		rc = read_rdsock_cmd(rdsock, &cmd);
		if (rc != RC_OK) {
			printf("read_rdsock_cmd rc = %d\n", rc);
			break;
		}

		rc = proc_rdsock_cmd(rdsock, &cmd);
		free_rdsockcmd_data(&cmd);
		if (rc != RC_OK) {
			printf("proc_rdsock_cmd rc = %d\n", rc);
			break;
		}
	}

out:
	return 0;

usage:
	fprintf(stderr, "usage test_routed addr\n");
	return 1;
}
