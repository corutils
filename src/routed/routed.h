/**
 *	Connection oriented routing user space utils
 *	Copyright (C) 2009-2020
 *	Authors:
 * 		Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <errno.h>
#include <sys/epoll.h>
#include <sys/stat.h>
#include <time.h>

#include "../list.h"
#include "../utils.h"
#include "../cor.h"
#include "libcor.h"


#define U32_MAX ((__u32) ((1LL << 32) - 1))


#define MAX_ADDRLEN 128
#define DEFAULT_LATENCY_US 100000
#define EXTRA_LATENCY_PERHOP_US 100


struct route{
	struct node *dst;
	__u32 latency_us;
};

struct route_list{
	struct route *routes;
	__u16 rows_alloc;
	__u16 numroutes;
};

struct service_list{
	__be32 *ports;
	__u16 rows_alloc;
	__u16 numports;
};

#define QUERY_STATUS_NOTQUERIED 0
#define QUERY_STATUS_QUERIED 1
#define QUERY_STATUS_ERROR 2

struct node{
	__be64 addr;

	struct route_list routes;

	struct service_list services;

	__u16 addrlen;
	__u8 	query_status:2,
		query_in_progress:1,
		is_neighbor:1;

	__u64 last_query_time;

	struct list_head node_list;

	struct node *prevhop;
	__u32 metric;
};

#define SEARCHQUERY_NEIGHSNOTQUERIED 1
#define SEARCHQUERY_NODEBYADDRESS 2
#define SEARCHQUERY_NODEBYSERVICEPORT 3
struct search_query{
	int type;
	union {
		struct {
			__u16 addrlen;
			char *addr;
		}nodebyaddress;

		__be32 serviceport;
	}query;
};

struct e2eroute{
	__u16 numhops;
	__be64 *addr; /* array of size numhops */
};


#define EPOLLDATA_TIMER 1
#define EPOLLDATA_DISCOVERNETWORK 2
#define EPOLLDATA_RDSCONNECT 3
#define EPOLLDATA_RDSOCKCMD 4
#define EPOLLDATA_FWD_TO_R 5
#define EPOLLDATA_FWD_TO_LSERVICE 6
#define EPOLLDATA_FORWARD 7

struct nonblock_resumeinfo_connect_to_host_send{
	int fd;
	struct e2eroute e2er;

	__u8 state;

	__u32 u;
};

struct nonblock_resumeinfo_connect_to_host_recv{
	int fd;
	__u32 numhops;

	__u8 state;

	__u32 u;
};

#define FORWARD_BUF_SIZE 4096

struct nonblock_resumeinfo{
	int fd;

	__u8 type;

	union{
		struct{
			__u8 state;

			struct node *node;

			struct e2eroute e2er;

			struct nonblock_resumeinfo_connect_to_host_send connect_send;
			struct nonblock_resumeinfo_connect_to_host_recv connect_recv;

			struct libcor_nonblock_resumeinfo lnr;
		}discover_network;

		struct{
			__u8 state;

			__u64 cookie;
			struct e2eroute e2er;
			__be32 port;

			struct nonblock_resumeinfo_connect_to_host_send connect_send;
			struct nonblock_resumeinfo_connect_to_host_recv connect_resp;

			struct libcor_nonblock_resumeinfo lnr;
		}rds_connect;

		struct{
			struct libcor_nonblock_resumeinfo lnr;
		}rdsock_cmd;

		struct{
			__u8 fwd_to_host;
			__be64 addr;
			__be32 targetport;
		}fwd_to_r;

		struct{
			int af;
			struct sockaddr *saddr;
			socklen_t saddrlen;
		}fwd_to_lservice;

		struct{
			/**
			 * buf gets filled by reversedir and written into
			 * this fd
			 */
			char *buf;
			__u32 buffill;
			__u32 bufwritten;

			__u8 is_connected;

			struct nonblock_resumeinfo *reversedir;
		}forward;
	}data;
};

struct fwd_to_r_item{
	struct list_head allfwds;

	int bindfd;

	__u8 fwd_to_host;
	__be64 addr;
	__be32 targetport;
};

struct fwd_to_lservice_item{
	struct list_head allfwds;

	int bindfd;

	int af;
	struct sockaddr *saddr;
	socklen_t saddrlen;
};


#define offsetof(type, member)  __builtin_offsetof (type, member)

#define container_of(ptr, type, member) ({				\
		const typeof( ((type *)0)->member ) *__mptr = (ptr);	\
		(type *)( (char *)__mptr - offsetof(type,member) );})


static inline int check_rc(int rc, char *errormsg)
{
	if (rc != RC_OK && rc != RC_WOULDBLOCK)
		printf("%s\n", errormsg);
	return (rc != RC_OK);
}


/* main.c */
extern struct list_head node_list;

extern int epoll_fd;

extern int rdsock_fd;


/* util.c */
void set_nonblock(int fd, int value);


/* conf.c */
extern int has_localaddr;
extern __be64 localaddr;

extern char **interfaces;
extern __u32 num_interfaces;
extern int all_interfaces;

extern __u32 sleep_before_discover;
extern __u8 export_servicelist_enabled;

extern __u32 verbosity;

int get_static_route(__be64 addr, struct e2eroute *e2er);

int parse_args(int argc, char *argv[], struct list_head *r_fwds,
		struct list_head *lservice_fwds);


/* nodedb.c */
extern struct route_list localneighs;
extern struct service_list localservices;

extern struct list_head node_list;

int service_list_contains(struct service_list *services, __be32 port);

struct node * try_find_neigh_byaddr(__be64 addr);

struct node * try_find_neigh_byservice(__be32 serviceport);

struct e2eroute get_e2eroute_localhost(void);

struct e2eroute get_e2eroute(struct node *node);

int connect_to_host_recv(int fd, struct libcor_nonblock_resumeinfo *lnr,
		struct nonblock_resumeinfo_connect_to_host_recv *rcr,
		struct e2eroute e2er);

int connect_to_host_send(int fd, struct libcor_nonblock_resumeinfo *lnr,
		struct nonblock_resumeinfo_connect_to_host_send *cth,
		struct e2eroute e2er, int flush);

void _discover_network(int fd, struct nonblock_resumeinfo *nr);

void discover_network(void);

int initial_discovery_inprogress(void);

void init_nodedb(void);

/* forward.c */
void fwd_to_r_accept(int listenerfd, __u8 fwd_to_host, __be64 hostaddr,
		__be32 targetport);

void fwd_to_lservice_accept(int listenerfd, int af,
		struct sockaddr *saddr, socklen_t saddrlen);

void forward(struct nonblock_resumeinfo *nr, __u32 eventflags);

void epoll_add_fwd_to_r(struct list_head *r_fwds);

void epoll_add_fwd_to_lservice(struct list_head *lservice_fwds);


static inline __u64 get_time_nsec(void)
{
	struct timespec ts;
	clock_gettime(CLOCK_BOOTTIME, &ts);
	return 1000000000L * ((__u64) ts.tv_sec) + ((__u64) ts.tv_nsec);
}
