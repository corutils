/**
 *	Connection oriented routing user space utils
 *	Copyright (C) 2009-2011
 *	Authors:
 * 		Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#include <sys/socket.h>
#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>

#include "../cor.h"

int main(void)
{
	int fd, rc, fd2;

	struct cor_sockaddr addr;
	bzero(&addr, sizeof(struct cor_sockaddr));
	addr.sin_family = AF_COR;
	addr.port = htonl(1);

	fd = socket(PF_COR, SOCK_STREAM, 0);
	printf("socket\n");
	if(fd < 0) {
		perror("socket");
		goto early_out;
	}

	rc = bind(fd, (struct sockaddr *) &addr, sizeof(addr));
	printf("bind\n");
	if(rc < 0) {
		perror("bind");
		goto out;
	}

	rc = listen(fd, 10);
	printf("listen\n");
	if(rc < 0) {
                perror("listen");
                goto out;
        }


	fd2 = accept(fd, 0, 0);
	printf("accept\n");

	sleep(2);

	close(fd2);
	printf("success\n");

out:
	close(fd);

early_out:
	//sleep(10);

	return 0;
}


