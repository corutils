/**
 *	Connection oriented routing user space utils
 *	Copyright (C) 2009-2019
 *	Authors:
 * 		Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#include <stdlib.h>
#include <linux/types.h>
#include <sys/socket.h>


#define CRD_KTU_SUPPORTEDVERSIONS 1
#define CRD_KTU_CONNECT 2

#define CRD_UTK_VERSION 1
/*
 * CRD_UTK_VERSION[4] length[4] version[4]
 */

#define CRD_UTK_UP 2
#define CRD_UTK_UP_FLAGS_ADDR 1
#define CRD_UTK_UP_FLAGS_INTERFACES 2
/*
 * CRD_UTK_UP[4] length[4] flags[8]
 * if CRD_UTK_UP_FLAGS_ADDR
 *   addr[8]
 * if CRD_UTK_UP_FLAGS_INTERFACES:
 *   num_interfaces[4] (length[4] interface[length])[num_interfaces]
 */

#define CRD_UTK_CONNECTERROR 3
/*
 * CRD_UTK_CONNECTERROR[4] length[4] cookie[8] error[4]
 */


#define RESUME_TYPE_NONE 0
#define RESUME_TYPE_READ 1
#define RESUME_TYPE_WRITE 2

#define RESUME_READ_FUNC_NONE 0
#define RESUME_READ_FUNC_RESP 1
#define RESUME_READ_FUNC_NEIGHLIST 2
#define RESUME_READ_FUNC_SERVICELIST 3
#define RESUME_READ_FUNC_RDSOCK_CMD 4

#define NEIGHLIST_MAX_FIELDS 256
#define WRITE_BUF_SIZE 1024

struct listneigh_field {
	__u16 field;
	__u16 len;
};

struct libcor_nonblock_resumeinfo_resp{
	int fd;

	__u8 state;
	__u8 respcode;

	__u16 reasoncode;
	__u32 reasonlen;
};

struct libcor_nonblock_resumeinfo_neighlist{
	int fd;

	void *ptr;
	void (*init)(void *ptr, __u32 numneighs);
	void (*next_neigh)(void *ptr, __be64 addr);


	__u8 state;

	__u32 len;
	__u32 numneighs;
	__u32 numfields;

	struct listneigh_field fields[NEIGHLIST_MAX_FIELDS];

	__u64 u;
	__u64 v;

	__u32 fieldlen;

	__be64 addr;
	char latency;
};

struct libcor_nonblock_resumeinfo_servicelist{
	int fd;

	void *ptr;
	void (*init)(void *ptr, __u32 numservices);
	void (*next_service)(void *ptr, __be32 port);

	__u8 state;

	__u32 len;
	__u32 numservices;

	__u64 q;
	__be32 port;
};

struct libcor_nonblock_resumeinfo_rdsockcmd{
	int fd;

	__u8 state;

	char buf[8];

	__u32 cmd;
	__u32 cmddatalen;
	char *cmddata;
};

struct libcor_nonblock_resumeinfo{
	__u8 type;

	union{
		struct{
			struct{
				__u8 state;

				int fd;
				char *buf;
				__u32 len;
				__u32 *maxread;

				__u32 totalread;
			}read_fully;

			struct{
				__u8 state;

				int fd;
				__u32 len;
				__u32 discarded;
			}read_discard;

			struct{
				__u8 state;

				int fd;
				__u32 *len;
				__u32 *maxread;

				char buf[4];
				__u32 read;
			}read_len;

			__u8 functype;
			union{
				struct libcor_nonblock_resumeinfo_resp resp;

				struct libcor_nonblock_resumeinfo_neighlist
						neighlist;

				struct libcor_nonblock_resumeinfo_servicelist
						servicelist;

				struct libcor_nonblock_resumeinfo_rdsockcmd
						rdsock_cmd;
			}funcdata;
		}read;

		struct{
			int fd;

			__u32 len;
			__u32 totalsent;

			char buf[WRITE_BUF_SIZE];
		}write;
	}data;
};

#define RC_OK 0
#define RC_CONNBROKEN 1
#define RC_WOULDBLOCK 2

extern int resume_send(int fd, struct libcor_nonblock_resumeinfo *nr);

static inline int resume_send_ifneeded(int fd,
		struct libcor_nonblock_resumeinfo *nr)
{
	if (nr->type != RESUME_TYPE_WRITE)
		return RC_OK;

	return resume_send(fd, nr);
}

extern int read_resp_nonblock(int fd, int expect_bindata,
		struct libcor_nonblock_resumeinfo *nr);

extern int read_resp(int fd, int expect_bindata);

extern int send_connect_neigh_nonblock(int fd,
		struct libcor_nonblock_resumeinfo *nr, __be64 addr);

extern int send_connect_neigh(int fd, __be64 addr);

extern int send_connect_port_nonblock(int fd,
		struct libcor_nonblock_resumeinfo *nr,
		__be32 port);

extern int send_connect_port(int fd, __be32 port);

extern int send_list_services_nonblock(int fd,
		struct libcor_nonblock_resumeinfo *nr);

extern int send_list_services(int fd);

extern int read_service_list_nonblock(int fd,
		struct libcor_nonblock_resumeinfo *nr, void *ptr,
		void (*init)(void *ptr, __u32 numservices),
		void (*next_service)(void *ptr, __be32 port));

extern int read_service_list(int fd, void *ptr,
		void (*init)(void *ptr, __u32 numservices),
		void (*next_service)(void *ptr, __be32 port));

extern int send_list_neigh_nonblock(int fd,
		struct libcor_nonblock_resumeinfo *nr);

extern int send_list_neigh(int fd);

extern int read_neigh_list_nonblock(int fd,
		struct libcor_nonblock_resumeinfo *nr,
		void *ptr,
		void (*init)(void *ptr, __u32 numneighs),
		void (*next_neigh)(void *ptr, __be64 addr));

extern int read_neigh_list(int fd, void *ptr,
		void (*init)(void *ptr, __u32 numneighs),
		void (*next_neigh)(void *ptr, __be64 addr));



extern int pass_socket(int fd, __u64 cookie);

extern int send_rdsock_version_nonblock(int fd,
		struct libcor_nonblock_resumeinfo *nr, __u32 version);

extern int send_rdsock_version(int fd, __u32 version);

extern int send_rdsock_up_nonblock(int fd,
		struct libcor_nonblock_resumeinfo *nr,
		int has_addr, __be64 addrlen);

extern int send_rdsock_up(int fd, int has_addr, __be64 addr);

extern int send_rdsock_connecterror_nonblock(int fd,
		struct libcor_nonblock_resumeinfo *nr,
		__u64 cookie, __u32 error);

extern int send_rdsock_connecterror(int fd, __u64 cookie, __u32 error);

struct rdsock_cmd{
	__u32 cmd;
	char *cmddata;
	__u32 cmddatalen;
};

extern int parse_rdsock_supported_versions(struct rdsock_cmd *cmd,
		__u32 *versionmin, __u32 *versionmax);

extern int parse_rdsock_connect(void *ptr, struct rdsock_cmd *cmd,
		int (*proc_connect)(void *ptr, __u64 cookie,
		struct cor_sockaddr *addr, __u32 tos));

static inline void free_rdsockcmd_data(struct rdsock_cmd *cmd)
{
	free(cmd->cmddata);
	cmd->cmddata = 0;
}

extern int read_rdsock_cmd_nonblock(int fd,
		struct libcor_nonblock_resumeinfo *nr,
		struct rdsock_cmd *cmd);

extern int read_rdsock_cmd(int fd, struct rdsock_cmd *cmd);
