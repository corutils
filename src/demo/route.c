/**
 *	Connection oriented routing user space utils
 *	Copyright (C) 2009-2011
 *	Authors:
 * 		Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>

#include "../utils.h"
#include "../cor.h"
#include "libcor.h"


#define MAX_ADDRLEN 128


struct route{
	struct node *dst;
};

struct route_list{
	struct route *routes;
	__u16 rows_alloc;
	__u16 numroutes;
};

struct route_list localneighs;

struct node{
	__be64 addr;

	struct route_list routes;

	__u16 minhops;
	__u8 neighs_queried;
};

#define ROUTE_MAXHOPS 7

struct e2e_route{
	struct node *result;
	int routes[ROUTE_MAXHOPS + 1];
	int pos;
};

#define SEARCHQUERY_NEIGHSNOTQUERIED 1
#define SEARCHQUERY_NODEBYADDRESS 2
struct search_query{
	int type;
	union {
		__be64 addr;
	}query;
};

static int node_matches_searchquery(struct node *n, struct search_query *q)
{
	if (q->type == SEARCHQUERY_NEIGHSNOTQUERIED) {
		return n->neighs_queried == 0;
	} else if (q->type == SEARCHQUERY_NODEBYADDRESS) {
		return n->addr == q->query.addr;
	} else {
		assert(0);
		return 0;
	}
}

static int try_find_neigh(struct e2e_route *route, struct search_query *q,
		struct route_list *curr)
{
	int *pos = &(route->routes[route->pos]);

	for (*pos=0;*pos<curr->numroutes;(*pos)++) {
		struct node *n = curr->routes[*pos].dst;
		if (node_matches_searchquery(n, q)) {
			route->result = n;
			return 1;
		}
	}

	if (route->pos >= ROUTE_MAXHOPS)
		return 0;

	route->pos++;

	for (*pos=0;*pos<curr->numroutes;(*pos)++) {
		int rc = try_find_neigh(route, q,
				&(curr->routes[*pos].dst->routes));
		if (rc)
			return rc;
	}

	route->pos--;

	return 0;
}

static int try_find_neigh_byaddr(struct e2e_route *route, __be64 addr)
{
	struct search_query q;
	bzero(&q, sizeof(struct search_query));
	q.type = SEARCHQUERY_NODEBYADDRESS;
	q.query.addr = addr;

	return try_find_neigh(route, &q, &localneighs);
}

static int connect_to_host_recv(int fd, struct e2e_route *route)
{
	int u;
	for (u=0;route != 0 && u<=route->pos;u++) {
		int rc = read_resp(fd, 0);
		if (rc != RC_OK)
			return RC_CONNBROKEN;
	}
	return RC_OK;
}

static int connect_to_host_send(int fd, struct e2e_route *route)
{
	struct route_list *routes = &localneighs;
	int u;
	for (u=0;u<=route->pos;u++) {
		struct node *n = routes->routes[route->routes[u]].dst;
		int rc = send_connect_neigh(fd, n->addr);
		if (rc != RC_OK)
			return RC_CONNBROKEN;
		routes = &(n->routes);
	}

	return RC_OK;
}

static int connect_to_host(int fd, __be64 addr)
{
	int rc;
	struct e2e_route route;
	bzero(&route, sizeof(struct e2e_route));

	if (try_find_neigh_byaddr(&route, addr) == 0)
		return RC_CONNBROKEN;

	rc = connect_to_host_send(fd, &route);
	if (rc != RC_OK)
		goto out;

	rc = connect_to_host_recv(fd, &route);

out:
	return rc;
}

void add_neigh(void *ptr, __be64 addr)
{
	struct route_list *list = (struct route_list *) ptr;
	struct node *node;
	struct e2e_route route;

	if (addr == 0)
		return;

	if (list->numroutes >= list->rows_alloc)
		return;

	assert(list->routes[list->numroutes].dst == 0);

	bzero(&route, sizeof(struct e2e_route));
	if (try_find_neigh_byaddr(&route, addr) == 0) {
		node = calloc(1, sizeof(struct node));
		node->addr = addr;
	} else {
		node = route.result;
	}

	list->routes[list->numroutes].dst = node;
	list->numroutes++;
}

void init_neighlist(void *ptr, __u32 numneighs)
{
	struct route_list *list = (struct route_list *) ptr;
	if (numneighs > 16)
		numneighs = 16;
	#warning todo limit
	list->rows_alloc = (__u16) numneighs;
	list->routes = calloc(numneighs, sizeof(struct route));
}

void load_neigh_list(struct e2e_route *route)
{
	struct route_list *list;
	int fd, rc;
	int u;

	fd = socket(PF_COR, SOCK_RAW, PROTO_COR_RAW);
	if (fd < 0) {
		perror("socket");
		goto early_out;
	}

	if (connect(fd, 0, 0) != 0) {
		perror("connect");
		goto out;
	}

	if (route == 0) {
		list = &localneighs;
	} else {
		printf("addr: %llx\n", ntohll(route->result->addr));

		list = &(route->result->routes);
		rc = connect_to_host_send(fd, route);

		if (rc != RC_OK) {
			printf("connect_to_host_send error\n");
			goto out;
		}
	}
	rc = send_list_neigh(fd);
	if (rc != RC_OK) {
		printf("send_list_neigh error\n");
		goto out;
	}

	if (route != 0) {
		rc = connect_to_host_recv(fd, route);
		if (rc != RC_OK) {
			printf("connect_to_host_recv error\n");
			goto out;
		}
	}
	rc = read_resp(fd, 1);
	if (rc != RC_OK)
		printf("read_resp error\n");

	rc = read_neigh_list(fd, list, init_neighlist, add_neigh);
	if (rc != RC_OK)
		printf("read_neigh_list error\n");
out:
	printf("load_neigh_list rc %d\n", rc);
	close(fd);

early_out:
	return;
}

int main(void)
{
	struct search_query q;
	bzero(&q, sizeof(struct search_query));
	q.type = SEARCHQUERY_NEIGHSNOTQUERIED;
	load_neigh_list(0);
	while (1) {
		struct e2e_route nextroute;
		bzero(&nextroute, sizeof(struct e2e_route));
		int rc = try_find_neigh(&nextroute, &q, &localneighs);
		if (rc == 0)
			break;
		load_neigh_list(&nextroute);
		nextroute.result->neighs_queried = 1;
	}
	return 0;
}

