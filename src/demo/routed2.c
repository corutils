/**
 *	Connection oriented routing user space utils
 *	Copyright (C) 2009-2019
 *	Authors:
 * 		Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <errno.h>
#include <sys/epoll.h>
#include <sys/stat.h>

#include "../list.h"
#include "../utils.h"
#include "../cor.h"
#include "libcor.h"


#define U32_MAX ((__u32) ((1LL << 32) - 1))


#define MAX_ADDRLEN 128			\


struct route{
	struct node *dst;
};

struct route_list{
	struct route *routes;
	__u16 rows_alloc;
	__u16 numroutes;
};

struct service_list{
	__be32 *ports;
	__u16 rows_alloc;
	__u16 numports;
};

#define ROUTE_MAXHOPS 7

struct node{
	__be64 addr;

	struct route_list routes;

	struct service_list services;

	__u16 minhops;
	__u8 neighs_queried;

	struct list_head node_list;

	__u32 route[ROUTE_MAXHOPS + 1];
	__u32 hopcount;
};

#define SEARCHQUERY_NEIGHSNOTQUERIED 1
#define SEARCHQUERY_NODEBYADDRESS 2
#define SEARCHQUERY_NODEBYSERVICEPORT 3
struct search_query{
	int type;
	union {
		__be64 addr;
		__be32 serviceport;
	}query;
};


int has_localaddr = 0;
__be64 localaddr;

__u32 sleep_before_discover = 5;
__u8 export_servicelist_enabled = 0;

struct route_list localneighs;
struct service_list localservices;

struct list_head node_list;

int discover_finished = 0;
int epoll_fd = 0;

int rdsock_fd = 0;

#define EPOLL_EVENTS 16

#define EPOLLDATA_DISCOVERNETWORK 1
#define EPOLLDATA_RDSCONNECT 2
#define EPOLLDATA_RDSOCKCMD 3

struct nonblock_resumeinfo_connect_to_host_send{
	int fd;
	struct node *node;

	__u8 state;

	struct route_list *routes;
	int u;
};

struct nonblock_resumeinfo_connect_to_host_recv{
	int fd;
	struct node *node;

	__u8 state;

	int u;
};

#define FORWARD_BUF_SIZE 4096

struct nonblock_resumeinfo{
	int fd;

	__u8 type;

	union{
		struct{
			struct node *node;

			__u8 state;

			struct route_list *list;
			struct service_list *service;

			struct nonblock_resumeinfo_connect_to_host_send connect_send;
			struct nonblock_resumeinfo_connect_to_host_recv connect_recv;

			struct libcor_nonblock_resumeinfo lnr;
		}discover_network;

		struct{
			__u8 state;

			__u64 cookie;
			struct node *node;
			__be32 port;

			struct nonblock_resumeinfo_connect_to_host_send connect_send;
			struct nonblock_resumeinfo_connect_to_host_recv connect_resp;

			struct libcor_nonblock_resumeinfo lnr;
		}rds_connect;

		struct{
			struct libcor_nonblock_resumeinfo lnr;
		}rdsock_cmd;
	}data;
};

struct fwd_to_r_item{
	struct list_head allfwds;

	int bindfd;

	__u8 fwd_to_host;
	char *addr;
	__u32 addrlen;
	__be32 targetport;
};

struct fwd_to_lservice_item{
	struct list_head allfwds;

	int bindfd;

	int af;
	struct sockaddr *saddr;
	socklen_t saddrlen;
};



#define offsetof(type, member)  __builtin_offsetof (type, member)

#define container_of(ptr, type, member) ({				\
		const typeof( ((type *)0)->member ) *__mptr = (ptr);	\
		(type *)( (char *)__mptr - offsetof(type,member) );})


static void set_nonblock(int fd, int value)
{
	int flags;

	flags = fcntl(fd, F_GETFL, 0);
    	if (flags == -1) {
		perror("set_nonblock F_GETFL");
		exit(1);
	}

	flags = (flags & (~(O_NONBLOCK)));
	if (value)
		flags = flags | O_NONBLOCK;

    	if (fcntl(fd, F_SETFL, flags) == -1) {
		perror("set_nonblock F_SETFL");
		exit(1);
	}
}

static int service_list_contains(struct service_list *services, __be32 port)
{
	__u32 u;

	for (u=0;u<services->numports;u++) {
		if (services->ports[u] == port) {
			return 1;
		}
	}

	return 0;
}

static int node_matches_searchquery(struct node *n, struct search_query *q)
{
	if (q->type == SEARCHQUERY_NEIGHSNOTQUERIED) {
		return n->neighs_queried == 0 && n->addr != 0 &&
				n->addr != localaddr;
	} else if (q->type == SEARCHQUERY_NODEBYADDRESS) {
		return n->addr == q->query.addr;
	} else if (q->type == SEARCHQUERY_NODEBYSERVICEPORT) {
		return service_list_contains(&(n->services),
				q->query.serviceport);
	} else {
		ASSERT(0);
		return 0;
	}
}

static struct node * try_find_neigh(struct search_query *q)
{
	struct list_head *curr = node_list.next;

	while (curr != &node_list) {
		struct node *currnode = container_of(curr, struct node,
				node_list);

		if (node_matches_searchquery(currnode, q)) {
			return currnode;
		}

		curr = curr->next;
	}

	return 0;
}

static struct node * try_find_neigh_byaddr(__be64 addr)
{
	struct search_query q;
	bzero(&q, sizeof(struct search_query));
	q.type = SEARCHQUERY_NODEBYADDRESS;
	q.query.addr = addr;
	return try_find_neigh(&q);
}

static struct node * try_find_neigh_byservice(__be32 serviceport)
{
	struct search_query q;
	bzero(&q, sizeof(struct search_query));
	q.type = SEARCHQUERY_NODEBYSERVICEPORT;
	q.query.serviceport = serviceport;
	return try_find_neigh(&q);
}


static int check_rc(int rc, char *errormsg)
{
	if (rc != RC_OK && rc != RC_WOULDBLOCK)
		printf("%s\n", errormsg);
	return (rc != RC_OK);
}

static int connect_to_host_recv(int fd, struct libcor_nonblock_resumeinfo *lnr,
		struct nonblock_resumeinfo_connect_to_host_recv *rcr,
		struct node *node)
{
	if (rcr->state == 0) {
		rcr->fd = fd;
		rcr->node = node;

		rcr->state = 1;
		rcr->u=0;
	}

	ASSERT(rcr->fd = fd);
	ASSERT(rcr->node = node);

	ASSERT(node->hopcount != 0);

	for (;rcr->u < node->hopcount;rcr->u++) {
		int rc = read_resp_nonblock(fd, 0, lnr);
		if (rc != RC_OK)
			return rc;
	}
	bzero(rcr, sizeof(struct nonblock_resumeinfo_connect_to_host_recv));
	return RC_OK;
}

static int connect_to_host_send(int fd,
		struct libcor_nonblock_resumeinfo *lnr,
		struct nonblock_resumeinfo_connect_to_host_send *cth,
		struct node *node)
{
	if (cth->state == 0) {
		cth->fd = fd;
		cth->node = node;

		cth->routes = &localneighs;

		cth->state = 1;
		cth->u=0;
	}

	ASSERT(cth->fd = fd);
	ASSERT(cth->node = node);

	ASSERT(node->hopcount != 0);

	for (;cth->u < node->hopcount;) {
		struct node *n = cth->routes->routes[node->route[cth->u]].dst;
		int rc = send_connect_neigh_nonblock(fd, lnr, n->addr);
		cth->u++;
		if (rc != RC_OK)
			return rc;
		cth->routes = &(n->routes);
	}
	bzero(cth, sizeof(struct nonblock_resumeinfo_connect_to_host_send));
	return RC_OK;
}

static void add_neigh(void *ptr, __be64 addr)
{
	struct route_list *list = (struct route_list *) ptr;
	struct node *node;

	if (addr == 0)
		return;

	if (list->numroutes >= list->rows_alloc)
		return;

	ASSERT(list->routes[list->numroutes].dst == 0);

	node = try_find_neigh_byaddr(addr);

	if (node == 0) {
		node = calloc(1, sizeof(struct node));

		node->addr = addr;

		list_add_tail(&(node->node_list), &node_list);
	}

	list->routes[list->numroutes].dst = node;
	list->numroutes++;
}

static void init_neighlist(void *ptr, __u32 numneighs)
{
	struct route_list *list = (struct route_list *) ptr;
	if (numneighs > 16)
		numneighs = 16;
	#warning todo limit
	list->rows_alloc = (__u16) numneighs;
	list->routes = calloc(numneighs, sizeof(struct route));
}

static void add_service(void *ptr, __be32 port)
{
	struct service_list *list = (struct service_list *) ptr;

	printf("add service %u\n", ntohs(port));

	if (list->numports >= list->rows_alloc)
		return;

	list->ports[list->numports] = port;
	list->numports++;
}

static void init_servicelist(void *ptr, __u32 numservices)
{
	struct service_list *list = (struct service_list *) ptr;

	printf("init servicelist %u\n", numservices);

	if (numservices > 16)
		numservices = 16;
	list->rows_alloc = (__u16) numservices;
	list->numports = 0;
	list->ports = calloc(numservices, 4);
}

static int _export_servicelist(int fd, __be64 addr, __u32 cost,
		struct service_list *services)
{
	__u32 u;

	for (u=0;u<services->numports;u++) {
		int rc = dprintf(fd, "%llx:%u:%u\n", ntohll(addr),
				ntohl(services->ports[u]), cost);

		if (rc < 0) {
			return 1;
		}
	}

	return 0;
}

static void export_servicelist(void)
{
	char *servicelist_txtfile = "/var/lib/cor/services/txt";
	char *servicelist_tmpfile = "/var/lib/cor/services/tmp";

	int fd = 0;
	struct list_head *curr;

	if (export_servicelist_enabled == 0)
		return;

	fd = open(servicelist_tmpfile, O_WRONLY | O_CREAT | O_TRUNC, 0644);
	if (fd <= 0) {
		perror("export servicelist");
		return;
	}

	if (_export_servicelist(fd, 0, 0, &localservices) != 0)
		goto err;

	curr = node_list.next;
	while (curr != &node_list) {
		struct node *currnode = container_of(curr, struct node,
				node_list);

		curr = curr->next;

		ASSERT(has_localaddr != 0 || localaddr == 0);
		if (currnode->addr == 0)
			continue;
		if (currnode->addr == localaddr)
			continue;

		if (_export_servicelist(fd, currnode->addr,
				currnode->hopcount, &(currnode->services)) != 0)
			goto err;
	}

	fsync(fd);
	close(fd);

	if (rename(servicelist_tmpfile, servicelist_txtfile) != 0) {
		perror("export servicelist");
		goto err2;
	}

	if (0) {
err:
		close(fd);
err2:
		unlink(servicelist_tmpfile);
	}
}

static void __recalc_routes(struct node *node)
{
	struct list_head *curr;

	curr = node_list.next;
	while (curr != &node_list) {
		struct node *currnode = container_of(curr, struct node,
				node_list);

		if (currnode->hopcount < node->hopcount)
			break;

		curr = curr->next;
	}

	list_del(&(node->node_list));
	list_add_tail(&(node->node_list), curr);
}

static void _recalc_routes(struct node *node)
{
	struct route_list *routes = (node == 0 ?
			&localneighs : &(node->routes));

	__u32 u;

	for (u=0;u<routes->numroutes;u++) {
		struct node *target = routes->routes[u].dst;

		__u32 hopcount = (node == 0 ? 0 : node->hopcount) + 1;

		if (target->hopcount != 0 && target->hopcount <= hopcount)
			continue;

		if (node == 0) {
			bzero(&(target->route[0]), sizeof(target->route));
		} else {
			memcpy(&(target->route[0]), &(node->route[0]),
					sizeof(target->route));
		}

		target->hopcount = hopcount;
		target->route[hopcount-1] = u;

		__recalc_routes(target);
	}
}

static void recalc_routes(void)
{
	struct list_head oldnodes;
	struct list_head *curr;

	list_add(&oldnodes, &node_list);
	list_del(&node_list);
	init_list_head(&node_list);


	curr = oldnodes.next;
	while (curr != &oldnodes) {
		struct node *currnode = container_of(curr, struct node,
				node_list);

		bzero(&(currnode->route[0]), sizeof(currnode->route));
		currnode->hopcount = 0;

		curr = curr->next;
	}


	_recalc_routes(0);

	curr = node_list.next;
	while (curr != &node_list) {
		struct node *currnode = container_of(curr, struct node,
				node_list);

		_recalc_routes(currnode);

		curr = curr->next;
	}


	curr = oldnodes.next;
	while (curr != &oldnodes) {
		struct node *currnode = container_of(curr, struct node,
				node_list);

		curr = curr->next;

		list_del(&(currnode->node_list));

		free(currnode);
	}

	export_servicelist();
}

static int load_neigh_list(int fd, struct nonblock_resumeinfo *nr,
		struct node *node)
{
	struct libcor_nonblock_resumeinfo *lnr;
	int rc = RC_CONNBROKEN;

	ASSERT((fd == 0 && nr == 0) || (fd != 0 && nr != 0));

	if (fd == 0 && nr == 0) {
		struct epoll_event epe;

		fd = socket(PF_COR, SOCK_RAW, PROTO_COR_RAW);
		if (fd < 0) {
			perror("socket");
			goto early_out;
		}

		if (connect(fd, 0, 0) != 0) {
			perror("connect");
			goto out;
		}

		set_nonblock(fd, 1);

		nr = (struct nonblock_resumeinfo *)
				malloc(sizeof(struct nonblock_resumeinfo));
		bzero(nr, sizeof(struct nonblock_resumeinfo));
		nr->fd = fd;
		nr->type = EPOLLDATA_DISCOVERNETWORK;
		nr->data.discover_network.node = node;

		bzero(&epe, sizeof(struct epoll_event));
		epe.events = (EPOLLIN | EPOLLOUT | EPOLLRDHUP | EPOLLERR |
				EPOLLET);
		epe.data.ptr = nr;
		epoll_ctl(epoll_fd, EPOLL_CTL_ADD, fd, &epe);
	}

	lnr = &(nr->data.discover_network.lnr);

	ASSERT(nr->type == EPOLLDATA_DISCOVERNETWORK);
	ASSERT(nr->data.discover_network.node == node);

	if (nr->data.discover_network.state == 1) {
		goto state_1;
	} else if (nr->data.discover_network.state == 2) {
		goto state_2;
	} else if (nr->data.discover_network.state == 3) {
		goto state_3;
	} else if (nr->data.discover_network.state == 4) {
		goto state_4;
	} else if (nr->data.discover_network.state == 5) {
		goto state_5;
	} else if (nr->data.discover_network.state == 6) {
		goto state_6;
	} else if (nr->data.discover_network.state == 7) {
		goto state_7;
	} else if (nr->data.discover_network.state == 8) {
		goto state_8;
	} else if (nr->data.discover_network.state != 0) {
		ASSERT(0);
	}

	if (node == 0) {
		nr->data.discover_network.list = &localneighs;
		nr->data.discover_network.service = &localservices;
	} else {
		printf("addr %llx\n", ntohll(node->addr));

		nr->data.discover_network.list = &(node->routes);
		nr->data.discover_network.service = &(node->services);

		nr->data.discover_network.state = 1;
state_1:
		rc = connect_to_host_send(fd, lnr,
				&(nr->data.discover_network.connect_send),
				node);

		if (check_rc(rc, "load_neigh_list: connect_to_host_send error"))
			goto out;
	}

	rc = send_list_neigh_nonblock(fd, lnr);
	nr->data.discover_network.state = 2;
	if (check_rc(rc, "load_neigh_list: send_list_neigh error"))
		goto out;

state_2:
	rc = send_list_services_nonblock(fd, lnr);
	nr->data.discover_network.state = 3;
	if (check_rc(rc, "load_neigh_list: send_list_services error"))
		goto out;

state_3:
	if (node != 0) {
		nr->data.discover_network.state = 4;
state_4:
		rc = connect_to_host_recv(fd, lnr,
				&(nr->data.discover_network.connect_recv),
				node);
		if (check_rc(rc, "load_neigh_list: connect_to_host_recv error"))
			goto out;
	}

	nr->data.discover_network.state = 5;
state_5:
	rc = read_resp_nonblock(fd, 1, lnr);
	if (check_rc(rc, "load_neigh_list: read_resp error"))
		goto out;

	nr->data.discover_network.state = 6;
state_6:
	rc = read_neigh_list_nonblock(fd, lnr, nr->data.discover_network.list,
			init_neighlist, add_neigh);
	if (check_rc(rc, "load_neigh_list: read_neigh_list error"))
		goto out;

	nr->data.discover_network.state = 7;
state_7:
	rc = read_resp_nonblock(fd, 0, lnr);
	if (check_rc(rc, "load_neigh_list: read_resp error"))
		goto out;

	nr->data.discover_network.state = 8;
state_8:
	rc = read_service_list_nonblock(fd, lnr,
			nr->data.discover_network.service,
			init_servicelist, add_service);
	if (check_rc(rc, "load_neigh_list: read_service_list error"))
		goto out;

	#warning todo rollback node->routes and node->services on error
	recalc_routes();
out:
	//printf("load_neigh_list state %d\n", nr->data.discover_network.state);
	if (rc != RC_WOULDBLOCK) {
		//printf("load_neigh_list rc %d\n", rc);
		close(fd);
		if (nr != 0)
			free(nr);
	}

early_out:
	return rc;
}

static void discover_network(int fd, struct nonblock_resumeinfo *nr)
{
	int rc;
	struct search_query q;
	bzero(&q, sizeof(struct search_query));
	q.type = SEARCHQUERY_NEIGHSNOTQUERIED;

	ASSERT((fd == 0 && nr == 0) || (fd != 0 && nr != 0));

	#warning todo catch RC_CONNBROKEN
	if (fd == 0 && nr == 0) {
		rc = load_neigh_list(fd, nr, 0);
		if (rc == RC_WOULDBLOCK)
			return;
	}

	while (1) {
		struct node *node;
		if (nr == 0) {
		 	node = try_find_neigh(&q);
		 	if (node == 0)
				break;
		} else {
			ASSERT(nr->type == EPOLLDATA_DISCOVERNETWORK);
			node = nr->data.discover_network.node;
		}

		rc = load_neigh_list(fd, nr, node);
		if (rc == RC_WOULDBLOCK)
			return;
		node->neighs_queried = 1;
		fd = 0;
		nr = 0;
	}
	discover_finished = 1;
}

static void send_unreach_error(__u64 cookie)
{
	int rc;
	// printf("send_rdsock_connecterror\n");
	set_nonblock(rdsock_fd, 0);
	rc = send_rdsock_connecterror(rdsock_fd, cookie,
			CONNECTERROR_NETUNREACH);
	set_nonblock(rdsock_fd, 1);
	ASSERT(rc == RC_OK);
}

static void _rdscmd_connect(struct nonblock_resumeinfo *nr)
{
	int rc = RC_OK;

	struct libcor_nonblock_resumeinfo *lnr = &(nr->data.rds_connect.lnr);

	ASSERT(nr->type == EPOLLDATA_RDSCONNECT);

	if (nr->data.rds_connect.state == 1) {
		goto state_1;
	} else if (nr->data.rds_connect.state == 2) {
		goto state_2;
	} else if (nr->data.rds_connect.state == 3) {
		goto state_3;
	} else if (nr->data.rds_connect.state != 0) {
		ASSERT(0);
	}

	if (nr->data.rds_connect.node == 0)
		rc = RC_OK;
	else
		rc = connect_to_host_send(nr->fd,  lnr,
				&(nr->data.rds_connect.connect_send),
				nr->data.rds_connect.node);
	nr->data.rds_connect.state = 1;
	if (check_rc(rc, "_rdscmd_connect: connect_to_host_send error"))
		goto out;

state_1:
	rc = send_connect_port_nonblock(nr->fd, lnr, nr->data.rds_connect.port);
	nr->data.rds_connect.state = 2;
	if (check_rc(rc, "_rdscmd_connect: send_connect_port error"))
		goto out;

state_2:
	if (nr->data.rds_connect.node == 0)
		rc = RC_OK;
	else
		rc = connect_to_host_recv(nr->fd, lnr,
				&(nr->data.rds_connect.connect_resp),
				nr->data.rds_connect.node);
	if (check_rc(rc, "_rdscmd_void_connect: connect_to_host_recv error"))
		goto out;

	nr->data.rds_connect.state = 3;
state_3:
	rc = read_resp_nonblock(nr->fd, 0, lnr);
	if (check_rc(rc, "_rdscmd_connect: read_resp error"))
		goto out;

	rc = pass_socket(nr->fd, nr->data.rds_connect.cookie);
	if (rc != RC_OK) {
		printf("pass_socket error\n");
		goto err;
	}

	return;

out:
	if (rc == RC_WOULDBLOCK)
		return;

err:
	close(nr->fd);
	send_unreach_error(nr->data.rds_connect.cookie);
}

static int rdscmd_connect(void *ptr, __u64 cookie,
		struct cor_sockaddr *addr, __u32 tos)
{
	int fd;
	struct nonblock_resumeinfo *nr;

	struct epoll_event epe;
	struct node *node;

	if (addr->sin_family != AF_COR) {
		printf("rds_connect %llu not af_cor\n", cookie);
		goto out_noclose;
	}

	printf("rds_connect cookie: %llu, addr: %llx port %u\n", cookie, ntohll(addr->addr), ntohs(addr->port));

	if (addr->addr == 0) {
		node = 0;
	} else if ((node = try_find_neigh_byaddr(addr->addr)) == 0) {
		printf("connect_to_host host not found\n");
		goto out_noclose;
	}


	fd = socket(PF_COR, SOCK_RAW, PROTO_COR_RAW);
	if (fd < 0) {
		perror("socket");
		goto out_noclose;
	}

	if (connect(fd, 0, 0) != 0) {
		perror("connect");
		goto out;
	}

	set_nonblock(fd, 1);

	nr = (struct nonblock_resumeinfo *)
			malloc(sizeof(struct nonblock_resumeinfo));
	bzero(nr, sizeof(struct nonblock_resumeinfo));
	nr->fd = fd;
	nr->type = EPOLLDATA_RDSCONNECT;
	nr->data.rds_connect.cookie = cookie;
	nr->data.rds_connect.node = node;
	nr->data.rds_connect.port = addr->port;

	bzero(&epe, sizeof(struct epoll_event));
	epe.events = (EPOLLIN | EPOLLOUT | EPOLLRDHUP | EPOLLERR |
			EPOLLET);
	epe.data.ptr = nr;
	epoll_ctl(epoll_fd, EPOLL_CTL_ADD, fd, &epe);

	_rdscmd_connect(nr);

	if (0) {
out:
		close(fd);
out_noclose:
		send_unreach_error(cookie);
	}
	return 0;
}

static int proc_rdsock_cmd(int fd, struct rdsock_cmd *cmd)
{
	if (cmd->cmd == CRD_KTU_CONNECT) {
		return parse_rdsock_connect(0, cmd, rdscmd_connect);
	} else {
		printf("error in proc_rdsock_cmd: unknown cmd: %u\n", cmd->cmd);
		ASSERT(0);
		return 1;
	}
}

static void nonblock_resume(struct nonblock_resumeinfo *nr, __u32 eventflags)
{
	if (nr->type == EPOLLDATA_DISCOVERNETWORK) {
		int rc = resume_send_ifneeded(nr->fd,
				&(nr->data.discover_network.lnr));

		//printf("load_neigh_list state = %d rc = %d\n", nr->data.discover_network.state, rc);

		if (rc != RC_OK)
			return;

		discover_network(nr->fd, nr);
	} else if (nr->type == EPOLLDATA_RDSCONNECT) {
		int rc = resume_send_ifneeded(nr->fd,
				&(nr->data.discover_network.lnr));

		if (rc != RC_OK)
			return;

		_rdscmd_connect(nr);
	} else if (nr->type == EPOLLDATA_RDSOCKCMD) {
		struct rdsock_cmd cmd;

		int rc = resume_send_ifneeded(nr->fd,
				&(nr->data.rdsock_cmd.lnr));

		if (rc != RC_OK)
			return;

		rc = read_rdsock_cmd_nonblock(rdsock_fd,
				&(nr->data.rdsock_cmd.lnr), &cmd);
		ASSERT(rc != RC_CONNBROKEN);
		if (rc == RC_WOULDBLOCK)
			return;

		rc = proc_rdsock_cmd(rdsock_fd, &cmd);
		free_rdsockcmd_data(&cmd);
		ASSERT(rc == 0);
	} else {
		ASSERT(0);
	}
}

static int rdsock_negotiate_version(void)
{
	struct rdsock_cmd cmd;
	__u32 versionmin = 1000;
	__u32 versionmax = 0;
	int rc;

	rc = read_rdsock_cmd(rdsock_fd, &cmd);
	if (rc != RC_OK) {
		printf("read_rdsock_cmd rc = %d\n", rc);
		return 1;
	}

	if (cmd.cmd != CRD_KTU_SUPPORTEDVERSIONS) {
		printf("rhsock supportedversions not sent\n", rc);
		return 1;
	}

	rc = parse_rdsock_supported_versions(&cmd, &versionmin, &versionmax);
	if (rc != 0) {
		printf("parse_rdsock_supported_versions rc = %d\n", rc);
		return 1;
	}

	/* printf("rdsock_negotiate_version versionmin %u versionmax %u\n",
			versionmin, versionmax); */

	if (versionmin != 0) {
		printf("rdsock_negotiate_version versionmin of kernel is %u, "
				"but needs to be 0\n"
				"You probably need to upgrade corutils or "
				"downgrade the kernel\n", versionmin);
		return 1;
	}

	rc = send_rdsock_version(rdsock_fd, 0);
	ASSERT(rc == RC_OK);

	return 0;
}

int parse_addr(char *argv_addr)
{
	if (strcmp(argv_addr, "noaddr") == 0) {
		has_localaddr = 0;
		localaddr = 0;
		return 0;
	} else if (parse_cor_addr(&localaddr, argv_addr, 0) == 0) {
		has_localaddr = 1;
		return 0;
	} else {
		fprintf(stderr, "error: \"%s\" is not a valid address (expected noaddr or 16 hex digits)\n", argv_addr);
		return -1;
	}
}

static int parse_args(int argc, char *argv[], struct list_head *r_fwds,
		struct list_head *lservice_fwds)
{
	int rc;
	int addrfound = 0;
	__u32 argsconsumed = 0;

	if (argc > 10000)
		goto usage;

	while (1) {
		if (argc <= argsconsumed + 1)
			break;

		if (strcmp(argv[argsconsumed + 1], "--addr") == 0) {
			if (argc <= argsconsumed + 2)
				goto usage;

			if (addrfound != 0)
				goto usage;

			rc = parse_addr(argv[argsconsumed + 2]);
			if (rc != 0)
				goto usage;

			addrfound = 1;

			argsconsumed += 2;
		} else if (strcmp(argv[argsconsumed + 1],
				"--export-servicelist") == 0) {
			export_servicelist_enabled = 1;

			argsconsumed += 1;
		} else {
			goto usage;
		}
	}

	if (addrfound == 0)
		goto usage;

	return 0;

usage:
	fprintf(stderr, "usage: test_routed2"
			" [--export-servicelist]"
			" --addr addr (noaddr or 16 hex digits)\n");
	return 1;
}

int main(int argc, char *argv[])
{
	int rc;
	struct list_head r_fwds;
	struct list_head lservice_fwds;

	struct nonblock_resumeinfo rds_nr;

	int discover_finished_executed = 0;

	discover_finished = 0;
	epoll_fd = 0;

	init_list_head(&r_fwds);
	init_list_head(&lservice_fwds);

	umask(0);

	rc = parse_args(argc, argv, &r_fwds, &lservice_fwds);
	if (rc != 0)
		goto out;


	init_list_head(&node_list);

	epoll_fd = epoll_create(1);
	if (epoll_fd <= 0) {
		perror("epoll_create");
		goto out;
	}


	rdsock_fd = socket(PF_COR, SOCK_RAW, PROTO_COR_RDEAMON);
	if (rdsock_fd < 0) {
		perror("socket");
		goto out;
	}

	if (connect(rdsock_fd, 0, 0) != 0) {
		perror("connect");
		goto out;
	}

	rc = rdsock_negotiate_version();
	if (rc != 0)
		goto out;

	rc = send_rdsock_up(rdsock_fd, has_localaddr, localaddr);
	ASSERT(rc == RC_OK);

	set_nonblock(rdsock_fd, 1);

	sleep(sleep_before_discover);

	discover_network(0, 0);

	while (1) {
		int u;
		struct epoll_event events[EPOLL_EVENTS];
		int rdycnt;

		if (discover_finished_executed == 0 && discover_finished != 0) {
			struct epoll_event rds_epe;

			bzero(&rds_nr, sizeof(struct nonblock_resumeinfo));
			rds_nr.fd = rdsock_fd;
			rds_nr.type = EPOLLDATA_RDSOCKCMD;
			bzero(&rds_epe, sizeof(struct epoll_event));
			rds_epe.events = (EPOLLIN | EPOLLRDHUP | EPOLLERR);
			rds_epe.data.ptr = &rds_nr;

			epoll_ctl(epoll_fd, EPOLL_CTL_ADD, rdsock_fd, &rds_epe);

			discover_finished_executed = 1;
			printf("discover finished\n");
		}

		rdycnt = epoll_wait(epoll_fd, events, EPOLL_EVENTS, -1);

		for (u=0;u<rdycnt;u++) {
			struct nonblock_resumeinfo *nr = events[u].data.ptr;
			__u32 eventflags = events[u].events;
			nonblock_resume(nr, eventflags);
		}
	}

out:
	return 1;
}
