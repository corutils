/**
 *	Connection oriented routing user space utils
 *	Copyright (C) 2009-2020
 *	Authors:
 * 		Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#include "routed.h"


void set_nonblock(int fd, int value)
{
	int flags;

	flags = fcntl(fd, F_GETFL, 0);
    	if (flags == -1) {
		perror("set_nonblock F_GETFL");
		exit(1);
	}

	flags = (flags & (~(O_NONBLOCK)));
	if (value)
		flags = flags | O_NONBLOCK;

    	if (fcntl(fd, F_SETFL, flags) == -1) {
		perror("set_nonblock F_SETFL");
		exit(1);
	}
}
