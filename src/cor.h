/**
 *	Connection oriented routing user space utils
 *	Copyright (C) 2009-2020
 *	Authors:
 * 		Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#include <linux/types.h>

#define		AF_COR		99
#define		PF_COR		AF_COR

#define PROTO_COR_RAW 0
#define PROTO_COR_RDEAMON 1

typedef __u16 __be16;
typedef __u32 __be32;

struct cor_sockaddr {
	//`__SOCKADDR_COMMON (sin_family); /* 2 bytes */
	__u16 sin_family;

	__be32 port;
	__be64 addr;
};


#define SOL_COR	400


#define COR_PASS_ON_CLOSE 1

#define CONNECTERROR_ACCES 1
#define CONNECTERROR_NETUNREACH 2
#define CONNECTERROR_TIMEDOUT 3
#define CONNECTERROR_REFUSED 4


#define COR_PUBLISH_SERVICE 2

#define COR_TOS 3
#define COR_TOS_DEFAULT 0
#define COR_TOS_LOW_LATENCY 1
#define COR_TOS_HIGH_LATENCY 2
