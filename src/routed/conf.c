/**
 *	Connection oriented routing user space utils
 *	Copyright (C) 2009-2020
 *	Authors:
 * 		Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#include "routed.h"


int has_localaddr = 0;
__be64 localaddr;

char **interfaces = 0;
__u32 num_interfaces = 0;
int all_interfaces = 0;

int no_interface = 0;

__u32 sleep_before_discover = 5;
__u8 export_servicelist_enabled = 0;

__u32 verbosity = 1;

struct static_route{
	__be64 dest_addr;
	__u16 numhops;
	__be64 *addr; /* array of size numhops */
};

static struct static_route *static_routes = 0;
static __u32 num_static_routes = 0;


int get_static_route(__be64 addr, struct e2eroute *e2er)
{
	__u32 i;

	for (i=0;i<num_static_routes;i++) {
		if (static_routes[i].dest_addr == addr) {
			e2er->numhops = static_routes[i].numhops;
			e2er->addr = calloc(8, e2er->numhops);
			memcpy(e2er->addr, static_routes[i].addr,
					8 * e2er->numhops);
			return 0;
		}
	}

	return 1;
}


static int parse_interface(char *arg)
{
	if (num_interfaces == 0) {
		interfaces = malloc(sizeof(char *));
	} else {
		interfaces = realloc(interfaces,
				sizeof(char *) * (num_interfaces+1));
	}

	interfaces[num_interfaces] = arg;
	num_interfaces++;

	return 0;
}

static int _parse_port(char *arg, size_t len, __u16 *port)
{
	char *tmpbuf;
	char *endptr = 0;

	long int tmpport = 0;

	tmpbuf = malloc(len+1);
	bzero(tmpbuf, len+1);
	memcpy(tmpbuf, arg, len);
	tmpbuf[len] = 0;

	tmpport = strtol(tmpbuf, &endptr, 10);
	if (tmpbuf[0] == 0 || endptr == 0 || endptr != tmpbuf + len) {
		free(tmpbuf);
		tmpbuf = 0;
		return 1;
	}
	free(tmpbuf);
	tmpbuf = 0;

	if (tmpport <= 0 || tmpport >= 65536)
		return 1;

	*port = (__u16) tmpport;

	return 0;
}

static int parse_port_firstarg(char *arg, size_t len, size_t *p_portlen,
		__u16 *port)
{
	size_t portlen = 0;

	while (portlen < len) {
		if (arg[portlen] == ':')
			break;
		portlen++;
	}

	if (portlen == 0 || portlen >= len)
		return 1;

	if (_parse_port(arg, portlen, port) != 0)
		return 1;

	*p_portlen = portlen + 1;// +1 ... ':'
	return 0;
}
static size_t _parse_addr_lastarglen(char *arg, size_t len)
{
	size_t lastarglen = 0;

	while (lastarglen < len) {
		if (arg[len-lastarglen-1] == ':')
			break;
		lastarglen++;
	}
	return lastarglen;
}

static int parse_port_lastarg(char *arg, size_t len, size_t *p_portlen,
		__u16 *port)
{
	size_t portlen = 0;

	portlen = _parse_addr_lastarglen(arg, len);

	if (portlen == 0 || portlen >= len)
		return 1;

	if (_parse_port(arg + len - portlen, portlen, port) != 0)
		return 1;

	*p_portlen = portlen + 1;// +1 ... ':'
	return 0;
}

static int parse_inetaddr(char *addr, size_t len, int *af,
		struct sockaddr **saddr, socklen_t *saddrlen)
{
	size_t portlen = 0;

	__u16 port;

	char *host;

	struct in_addr inaddr;
	struct in6_addr in6addr;

	if (parse_port_lastarg(addr, len, &portlen, &port) != 0)
		return 1;

	if (portlen >= len)
		return 1;

	host = malloc(len - portlen + 1);
	bzero(host, len - portlen + 1);
	memcpy(host, addr, len - portlen);
	host[len - portlen] = 0;

	if (inet_pton(AF_INET, host, &inaddr) == 1) {
		struct sockaddr_in *ret;

		free(host);
		host = 0;

		ret = malloc(sizeof(struct sockaddr_in));
		bzero(ret, sizeof(struct sockaddr_in));

		ret->sin_family = AF_INET;
		ret->sin_port = htons(port);
		memcpy(&(ret->sin_addr), &inaddr, sizeof(inaddr));

		*af = AF_INET;
		*saddr = (struct sockaddr *) ret;
		*saddrlen = sizeof(struct sockaddr_in);

		return 0;
	} else if (inet_pton(AF_INET6, host, &in6addr) == 1) {
		free(host);
		host = 0;

		printf("sorry IPv6 support is not implemented yet\n");

		return 1;
	} else {
		free(host);
		host = 0;
		return 1;
	}
}

static int parse_fwd_to_r(char *arg,
		struct list_head *r_fwds, __u8 fwd_to_host)
{
	struct fwd_to_r_item *item;
	int af;
	struct sockaddr *saddr;
	socklen_t saddrlen;

	size_t portlen = 0;

	__be64 addr;
	__u16 serviceport;

	size_t len = strlen(arg);

	int optval;

	if (parse_port_lastarg(arg, len, &portlen, &serviceport) != 0)
		return 1;

	len -= portlen;

	if (fwd_to_host != 0) {
		if (len < 17)
			return 1;

		if (parse_cor_addr(&addr, arg + len - 17, ':') != 0)
			return 1;

		len -= 18; /* 16 for addr + 2x ':' */
	}

	if (parse_inetaddr(arg, len, &af, &saddr, &saddrlen) != 0)
		return 1;

	item = malloc(sizeof(struct fwd_to_r_item));
	bzero(item, sizeof(struct fwd_to_r_item));
	item->bindfd = socket(af, SOCK_STREAM, 0);
	if (item->bindfd < 0) {
		perror("socket");
		goto err_noclose;
	}
	optval = 1;
	if (setsockopt(item->bindfd, SOL_SOCKET, SO_REUSEADDR, &optval,
			sizeof(optval)) != 0) {
		perror("setsockopt");
		goto err;
	}
	if (bind(item->bindfd, saddr, saddrlen) != 0) {
		perror("bind");
		goto err;
	}
	if (listen(item->bindfd, 100) != 0) {
		perror("listen");
		goto err;
	}
	item->fwd_to_host = fwd_to_host;
	if (fwd_to_host) {
		item->addr = addr;
	}
	item->targetport = htonl(serviceport);

	list_add_tail(&(item->allfwds), r_fwds);

	free(saddr);

	return 0;

err:
	close(item->bindfd);
err_noclose:
	free(item);
	free(saddr);

	return 1;
}

static int parse_fwd_to_lservice(char *arg,
		struct list_head *lservice_fwds, __u8 publish)
{
	size_t portlen = 0;

	__u16 serviceport;

	struct cor_sockaddr bind_saddr;

	int optval;

	struct fwd_to_lservice_item *item;

	int af;
	struct sockaddr *saddr;
	socklen_t saddrlen;

	size_t len = strlen(arg);

	if (parse_port_firstarg(arg, len, &portlen, &serviceport) != 0)
		return 1;

	arg += portlen;
	len -= portlen;

	if (parse_inetaddr(arg, len, &af, &saddr, &saddrlen) != 0)
		return 1;

	bzero(&bind_saddr, sizeof(struct cor_sockaddr));
	bind_saddr.sin_family = AF_COR;
	bind_saddr.port = htonl(serviceport);

	item = malloc(sizeof(struct fwd_to_lservice_item));
	bzero(item, sizeof(struct fwd_to_lservice_item));
	item->bindfd = socket(PF_COR, SOCK_STREAM, 0);
	if (item->bindfd < 0) {
		perror("socket");
		goto err;
	}
	optval = publish == 0 ? 0 : 1;
	if (setsockopts_publish_service(item->bindfd, optval) != 0) {
		perror("setsockopt");
		goto err;
	}
	if (bind(item->bindfd, (struct sockaddr *) &bind_saddr,
			sizeof(bind_saddr)) != 0) {
		perror("bind");
		goto err;
	}
	if (listen(item->bindfd, 100) != 0) {
		perror("listen");
		goto err;
	}

	item->af = af;
	item->saddr = saddr;
	item->saddrlen = saddrlen;

	list_add_tail(&(item->allfwds), lservice_fwds);

	return 0;

err:
	close(item->bindfd);
err_noclose:
	free(item);
	free(saddr);

	return 1;
}

static int parse_localaddr(char *arg)
{
	if (strcmp(arg, "noaddr") == 0) {
		has_localaddr = 0;
		localaddr = 0;
		return 0;
	} else if (parse_cor_addr(&localaddr, arg, 0) == 0) {
		has_localaddr = 1;
		return 0;
	} else {
		fprintf(stderr, "error: \"%s\" is not a valid address (expected noaddr or 16 hex digits)\n", arg);
		return -1;
	}
}

static struct static_route *add_static_route()
{
	static_routes = realloc(static_routes, sizeof(struct static_route) *
			(num_static_routes + 1));
	num_static_routes++;
	return &static_routes[num_static_routes-1];
}

static int parse_static_route(char *arg)
{
	__u32 len = (__u32) strlen(arg);
	__u32 offset = 0;

	struct static_route *sr;

	int morehops = 1;

	sr = add_static_route();
	bzero(sr, sizeof(struct static_route));

	if (len < offset + 17 || arg[16] != '=' ||
			parse_cor_addr(&sr->dest_addr, arg, '=') != 0) {
		fprintf(stderr, "error: \"%s\" is not a valid static route\n", arg);
		fprintf(stderr, "a route needs to start with a 16 address (16 hex-digits) followed by an\"'=\".", arg);
		return -1;
	}
	offset += 17;


	while (morehops) {
		__be64 addr = 0;
		if (len == offset + 16) {
			if (parse_cor_addr(&addr, arg + offset, 0) != 0) {
				fprintf(stderr, "error: \"%s\" is not a valid static route\n", arg);
				fprintf(stderr, "unable to parse \"%s\"\n", arg + offset);
				return -1;
			}
			morehops = 0;
		} else {
			if (len < offset + 17 || arg[offset+16] != ',' ||
					parse_cor_addr(&addr, arg + offset, ',') != 0) {
				fprintf(stderr, "error: \"%s\" is not a valid static route\n", arg);
				fprintf(stderr, "unable to parse \"%s\"\n", arg + offset);
				return -1;

			}
			offset += 17;
		}

		sr->addr = realloc(sr->addr, 8 * (sr->numhops + 1));
		sr->addr[sr->numhops] = addr;
		sr->numhops++;
	}

	return 0;
}

int parse_args(int argc, char *argv[], struct list_head *r_fwds,
		struct list_head *lservice_fwds)
{
	int rc;
	int addrfound = 0;
	__u32 argsconsumed = 0;

	if (argc > 10000)
		goto usage;

	while (1) {
		if (argc <= argsconsumed + 1)
			break;

		if (strcmp(argv[argsconsumed + 1],
				"--verbose") == 0 ||
				strcmp(argv[argsconsumed + 1],
				"-v") == 0) {
			verbosity += 1;

			argsconsumed += 1;
		} else if (strcmp(argv[argsconsumed + 1],
				"--sleep_before_discover") == 0) {
			long int l_sleep_before_discover;
			char *endptr;

			if (argc <= argsconsumed + 2)
				goto usage;

			l_sleep_before_discover = strtol(argv[argsconsumed + 1],
					&endptr, 10);
			if (*endptr != 0)
				goto usage;
			if (l_sleep_before_discover < 0 ||
					l_sleep_before_discover > 3600)
				goto usage;

			sleep_before_discover = (__u32) l_sleep_before_discover;

			argsconsumed += 2;
		} else if (strcmp(argv[argsconsumed + 1], "--addr") == 0) {
			if (argc <= argsconsumed + 2)
				goto usage;

			if (addrfound != 0)
				goto usage;

			rc = parse_localaddr(argv[argsconsumed + 2]);
			if (rc != 0)
				goto usage;

			addrfound = 1;

			argsconsumed += 2;
		} else if (strcmp(argv[argsconsumed + 1], "--sroute") == 0) {
			if (argc <= argsconsumed + 2)
				goto usage;

			rc = parse_static_route(argv[argsconsumed + 2]);
			if (rc != 0)
				goto usage;

			argsconsumed += 2;
		} else if (strcmp(argv[argsconsumed + 1], "--intf") == 0) {
			if (argc <= argsconsumed + 2)
				goto usage;

			rc = parse_interface(argv[argsconsumed + 2]);
			if (rc != 0)
				goto usage;

			argsconsumed += 2;
		} else if (strcmp(argv[argsconsumed + 1], "--all-intf") == 0) {
			all_interfaces = 1;

			argsconsumed += 1;
		} else if (strcmp(argv[argsconsumed + 1], "--no-intf") == 0) {
			no_interface = 1;

			argsconsumed += 1;
		} else if (strcmp(argv[argsconsumed + 1],
				"--export-servicelist") == 0) {
			export_servicelist_enabled = 1;

			argsconsumed += 1;
		} else if (strcmp(argv[argsconsumed + 1],
				"--fwd-to-rservice") == 0) {
			if (argc <= argsconsumed + 2)
				goto usage;

			rc = parse_fwd_to_r(argv[argsconsumed + 2],
					r_fwds, 0);
			if (rc != 0)
				goto usage;

			argsconsumed += 2;
		} else if (strcmp(argv[argsconsumed + 1],
				"--fwd-to-rhost") == 0) {
			if (argc <= argsconsumed + 2)
				goto usage;

			rc = parse_fwd_to_r(argv[argsconsumed + 2],
					r_fwds, 1);
			if (rc != 0)
				goto usage;

			argsconsumed += 2;
		} else if (strcmp(argv[argsconsumed + 1],
				"--fwd-to-lservice") == 0) {
			if (argc <= argsconsumed + 2)
				goto usage;

			rc = parse_fwd_to_lservice(argv[argsconsumed + 2],
					lservice_fwds, 0);
			if (rc != 0)
				goto usage;
			argsconsumed += 2;
		} else if (strcmp(argv[argsconsumed + 1],
				"--fwd-to-lservice-pub") == 0) {
			if (argc <= argsconsumed + 2)
				goto usage;

			rc = parse_fwd_to_lservice(argv[argsconsumed + 2],
					lservice_fwds, 1);
			if (rc != 0)
				goto usage;

			argsconsumed += 2;
		} else {
			goto usage;
		}
	}

	if (addrfound == 0)
		goto usage;

	if (!(no_interface || all_interfaces || num_interfaces != 0))
		goto usage;

	if (no_interface && (all_interfaces || num_interfaces != 0)) {
		fprintf(stderr, "error: --intf intf and --all-intf is not allowed if --no-intf is specified\n\n");
		goto usage;
	}

	return 0;

usage:
	fprintf(stderr, "usage: cor_routed\n"
			"\t[--export-servicelist]\n"
			"\t[--fwd-to-rservice bindaddr:bindport:remoteport]\n"
			"\t[--fwd-to-rhost bindaddr:bindport:remotaaddr:remoteport]\n"
			"\t[--fwd-to-lservice bindport:localaddr:localport]\n"
			"\t[--fwd-to-lservice-pub bindport:localaddr:localport]\n"
			"\t[--sroute dest_addr=hop1_addr,hop2_addr,dest_addr]\n"
			"\t--intf intf or --all-intf or --no-intf\n"
			"\t--addr addr (noaddr or 16 hex digits)\n");
	return 1;
}
