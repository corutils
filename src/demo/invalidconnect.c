/**
 *	Connection oriented routing user space utils
 *	Copyright (C) 2009-2011
 *	Authors:
 * 		Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#include <stdio.h>
#include <string.h>

#include "../cor.h"
#include "../utils.h"
#include "libcor.h"

int main(void)
{
	int fd, rc;

	fd = socket(PF_COR, 0, 0);
	if (fd < 0) {
		perror("socket");
		return 1;
	}

	if (connect(fd, 0, 0) != 0) {
		perror("connect");
		return 1;
	}

	while (1) {
		rc = send_connect_neigh(fd, htonll(565));

		printf("send_connect_neigh rc = %d\n", rc);

		rc = read_resp(fd, 0);

		printf("read_resp rc = %d\n", rc);

		printf("we should now be stuck\n", rc);
	}

	return 0;
}
