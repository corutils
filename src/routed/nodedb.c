/**
 *	Connection oriented routing user space utils
 *	Copyright (C) 2009-2020
 *	Authors:
 * 		Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#include "routed.h"


struct route_list localneighs;
struct service_list localservices;

__u8 local_query_status = QUERY_STATUS_NOTQUERIED;
__u64 local_last_query_time = 0;

struct list_head node_list_reachable;
struct list_head node_list_unreachable;
struct list_head node_list_recalcneeded;


#warning todo refcnt of struct node

int service_list_contains(struct service_list *services, __be32 port)
{
	__u32 u;

	for (u=0;u<services->numports;u++) {
		if (services->ports[u] == port) {
			return 1;
		}
	}

	return 0;
}

struct node * try_find_neigh_byaddr(__be64 addr)
{
	struct list_head *curr = node_list_reachable.next;

	while (curr != &node_list_reachable) {
		struct node *currnode = container_of(curr, struct node,
				node_list);

		if (currnode->addr == addr)
			return currnode;

		curr = curr->next;
	}

	return 0;
}

struct node * try_find_neigh_byservice(__be32 serviceport)
{
	struct list_head *curr = node_list_reachable.next;

	while (curr != &node_list_reachable) {
		struct node *currnode = container_of(curr, struct node,
				node_list);

		if (service_list_contains(&(currnode->services), serviceport)) {
			return currnode;
		}

		curr = curr->next;
	}

	return 0;
}

struct e2eroute get_e2eroute_localhost(void)
{
	struct e2eroute ret;
	bzero(&ret, sizeof(struct e2eroute));
	ret.numhops = 0;
	return ret;
}

struct e2eroute get_e2eroute(struct node *node)
{
	struct e2eroute ret;

	struct node *currnode;
	__u32 u;

	ASSERT(node != 0);

	bzero(&ret, sizeof(struct e2eroute));

	if (get_static_route(node->addr, &ret) == 0)
		return ret;

	ret.numhops = 0;
	for (currnode = node;currnode != 0;currnode = currnode->prevhop) {
		ret.numhops += 1;
		ASSERT(currnode->prevhop != 0 || currnode->is_neighbor == 1);
	}

	ret.addr = calloc(8, ret.numhops);
	u = ret.numhops;
	for (currnode = node;currnode != 0;currnode = currnode->prevhop) {
		ASSERT(u > 0);
		ret.addr[u-1] = node->addr;
		u--;

		ASSERT(currnode->prevhop != 0 || currnode->is_neighbor == 1);
	}
	ASSERT(u == 0);

	return ret;
}

#warning todo call from rds_connect
void free_e2eroute_addr(struct e2eroute *e2er)
{
	if (e2er->numhops == 0) {
		ASSERT(e2er->addr == 0);
	} else {
		ASSERT(e2er->addr != 0);
		free(e2er->addr);
		e2er->addr = 0;

		e2er->numhops = 0;
	}

	bzero(e2er, sizeof(struct e2eroute));
}


int connect_to_host_recv(int fd, struct libcor_nonblock_resumeinfo *lnr,
		struct nonblock_resumeinfo_connect_to_host_recv *rcr,
		struct e2eroute e2er)
{
	if (rcr->state == 0) {
		rcr->fd = fd;
		rcr->numhops = e2er.numhops;

		rcr->state = 1;
		rcr->u=0;
	}

	ASSERT(rcr->fd == fd);
	ASSERT(rcr->numhops == e2er.numhops);

	for (;rcr->u < rcr->numhops;rcr->u++) {
		int rc = read_resp_nonblock(fd, 0, lnr, 0, 0, 0);
		if (rc != RC_OK)
			return rc;
	}
	bzero(rcr, sizeof(struct nonblock_resumeinfo_connect_to_host_recv));
	return RC_OK;
}

int connect_to_host_send(int fd,
		struct libcor_nonblock_resumeinfo *lnr,
		struct nonblock_resumeinfo_connect_to_host_send *cth,
		struct e2eroute e2er, int flush)
{
	if (cth->state == 0) {
		cth->fd = fd;

		cth->e2er = e2er;

		cth->state = 1;
		cth->u=0;
	}

	ASSERT(cth->fd == fd);
	ASSERT(cth->e2er.numhops == e2er.numhops);

	for (;cth->u < e2er.numhops;) {
		int flush1 = (flush && cth->u == e2er.numhops - 1);
		int rc = send_connect_neigh_nonblock(fd, lnr,
				e2er.addr[cth->u], flush1);
		cth->u++;
		if (rc != RC_OK)
			return rc;
	}
	bzero(cth, sizeof(struct nonblock_resumeinfo_connect_to_host_send));
	return RC_OK;
}

static void add_neigh(void *ptr, __be64 addr, __u32 latency_us)
{
	struct route_list *list = (struct route_list *) ptr;
	struct node *node;

	if (verbosity >= 2)
		printf("add neighbor %llx latency %u\n", ntohll(addr), latency_us);

	if (addr == 0)
		return;

	if (list->numroutes >= list->rows_alloc)
		return;

	ASSERT(list->routes[list->numroutes].dst == 0);

	node = try_find_neigh_byaddr(addr);

	if (node == 0) {
		node = calloc(1, sizeof(struct node));
		node->addr = addr;
		list_add_tail(&(node->node_list), &node_list_reachable);
	}

	list->routes[list->numroutes].dst = node;
	list->routes[list->numroutes].latency_us = latency_us;
	list->numroutes++;
}

static void init_neighlist(void *ptr, __u32 numneighs)
{
	struct route_list *list = (struct route_list *) ptr;

	if (verbosity >= 2)
		printf("init neighlist %u\n", numneighs);

	if (list->routes != 0) {
		free(list->routes);
	}

	if (numneighs > 16)
		numneighs = 16;
	#warning todo limit
	list->rows_alloc = (__u16) numneighs;
	list->numroutes = 0;
	list->routes = calloc(numneighs, sizeof(struct route));
}

static void add_service(void *ptr, __be32 port)
{
	struct service_list *list = (struct service_list *) ptr;

	if (verbosity >= 2)
		printf("add service %u\n", ntohs(port));

	if (list->numports >= list->rows_alloc)
		return;

	list->ports[list->numports] = port;
	list->numports++;
}

static void init_servicelist(void *ptr, __u32 numservices)
{
	struct service_list *list = (struct service_list *) ptr;

	if (verbosity >= 2)
		printf("init servicelist %u\n", numservices);

	if (list->ports != 0) {
		free(list->ports);
	}

	if (numservices > 16)
		numservices = 16;
	list->rows_alloc = (__u16) numservices;
	list->numports = 0;
	list->ports = calloc(numservices, 4);
}


static int _export_servicelist(int fd, __be64 addr, __u32 cost,
		struct service_list *services)
{
	__u32 u;

	for (u=0;u<services->numports;u++) {
		int rc = dprintf(fd, "%llx:%u:%u\n", ntohll(addr),
				ntohl(services->ports[u]), cost);

		if (rc < 0) {
			return 1;
		}
	}

	return 0;
}

#warning todo move to /run/
static void export_servicelist(void)
{
	char *servicelist_txtfile = "/var/lib/cor/services/txt";
	char *servicelist_tmpfile = "/var/lib/cor/services/tmp";

	int fd = 0;
	struct list_head *curr;

	if (export_servicelist_enabled == 0)
		return;

	fd = open(servicelist_tmpfile, O_WRONLY | O_CREAT | O_TRUNC, 0644);
	if (fd <= 0) {
		perror("export servicelist");
		return;
	}

	if (_export_servicelist(fd, 0, 0, &localservices) != 0)
		goto err;

	curr = node_list_reachable.next;
	while (curr != &node_list_reachable) {
		struct node *currnode = container_of(curr, struct node,
				node_list);

		curr = curr->next;

		ASSERT(has_localaddr != 0 || localaddr == 0);
		if (currnode->addr == 0)
			continue;
		if (currnode->addr == localaddr)
			continue;

		if (_export_servicelist(fd, currnode->addr,
				currnode->metric, &(currnode->services)) != 0)
			goto err;
	}

	fsync(fd);
	close(fd);

	if (rename(servicelist_tmpfile, servicelist_txtfile) != 0) {
		perror("export servicelist");
		goto err2;
	}

	if (0) {
err:
		close(fd);
err2:
		unlink(servicelist_tmpfile);
	}
}

static void _recalc_routes_add_recalcneeded(struct node *node)
{
	struct list_head *curr;

	curr = node_list_recalcneeded.prev;
	while (curr != &node_list_recalcneeded) {
		struct node *currnode = container_of(curr, struct node,
				node_list);

		if (currnode->metric > node->metric) {
			list_add(&(node->node_list), &(currnode->node_list));
			return;
		}

		curr = curr->next;
	}

	list_add(&(node->node_list), &node_list_recalcneeded);
}

static void _recalc_routes(struct node *node)
{
	struct route_list *routes = &(node->routes);

	__u32 u;

	for (u=0;u<routes->numroutes;u++) {
		struct node *target = routes->routes[u].dst;

		__u32 metric = node->metric +
				routes->routes[u].latency_us +
				EXTRA_LATENCY_PERHOP_US;

		ASSERT(node->metric != 0);
		ASSERT(node->is_neighbor == 1 || node->prevhop != 0);

		if (metric < node->metric)
			continue;

		if (target->metric <= metric)
			continue;

		target->metric = metric;
		target->prevhop = node;

		list_del(&(target->node_list));
		_recalc_routes_add_recalcneeded(target);
	}

	list_del(&(node->node_list));
	list_add_tail(&(node->node_list), &node_list_reachable);
}

static void _recalc_routes_localneighs(void)
{
	__u32 u;
	for (u=0;u<localneighs.numroutes;u++) {
		struct node *target = localneighs.routes[u].dst;

		target->is_neighbor = 1;
		target->metric = localneighs.routes[u].latency_us +
				EXTRA_LATENCY_PERHOP_US;
		target->prevhop = 0;

		list_del(&(target->node_list));
		_recalc_routes_add_recalcneeded(target);
	}
}

static void _recalc_routes_reset_reachable(void)
{
	struct list_head *curr = node_list_reachable.next;
	while (curr != &node_list_reachable) {
		struct node *currnode = container_of(curr, struct node,
				node_list);

		currnode->is_neighbor = 0;
		currnode->prevhop = 0;
		currnode->metric = 0;

		curr = curr->next;
	}

	list_add(&node_list_unreachable, &node_list_reachable);
	list_del(&node_list_reachable);
	init_list_head(&node_list_reachable);
}

static void recalc_routes(void)
{
	if (verbosity >= 2)
		printf("recalc_routes\n");

	_recalc_routes_reset_reachable();

	_recalc_routes_localneighs();

	while (!list_empty(&node_list_recalcneeded)) {
		struct node *currnode = container_of(
				node_list_recalcneeded.next, struct node,
				node_list);

		_recalc_routes(currnode);
	}

	/* we cannot free struct node here, because nonblock_resumeinfo may still have a pointer */
	/*while (!list_empty(&node_list_unreachable)) {
		struct node *currnode = container_of(
				node_list_unreachable.next, struct node,
				node_list);

		list_del(&(currnode->node_list));

		if (currnode->addr != 0) {
			free(currnode->addr);
			currnode->addr = 0;
		}

		free(currnode);
	}*/

	export_servicelist();
}


static void discover_localhost()
{
	int fd;
	int rc;

	__u8 bindata_noresp = 0;

	local_last_query_time = get_time_nsec();

	fd = socket(PF_COR, SOCK_RAW, PROTO_COR_RAW);
	if (fd < 0) {
		perror("socket");
		goto err_noclose;
	}

	if (setsockopts_tos(fd, COR_TOS_HIGH_LATENCY) != 0) {
		perror("setsockopt");
		goto err;
	}

	if (connect(fd, 0, 0) != 0) {
		perror("connect");
		goto err;
	}


	rc = send_list_neigh(fd, 1);
	if (rc != RC_OK)
		goto err;

	rc = read_resp(fd, 1, &bindata_noresp);
	if (rc != RC_OK)
		goto err;

	if (bindata_noresp != 0) {
		init_neighlist(&localneighs, 0);
	} else {
		char *resp_bin = 0;
		__u32 resp_bin_len = 0;

		rc = read_resp_bin(fd, &resp_bin, &resp_bin_len);
		if (rc != RC_OK)
			goto err;

		rc = parse_neigh_list(resp_bin, resp_bin_len, DEFAULT_LATENCY_US,
				&localneighs, init_neighlist, add_neigh);
		free(resp_bin);
		resp_bin = 0;
		resp_bin_len = 0;
		if (rc != RC_OK)
			goto err;
	}


	rc = send_list_services(fd, 1);
	if (rc != RC_OK)
		goto err;

	rc = read_resp(fd, 1, &bindata_noresp);
	if (rc != RC_OK)
		goto err;

	if (bindata_noresp != 0) {
		init_servicelist(&localservices, 0);
	} else {
		char *resp_bin = 0;
		__u32 resp_bin_len = 0;

		rc = read_resp_bin(fd, &resp_bin, &resp_bin_len);
		if (rc != RC_OK)
			goto err;

		rc = parse_service_list(resp_bin, resp_bin_len, &localservices,
				init_servicelist, add_service);
		free(resp_bin);
		resp_bin = 0;
		resp_bin_len = 0;
		if (rc != RC_OK)
			goto err;
	}

	close(fd);

	local_query_status = QUERY_STATUS_QUERIED;

	recalc_routes();

	if (0) {
err:
		close(fd);
err_noclose:
		local_query_status = QUERY_STATUS_ERROR;
	}
}

void _discover_network(int fd, struct nonblock_resumeinfo *nr)
{
	struct node *node;
	struct libcor_nonblock_resumeinfo *lnr;
	int rc = RC_CONNBROKEN;

	__u8 bindata_noresp = 0;

	lnr = &(nr->data.discover_network.lnr);

	ASSERT(nr->type == EPOLLDATA_DISCOVERNETWORK);

	node = nr->data.discover_network.node;

	if (nr->data.discover_network.state == 1) {
		goto state_1;
	} else if (nr->data.discover_network.state == 2) {
		goto state_2;
	} else if (nr->data.discover_network.state == 3) {
		goto state_3;
	} else if (nr->data.discover_network.state == 4) {
		goto state_4;
	} else if (nr->data.discover_network.state == 5) {
		goto state_5;
	} else if (nr->data.discover_network.state == 6) {
		goto state_6;
	} else if (nr->data.discover_network.state == 7) {
		goto state_7;
	} else if (nr->data.discover_network.state != 0) {
		ASSERT(0);
	}

	if (verbosity >= 2)
		printf("discover resume neighbor %llx\n", ntohll(node->addr));

	nr->data.discover_network.e2er = get_e2eroute(node);

	nr->data.discover_network.state = 1;
state_1:
	rc = connect_to_host_send(fd, lnr,
			&(nr->data.discover_network.connect_send),
			nr->data.discover_network.e2er, 0);

	if (check_rc(rc, "load_neigh_list: connect_to_host_send error"))
		goto out;

	rc = send_list_neigh_nonblock(fd, lnr, 0);
	nr->data.discover_network.state = 2;
	if (check_rc(rc, "load_neigh_list: send_list_neigh error"))
		goto out;

state_2:
	rc = send_list_services_nonblock(fd, lnr, 1);
	nr->data.discover_network.state = 3;
	if (check_rc(rc, "load_neigh_list: send_list_services error"))
		goto out;

state_3:
	rc = connect_to_host_recv(fd, lnr,
			&(nr->data.discover_network.connect_recv),
			nr->data.discover_network.e2er);
	if (check_rc(rc, "load_neigh_list: connect_to_host_recv error"))
		goto out;

	free_e2eroute_addr(&nr->data.discover_network.e2er);

	nr->data.discover_network.state = 4;
state_4:
	rc = read_resp_nonblock(fd, 1, lnr, 0, 0, &bindata_noresp);
	if (check_rc(rc, "load_neigh_list: read_resp error"))
		goto out;

	if (bindata_noresp != 0) {
		init_neighlist(&(node->routes), 0);
	} else {
		char *resp_bin = 0;
		__u32 resp_bin_len = 0;

		nr->data.discover_network.state = 5;
state_5:
		rc = read_resp_bin_nonblock(fd, lnr, &resp_bin, &resp_bin_len);
		if (check_rc(rc, "load_neigh_list: read_resp_bin_nonblock (neighborlest) error"))
			goto out;

		rc = parse_neigh_list(resp_bin, resp_bin_len, DEFAULT_LATENCY_US,
				&(node->routes), init_neighlist, add_neigh);

		free(resp_bin);
		resp_bin = 0;
		resp_bin_len = 0;

		ASSERT(rc != RC_WOULDBLOCK);
		if (check_rc(rc, "load_neigh_list: parse_neigh_list error"))
			goto out;
	}

	nr->data.discover_network.state = 6;
state_6:
	rc = read_resp_nonblock(fd, 1, lnr, 0, 0, &bindata_noresp);
	if (check_rc(rc, "load_neigh_list: read_resp error"))
		goto out;

	if (bindata_noresp != 0) {
		char *resp_bin = 0;
		__u32 resp_bin_len = 0;

		nr->data.discover_network.state = 7;
state_7:
		rc = read_resp_bin_nonblock(fd, lnr, &resp_bin, &resp_bin_len);
		if (check_rc(rc, "load_neigh_list: read_resp_bin_nonblock (servicelist) error"))
			goto out;

		rc = parse_service_list(resp_bin, resp_bin_len, &(node->services),
				init_servicelist, add_service);

		free(resp_bin);
		resp_bin = 0;
		resp_bin_len = 0;

		ASSERT(rc != RC_WOULDBLOCK);
		if (check_rc(rc, "load_neigh_list: parse_service_list error"))
			goto out;
	}

	node->query_status = QUERY_STATUS_QUERIED;
	recalc_routes();
out:
	//printf("load_neigh_list state %d\n", nr->data.discover_network.state);
	if (rc != RC_WOULDBLOCK) {
		free_e2eroute_addr(&nr->data.discover_network.e2er);

		//printf("load_neigh_list rc %d\n", rc);
		close(fd);
		if (nr != 0)
			free(nr);

		if (rc == RC_CONNBROKEN)
			node->query_status = QUERY_STATUS_ERROR;
		node->query_in_progress = 0;
	}
}

static int _discover_network_start_load_node(struct node *node)
{
	int fd;
	int rc;
	struct nonblock_resumeinfo *nr;

	struct epoll_event epe;

	if (verbosity >= 2)
		printf("discover start neighbor %llx\n", ntohll(node->addr));

	ASSERT(node->query_in_progress == 0);
	node->query_in_progress = 1;
	node->last_query_time = get_time_nsec();

	fd = socket(PF_COR, SOCK_RAW, PROTO_COR_RAW);
	if (fd < 0) {
		perror("socket");
		goto err;
	}

	if (setsockopts_tos(fd, COR_TOS_HIGH_LATENCY) != 0) {
		close(fd);
		perror("setsockopt");
		goto err;
	}

	if (connect(fd, 0, 0) != 0) {
		close(fd);
		perror("connect");
		goto err;
	}

	set_nonblock(fd, 1);

	nr = (struct nonblock_resumeinfo *)
			malloc(sizeof(struct nonblock_resumeinfo));
	bzero(nr, sizeof(struct nonblock_resumeinfo));
	nr->fd = fd;
	nr->type = EPOLLDATA_DISCOVERNETWORK;
	nr->data.discover_network.node = node;

	bzero(&epe, sizeof(struct epoll_event));
	epe.events = (EPOLLIN | EPOLLOUT | EPOLLRDHUP | EPOLLERR | EPOLLET);
	epe.data.ptr = nr;
	epoll_ctl(epoll_fd, EPOLL_CTL_ADD, fd, &epe);

	if (0) {
err:
		node->query_status = QUERY_STATUS_ERROR;
		node->query_in_progress = 0;
	}
}

void discover_network(void)
{
	struct list_head *curr;

	__u64 time_nsec = get_time_nsec();

	if (local_query_status != QUERY_STATUS_QUERIED ||
			local_last_query_time + 1000000L < time_nsec) {
		if (verbosity >= 2)
			printf("discover localhost\n");
		discover_localhost();
	}

	curr = node_list_reachable.next;

	while (curr != &node_list_reachable) {
		struct node *currnode = container_of(curr, struct node,
				node_list);

		if (currnode->query_in_progress == 0 && (
				currnode->query_status ==
				QUERY_STATUS_NOTQUERIED ||
				currnode->last_query_time + 30000000L <
				time_nsec)) {
			_discover_network_start_load_node(currnode);
		}

		curr = curr->next;
	}
}

int initial_discovery_inprogress(void)
{
	struct list_head *curr = node_list_reachable.next;

	while (curr != &node_list_reachable) {
		struct node *currnode = container_of(curr, struct node,
				node_list);

		if (currnode->query_status == QUERY_STATUS_NOTQUERIED &&
				currnode->query_in_progress != 0)
			return 1;

		curr = curr->next;
	}

	return 0;
}

void init_nodedb(void)
{
	init_list_head(&node_list_reachable);
	init_list_head(&node_list_unreachable);
	init_list_head(&node_list_recalcneeded);
}
