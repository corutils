/**
 *	Connection oriented routing user space utils
 *	Copyright (C) 2009-2019
 *	Authors:
 * 		Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#include <linux/types.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <arpa/inet.h>
#include <unistd.h>

#include "../cor.h"
#include "../utils.h"
#include "libcor.h"

#define likely(x) __builtin_expect((x), 1)
#define unlikely(x) __builtin_expect((x), 0)

#define CD_CONTINUE_ON_ERROR_FLAG 32768

#define CD_CONNECT_NB 1
#define CD_CONNECT_PORT 2
#define CD_LIST_NEIGH 3
#define CD_LIST_SERVICES 4

#define CDR_EXECOK 1

#define CDR_EXECOK_BINDATA 2
#define CDR_EXECOK_BINDATA_NORESP 3

#define CDR_EXECFAILED 4
	#define CDR_EXECFAILED_INVALID_COMMAND 1
	#define CDR_EXECFAILED_TEMPORARILY_OUT_OF_RESOURCES 2
	#define CDR_EXECFAILED_NB_DOESNTEXIST 3
	#define CDR_EXECFAILED_UNKNOWN_L4PROTOCOL 4
	#define CDR_EXECFAILED_PORTCLOSED 5

#define CDR_BINDATA 3

#define LIST_NEIGH_FIELD_ADDR 1
#define LIST_NEIGH_FIELD_LATENCY 2

#define L4PROTO_STREAM 42399


static int bzero_nr_iffinished(struct libcor_nonblock_resumeinfo *nr, int rc)
{
	/*if (rc == RC_WOULDBLOCK) {
		printf("wouldblock\n");
	}*/

	if (rc != RC_WOULDBLOCK) {
		bzero(nr, sizeof(struct libcor_nonblock_resumeinfo));
	}
	return rc;
}

#define LIBCOR_ASSERT_ERR()	\
	do {					\
		assert(0);			\
		exit(1);			\
		while (1) {			\
		}				\
	} while(0)				\


#warning todo commands are sent via multiple packets
int resume_send(int fd, struct libcor_nonblock_resumeinfo *nr)
{
	if (unlikely(nr->type != RESUME_TYPE_WRITE || nr->data.write.fd !=fd))
		LIBCOR_ASSERT_ERR();

	if (sizeof(nr->data.write.buf) != WRITE_BUF_SIZE ||
			unlikely(nr->data.write.len > WRITE_BUF_SIZE))
		LIBCOR_ASSERT_ERR();

	while (nr->data.write.totalsent < nr->data.write.len) {
		char *buf = &(nr->data.write.buf[0]);
		#warning todo use ssize_t (other places too?)
		int sent = send(fd, buf + nr->data.write.totalsent,
				nr->data.write.len - nr->data.write.totalsent,
				0);

		if (sent < 0 && (errno == EAGAIN ||
				errno == EWOULDBLOCK))
			return bzero_nr_iffinished(nr, RC_WOULDBLOCK);

		if (sent <= 0) {
			if (errno == EINTR)
				continue;

			perror("send");
			return bzero_nr_iffinished(nr, RC_CONNBROKEN);
		}

		nr->data.write.totalsent += sent;
	}

	return bzero_nr_iffinished(nr, RC_OK);
}

static int read_fully(int fd, struct libcor_nonblock_resumeinfo *nr,
		char *buf, __u32 len, __u32 *maxread)
{
	int rc = RC_OK;

	if (unlikely(nr->type != RESUME_TYPE_READ))
		LIBCOR_ASSERT_ERR();

	if (unlikely(nr->data.read.read_fully.state != 0 && (
			nr->data.read.read_fully.fd != fd ||
			nr->data.read.read_fully.buf != buf ||
			nr->data.read.read_fully.len != len ||
			nr->data.read.read_fully.maxread != maxread)))
		LIBCOR_ASSERT_ERR();

	if (nr->data.read.read_fully.state == 1) {
		goto state_1;
	} else if (unlikely(nr->data.read.read_fully.state != 0)) {
		LIBCOR_ASSERT_ERR();
	}

	nr->data.read.read_fully.fd = fd;
	nr->data.read.read_fully.buf = buf;
	nr->data.read.read_fully.len = len;
	nr->data.read.read_fully.maxread = maxread;
	nr->data.read.read_fully.totalread = 0;

	nr->data.read.read_fully.state = 1;
state_1:

	if (maxread != 0) {
		if (len > (*maxread)) {
			printf("error in read_fully: maxread reached\n");
			rc = RC_CONNBROKEN;
			goto out;
		}
		(*maxread) -= len;
	}

	while (len > nr->data.read.read_fully.totalread) {
		int rcvd = recv(fd, buf + nr->data.read.read_fully.totalread,
				len - nr->data.read.read_fully.totalread, 0);
		int u;

		if (rcvd < 0 && (errno == EAGAIN ||
				errno == EWOULDBLOCK)) {
			/* printf("wouldblock\n"); */
			rc = RC_WOULDBLOCK;
			goto out;
		}

		if (rcvd <= 0) {
			if (errno == EINTR)
				continue;

			perror("recv");
			rc = RC_CONNBROKEN;
			goto out;
		}

		/*printf("rcvd: %d:", rcvd);
		for(u=0;u<rcvd;u++) {
			printf(" %d, ", (__s32) ((__u8) buf[totalread+u]));
		}
		printf("\n");*/

		nr->data.read.read_fully.totalread += (__u32) rcvd;
	}

out:
	if (rc != RC_WOULDBLOCK) {
		bzero(&(nr->data.read.read_fully),
				sizeof(nr->data.read.read_fully));
	}

	return rc;
}

static int read_discard(int fd, struct libcor_nonblock_resumeinfo *nr,
		__u32 len)
{
	char buf[128];
	int rc = RC_OK;

	if (unlikely(nr->type != RESUME_TYPE_READ))
		LIBCOR_ASSERT_ERR();

	if (unlikely(nr->data.read.read_discard.state != 0 && (
			nr->data.read.read_discard.fd != fd ||
			nr->data.read.read_discard.len != len)))
		LIBCOR_ASSERT_ERR();

	if (nr->data.read.read_discard.state == 1) {
		goto state_1;
	} else if (unlikely(nr->data.read.read_discard.state != 0)) {
		LIBCOR_ASSERT_ERR();
	}

	nr->data.read.read_discard.fd = fd;
	nr->data.read.read_discard.len = len;
	nr->data.read.read_discard.discarded = 0;


	nr->data.read.read_discard.state = 1;
state_1:

	while (len > 0) {
		int rcvd;

		__u32 rcvlen = len - nr->data.read.read_discard.discarded;
		if (rcvlen > 128)
			rcvlen = 128;

		rcvd = recv(fd, buf, rcvlen, 0);

		if (rcvd < 0 && (errno == EAGAIN ||
				errno == EWOULDBLOCK)) {
			rc = RC_WOULDBLOCK;
			break;
		}

		if (rcvd <= 0) {
			if (errno == EINTR)
				continue;

			perror("recv");
			rc = RC_CONNBROKEN;
			goto out;
		}

		nr->data.read.read_discard.discarded -= rcvd;
	}

out:
	if (rc != RC_WOULDBLOCK) {
		bzero(&(nr->data.read.read_discard),
				sizeof(nr->data.read.read_discard));
	}

	return rc;
}

int encode_len(char *buf, int buflen, __u32 len)
{
	if (unlikely(buf == 0))
		LIBCOR_ASSERT_ERR();
	if (unlikely(buflen < 4))
		LIBCOR_ASSERT_ERR();

	if (len < 128) {
		buf[0] = (__u8) len;
		return 1;
	} else if (len < 16512) {
		__u16 len_be = htons(len - 128);
		char *len_p = (char *) &len_be;

		buf[0] = len_p[0] + 128;
		buf[1] = len_p[1];
		return 2;
	} else if (len < 1073758336) {
		__u32 len_be = htonl(len - 16512);
		char *len_p = (char *) &len_be;

		buf[0] = len_p[0] + 192;
		buf[1] = len_p[1];
		buf[2] = len_p[2];
		buf[3] = len_p[3];
		return 4;
	} else {
		return -1;
	}
}

int decode_len(char *buf, int buflen, __u32 *len)
{
	__u8 b0 = (__u8) buf[0];

	*len = 0;

	if (buflen >= 1 && b0 < 128) {
		*len = (__u8) buf[0];
		return 1;
	} else if (buflen >= 2 && b0 >= 128 && b0 < 192) {
		((char *) len)[0] = buf[0] - 128;
		((char *) len)[1] = buf[1];

		*len = ntohs(*len) + 128;
		return 2;
	} else if (buflen >= 4 && b0 >= 192) {
		((char *) len)[0] = buf[0] - 192;
		((char *) len)[1] = buf[1];
		((char *) len)[2] = buf[2];
		((char *) len)[3] = buf[3];

		*len = ntohl(*len) + 16512;
		return 4;
	} else {
		return 0;
	}
}

static int read_len(int fd, struct libcor_nonblock_resumeinfo *nr,
		__u32 *len, __u32 *maxread)
{
	int rc = RC_CONNBROKEN;

	if (unlikely(nr->type != RESUME_TYPE_READ))
		LIBCOR_ASSERT_ERR();

	if (unlikely(nr->data.read.read_len.state != 0 && (
			nr->data.read.read_len.fd != fd ||
			nr->data.read.read_len.len != len ||
			nr->data.read.read_len.maxread != maxread)))
		LIBCOR_ASSERT_ERR();

	if (sizeof(nr->data.read.read_len.buf) != 4)
		LIBCOR_ASSERT_ERR();

	if (nr->data.read.read_len.state == 1) {
		goto state_1;
	} else if (unlikely(nr->data.read.read_len.state != 0)) {
		LIBCOR_ASSERT_ERR();
	}

	nr->data.read.read_len.fd = fd;
	nr->data.read.read_len.len = len;
	nr->data.read.read_len.maxread = maxread;
	bzero(&(nr->data.read.read_len.buf[0]), 4);
	nr->data.read.read_len.read = 0;


	while(1) {
		nr->data.read.read_len.state = 1;
state_1:
		if (nr->data.read.read_len.read >= 4) {
			printf("error in readlen: read to high\n");
			rc = RC_CONNBROKEN;
			goto out;
		}

		rc = read_fully(fd, nr, &(nr->data.read.read_len.buf[0]) +
				nr->data.read.read_len.read, 1, maxread);
		if (rc != RC_OK)
			return rc;

		nr->data.read.read_len.read++;

		rc = decode_len(&(nr->data.read.read_len.buf[0]),
				nr->data.read.read_len.read, len);
		if (rc > 0) {
			if (unlikely(rc < nr->data.read.read_len.read)) {
				printf("error in readlen: decode_len has not "
						"consumed the whole buffer\n");
				rc = RC_CONNBROKEN;
				goto out;
			}
			rc = RC_OK;
			break;
		}
	}

out:
	if (rc != RC_WOULDBLOCK) {
		bzero(&(nr->data.read.read_len),
				sizeof(nr->data.read.read_len));
	}

	return rc;
}

static int _send_cmd(int fd, struct libcor_nonblock_resumeinfo *nr, __u16 cmd)
{
	char buf[6];
	int rc;
	__u32 hdrlen = 0;


	if (unlikely(nr->type != RESUME_TYPE_WRITE || nr->data.write.fd != fd))
		LIBCOR_ASSERT_ERR();

	cmd = htons(cmd);
	buf[0] = ((char *) &cmd)[0];
	buf[1] = ((char *) &cmd)[1];

	rc = encode_len(&(buf[2]), 4, nr->data.write.len);
	if (rc <= 0 || rc > 4)
		LIBCOR_ASSERT_ERR();

	if (sizeof(nr->data.write.buf) != WRITE_BUF_SIZE)
		LIBCOR_ASSERT_ERR();

	hdrlen = 2 + ((__u32) rc);

	if (unlikely(nr->data.write.len + hdrlen < nr->data.write.len ||
			nr->data.write.len + hdrlen > WRITE_BUF_SIZE))
		LIBCOR_ASSERT_ERR();

	memmove(&(nr->data.write.buf[hdrlen]), &(nr->data.write.buf[0]),
			nr->data.write.len);
	memcpy(&(nr->data.write.buf[0]), &(buf[0]), hdrlen);
	nr->data.write.len += hdrlen;

	return resume_send(fd, nr);
}

static int send_cmd(int fd, struct libcor_nonblock_resumeinfo *nr,
		__u16 cmd, char *buf, __u32 len)
{
	if (unlikely(nr->type != RESUME_TYPE_NONE))
		LIBCOR_ASSERT_ERR();

	bzero(nr, sizeof(struct libcor_nonblock_resumeinfo));
	nr->type = RESUME_TYPE_WRITE;
	nr->data.write.fd = fd;

	if (sizeof(nr->data.write.buf) != WRITE_BUF_SIZE)
		LIBCOR_ASSERT_ERR();

	if (unlikely(WRITE_BUF_SIZE < len))
		LIBCOR_ASSERT_ERR();

	if (len != 0) {
		memcpy(&(nr->data.write.buf[0]), buf, len);
		nr->data.write.len = len;
	}

	return _send_cmd(fd, nr, cmd);
}

int read_resp_nonblock(int fd, int expect_bindata,
		struct libcor_nonblock_resumeinfo *nr)
{
	int rc;

	struct libcor_nonblock_resumeinfo_resp *nr_resp =
			&(nr->data.read.funcdata.resp);

	if (nr->type == RESUME_TYPE_NONE) {
		bzero(nr, sizeof(struct libcor_nonblock_resumeinfo));
		nr->type = RESUME_TYPE_READ;
		nr->data.read.functype = RESUME_READ_FUNC_RESP;
		nr_resp->fd = fd;
	} else if (unlikely(nr->type != RESUME_TYPE_READ ||
			nr->data.read.functype != RESUME_READ_FUNC_RESP)) {
		LIBCOR_ASSERT_ERR();
	} else if (unlikely(nr_resp->fd != fd)) {
		LIBCOR_ASSERT_ERR();
	} else {
		__u8 state = nr_resp->state;

		if (state == 1) {
			goto state_1;
		} else if (unlikely(state != 0)) {
			LIBCOR_ASSERT_ERR();
		}
	}

	nr_resp->respcode = 0;
	rc = read_fully(fd, nr, (char *) &(nr_resp->respcode), 1, 0);

	if (rc != RC_OK)
		return bzero_nr_iffinished(nr, rc);

	//printf("read_resp: respcode = %d\n", nr_resp->respcode);

	if (nr_resp->respcode == CDR_EXECFAILED) {
		/* printf("crd_execfailed\n"); */

		nr_resp->reasoncode = 0;

		nr_resp->state = 1;
state_1:
		rc = read_fully(fd, nr, (char *) &(nr_resp->reasoncode), 2, 0);
		if (rc != RC_OK)
			return bzero_nr_iffinished(nr, rc);

		nr_resp->reasoncode = ntohs(nr_resp->reasoncode);

		printf("execfailed: reasoncode = %d\n", (__s32)
				nr_resp->reasoncode);
	}

	if (expect_bindata) {
		if (nr_resp->respcode == CDR_EXECOK)
			printf("execfailed: received execok, expected execok_bindata\n");
		else if (nr_resp->respcode == CDR_EXECOK_BINDATA)
			return bzero_nr_iffinished(nr, RC_OK);
	} else {
		eue CDR_EXECOK_BINDATA
		if (nr_resp->respcode == CDR_EXECOK)
			return bzero_nr_iffinished(nr, RC_OK);
		else if (nr_resp->respcode == CDR_EXECOK_BINDATA)
			printf("execfailed: received execok_bindata, expected execok\n");
	}

	return bzero_nr_iffinished(nr, RC_CONNBROKEN);
}

int read_resp(int fd, int expect_bindata)
{
	int rc = RC_WOULDBLOCK;
	struct libcor_nonblock_resumeinfo nr;
	bzero(&nr, sizeof(struct libcor_nonblock_resumeinfo));

	rc = read_resp_nonblock(fd, expect_bindata, &nr);
	if (unlikely(rc == RC_WOULDBLOCK)) {
		LIBCOR_ASSERT_ERR();
		return bzero_nr_iffinished(&nr, RC_CONNBROKEN);
	}
	return rc;
}

#warning todo replace LIBCOR_ASSERT_ERR
int send_connect_neigh_nonblock(int fd, struct libcor_nonblock_resumeinfo *nr,
		__be64 addr)
{
	__u32 hdrlen = 0;
	int rc;

	if (unlikely(nr->type != RESUME_TYPE_NONE))
		LIBCOR_ASSERT_ERR();

	bzero(nr, sizeof(struct libcor_nonblock_resumeinfo));
	nr->type = RESUME_TYPE_WRITE;
	nr->data.write.fd = fd;

	if (sizeof(nr->data.write.buf) != WRITE_BUF_SIZE)
		LIBCOR_ASSERT_ERR();

	if (unlikely(WRITE_BUF_SIZE < 8))
		LIBCOR_ASSERT_ERR();

	memcpy(&(nr->data.write.buf[0]), (char *) &addr, 8);
	nr->data.write.len = 8;

	if (unlikely(nr->data.write.len > WRITE_BUF_SIZE))
		LIBCOR_ASSERT_ERR();

	return _send_cmd(fd, nr, CD_CONNECT_NB);
}

int send_connect_neigh(int fd, __be64 addr)
{
	int rc = RC_WOULDBLOCK;
	struct libcor_nonblock_resumeinfo nr;
	bzero(&nr, sizeof(struct libcor_nonblock_resumeinfo));

	rc = send_connect_neigh_nonblock(fd, &nr, addr);
	if (unlikely(rc == RC_WOULDBLOCK)) {
		LIBCOR_ASSERT_ERR();
		return bzero_nr_iffinished(&nr, RC_CONNBROKEN);
	}
	return rc;
}

int send_connect_port_nonblock(int fd,
		struct libcor_nonblock_resumeinfo *nr, __be32 port)
{
	char *p_port = (char *) &port;
	char cmd[6];
	cmd[0] = L4PROTO_STREAM / 256;
	cmd[1] = L4PROTO_STREAM % 256;
	cmd[2] = p_port[0];
	cmd[3] = p_port[1];
	cmd[4] = p_port[2];
	cmd[5] = p_port[3];
	return send_cmd(fd, nr, CD_CONNECT_PORT, &(cmd[0]), 6);
}

int send_connect_port(int fd, __be32 port)
{
	int rc = RC_WOULDBLOCK;
	struct libcor_nonblock_resumeinfo nr;
	bzero(&nr, sizeof(struct libcor_nonblock_resumeinfo));

	rc = send_connect_port_nonblock(fd, &nr, port);
	if (unlikely(rc == RC_WOULDBLOCK)) {
		LIBCOR_ASSERT_ERR();
		return bzero_nr_iffinished(&nr, RC_CONNBROKEN);
	}
	return rc;
}

int send_list_services_nonblock(int fd, struct libcor_nonblock_resumeinfo *nr)
{
	return send_cmd(fd, nr, CD_LIST_SERVICES | CD_CONTINUE_ON_ERROR_FLAG,
			0, 0);
}

int send_list_services(int fd)
{
	int rc = RC_WOULDBLOCK;
	struct libcor_nonblock_resumeinfo nr;
	bzero(&nr, sizeof(struct libcor_nonblock_resumeinfo));

	rc = send_list_services_nonblock(fd, &nr);
	if (unlikely(rc == RC_WOULDBLOCK)) {
		LIBCOR_ASSERT_ERR();
		return bzero_nr_iffinished(&nr, RC_CONNBROKEN);
	}
	return rc;
}

int read_service_list_nonblock(int fd, struct libcor_nonblock_resumeinfo *nr,
		void *ptr,
		void (*init)(void *ptr, __u32 numservices),
		void (*next_service)(void *ptr, __be32 port))
{
	int rc;

	struct libcor_nonblock_resumeinfo_servicelist *nr_sl =
			&(nr->data.read.funcdata.servicelist);

	if (nr->type == RESUME_TYPE_NONE) {
		bzero(nr, sizeof(struct libcor_nonblock_resumeinfo));
		nr->type = RESUME_TYPE_READ;
		nr->data.read.functype = RESUME_READ_FUNC_SERVICELIST;
		nr_sl->fd = fd;
		nr_sl->ptr = ptr;
		nr_sl->init = init;
		nr_sl->next_service = next_service;
	} else if (unlikely(nr->type != RESUME_TYPE_READ ||
			nr->data.read.functype !=
			RESUME_READ_FUNC_SERVICELIST)) {
		LIBCOR_ASSERT_ERR();
	} else if (unlikely(nr_sl->fd != fd ||
			nr_sl->ptr != ptr ||
			nr_sl->init != init ||
			nr_sl->next_service != next_service)) {
		LIBCOR_ASSERT_ERR();
	} else {
		__u8 state = nr_sl->state;
		if (state == 1) {
			goto state_1;
		} else if (state == 2) {
			goto state_2;
		} else if (unlikely(state != 0)) {
			LIBCOR_ASSERT_ERR();
		}
	}

	rc = read_len(fd, nr, &(nr_sl->len), 0);
	if (rc != RC_OK)
		return bzero_nr_iffinished(nr, rc);

	nr_sl->state = 1;
state_1:
	rc = read_len(fd, nr, &(nr_sl->numservices), &(nr_sl->len));
	if (rc != RC_OK)
		return bzero_nr_iffinished(nr, rc);

	init(ptr, nr_sl->numservices);

	for(nr_sl->q=0; nr_sl->q < nr_sl->numservices; nr_sl->q++) {
		nr_sl->port = 0;

		nr_sl->state = 2;
state_2:
		rc = read_fully(fd, nr, (char *) &(nr_sl->port), 4,
				&(nr_sl->len));
		if (rc != RC_OK)
			return bzero_nr_iffinished(nr, rc);

		next_service(ptr, nr_sl->port);
		nr_sl->port = 0;
	}

	return bzero_nr_iffinished(nr, rc);
}

int read_service_list(int fd, void *ptr,
		void (*init)(void *ptr, __u32 numservices),
		void (*next_service)(void *ptr, __be32 port))
{
	int rc = RC_WOULDBLOCK;
	struct libcor_nonblock_resumeinfo nr;
	bzero(&nr, sizeof(struct libcor_nonblock_resumeinfo));

	rc = read_service_list_nonblock(fd, &nr, ptr, init, next_service);
	if (unlikely(rc == RC_WOULDBLOCK)) {
		LIBCOR_ASSERT_ERR();
		return bzero_nr_iffinished(&nr, RC_CONNBROKEN);
	}
	return rc;
}


int send_list_neigh_nonblock(int fd, struct libcor_nonblock_resumeinfo *nr)
{
	return send_cmd(fd, nr, CD_LIST_NEIGH | CD_CONTINUE_ON_ERROR_FLAG,
			0, 0);
}

int send_list_neigh(int fd)
{
	int rc = RC_WOULDBLOCK;
	struct libcor_nonblock_resumeinfo nr;
	bzero(&nr, sizeof(struct libcor_nonblock_resumeinfo));

	rc = send_list_neigh_nonblock(fd, &nr);
	if (unlikely(rc == RC_WOULDBLOCK)) {
		LIBCOR_ASSERT_ERR();
		return bzero_nr_iffinished(&nr, RC_CONNBROKEN);
	}
	return rc;
}

static int field_present(struct listneigh_field *fields, __u32 numfields,
		__u16 field)
{
	__u64 u;
	for(u=0;u<numfields;u++) {
		if (fields[u].field == field)
			return 1;
	}
	return 0;
}

int read_neigh_list_nonblock(int fd, struct libcor_nonblock_resumeinfo *nr,
		void *ptr,
		void (*init)(void *ptr, __u32 numneigh),
		void (*next_neigh)(void *ptr, __be64 addr))
{
	int rc;

	struct libcor_nonblock_resumeinfo_neighlist *nr_nl =
			&(nr->data.read.funcdata.neighlist);

	if (nr->type == RESUME_TYPE_NONE) {
		bzero(nr, sizeof(struct libcor_nonblock_resumeinfo));
		nr->type = RESUME_TYPE_READ;
		nr->data.read.functype = RESUME_READ_FUNC_NEIGHLIST;
		nr_nl->fd = fd;
		nr_nl->ptr = ptr;
		nr_nl->init = init;
		nr_nl->next_neigh = next_neigh;
	} else if (unlikely(nr->type != RESUME_TYPE_READ ||
			nr->data.read.functype != RESUME_READ_FUNC_NEIGHLIST)) {
		LIBCOR_ASSERT_ERR();
	} else if (unlikely(nr_nl->fd != fd ||
			nr_nl->ptr != ptr ||
			nr_nl->init != init ||
			nr_nl->next_neigh != next_neigh)) {
		LIBCOR_ASSERT_ERR();
	} else {
		__u8 state = nr_nl->state;
		if (state == 1) {
			goto state_1;
		} else if (state == 2) {
			goto state_2;
		} else if (state == 3) {
			goto state_3;
		} else if (state == 4) {
			goto state_4;
		} else if (state == 5) {
			goto state_5;
		} else if (state == 6) {
			goto state_6;
		} else if (state == 7) {
			goto state_7;
		} else if (state == 8) {
			goto state_8;
		} else if (unlikely(state != 0)) {
			LIBCOR_ASSERT_ERR();
		}
	}

	rc = read_len(fd, nr, &(nr_nl->len), 0);
	if (rc != RC_OK)
		return bzero_nr_iffinished(nr, rc);

	nr_nl->state = 1;
state_1:
	rc = read_len(fd, nr, &(nr_nl->numneighs), &(nr_nl->len));
	if (rc != RC_OK)
		return bzero_nr_iffinished(nr, rc);

	nr_nl->state = 2;
state_2:
	rc = read_len(fd, nr, &(nr_nl->numfields), &(nr_nl->len));
	if (rc != RC_OK)
		return bzero_nr_iffinished(nr, rc);

	if (unlikely(nr_nl->numfields > NEIGHLIST_MAX_FIELDS))
		return bzero_nr_iffinished(nr, RC_CONNBROKEN);

	for(nr_nl->u=0; nr_nl->u < nr_nl->numfields; nr_nl->u++) {
		nr_nl->fields[nr_nl->u].field = 0;

		nr_nl->state = 3;
state_3:
		rc = read_fully(fd, nr,
				(char *) &(nr_nl->fields[nr_nl->u].field),
				2, &(nr_nl->len));
		if (rc != RC_OK)
			return bzero_nr_iffinished(nr, rc);
		nr_nl->fields[nr_nl->u].field =
				ntohs(nr_nl->fields[nr_nl->u].field);

		nr_nl->state = 4;
state_4:
		nr_nl->fieldlen = 0;
		rc = read_len(fd, nr, &(nr_nl->fieldlen), &(nr_nl->len));
		if (rc != RC_OK)
			return bzero_nr_iffinished(nr, rc);

		if (unlikely(nr_nl->fieldlen > 65535))
			return bzero_nr_iffinished(nr, RC_CONNBROKEN);

		nr_nl->fields[nr_nl->u].len = (__u16) nr_nl->fieldlen;
		nr_nl->fieldlen = 0;
	}

	if (unlikely(field_present(nr_nl->fields, nr_nl->numfields,
			LIST_NEIGH_FIELD_ADDR) == 0))
		return bzero_nr_iffinished(nr, RC_CONNBROKEN);

	init(ptr, nr_nl->numneighs);

	for (nr_nl->u=0; nr_nl->u < nr_nl->numneighs; nr_nl->u++) {
		nr_nl->addr = 0;
		nr_nl->latency = 0;

		for(nr_nl->v=0; nr_nl->v < nr_nl->numfields; nr_nl->v++) {
			nr_nl->fieldlen = nr_nl->fields[nr_nl->v].len;
			if (nr_nl->fieldlen == 0) {
				nr_nl->state = 5;
state_5:
				rc = read_len(fd, nr, &(nr_nl->fieldlen),
						&(nr_nl->len));
				if (rc != RC_OK)
					return bzero_nr_iffinished(nr, rc);
			}

			if (nr_nl->fieldlen > nr_nl->len)
				return bzero_nr_iffinished(nr, RC_CONNBROKEN);

			nr_nl->len -= nr_nl->fieldlen;

			if (field_present(nr_nl->fields, nr_nl->v,
					nr_nl->fields[nr_nl->v].field)) {
				goto discard_field;
			} else if (nr_nl->fields[nr_nl->v].field ==
					LIST_NEIGH_FIELD_ADDR) {
				if (unlikely(nr_nl->fieldlen != 8))
					goto discard_field;

				ASSERT(sizeof(nr_nl->addr) == 8);

				nr_nl->state = 6;
state_6:
				rc = read_fully(fd, nr, (char *) &(nr_nl->addr),
						8, &(nr_nl->fieldlen));
				if (rc != RC_OK)
					return bzero_nr_iffinished(nr, rc);

				if (unlikely(ntohll(nr_nl->addr) == 0))
					return RC_CONNBROKEN;
			} else if (nr_nl->fields[nr_nl->v].field ==
					LIST_NEIGH_FIELD_LATENCY) {
				nr_nl->latency = 0;

				nr_nl->state = 7;
state_7:
				rc = read_fully(fd, nr, &nr_nl->latency, 1,
						&(nr_nl->fieldlen));
				if (rc != RC_OK)
					return bzero_nr_iffinished(nr, rc);

				printf("latency %d\n", (int) nr_nl->latency);
			}

discard_field:
			if (nr_nl->fieldlen > 0) {
				nr_nl->state = 8;
state_8:
				rc = read_discard(fd, nr, nr_nl->fieldlen);
				if (rc != RC_OK)
					return bzero_nr_iffinished(nr, rc);
			}

			nr_nl->fieldlen = 0;
		}

		next_neigh(ptr, nr_nl->addr);
		nr_nl->addr = 0;
		nr_nl->latency = 0;
	}

	return bzero_nr_iffinished(nr, rc);
}

int read_neigh_list(int fd, void *ptr,
		void (*init)(void *ptr, __u32 numneighs),
		void (*next_neigh)(void *ptr, __be64 addr))
{
	int rc = RC_WOULDBLOCK;
	struct libcor_nonblock_resumeinfo nr;
	bzero(&nr, sizeof(struct libcor_nonblock_resumeinfo));

	rc = read_neigh_list_nonblock(fd, &nr, ptr, init, next_neigh);
	if (unlikely(rc == RC_WOULDBLOCK)) {
		LIBCOR_ASSERT_ERR();
		return bzero_nr_iffinished(&nr, RC_CONNBROKEN);
	}
	return rc;
}

int pass_socket(int fd, __u64 cookie)
{
	int rc;

	rc = setsockopt(fd, SOL_COR, COR_PASS_ON_CLOSE, &cookie, 8);
	if (rc != 0) {
		perror("pass_socket");
		return RC_CONNBROKEN;
	}

	close(fd);

	return RC_OK;
}

static int send_rdsock_cmd(int fd, struct libcor_nonblock_resumeinfo *nr,
		__u32 cmd, char *data, __u32 datalen)
{
	__u32 be_cmd = htonl(cmd);
	__u32 be_datalen = htonl(datalen);

	if (unlikely(nr->type != RESUME_TYPE_NONE))
		LIBCOR_ASSERT_ERR();

	bzero(nr, sizeof(struct libcor_nonblock_resumeinfo));
	nr->type = RESUME_TYPE_WRITE;
	nr->data.write.fd = fd;

	if (sizeof(nr->data.write.buf) != WRITE_BUF_SIZE)
		LIBCOR_ASSERT_ERR();

	if (unlikely(datalen + 8 < datalen || WRITE_BUF_SIZE < (datalen + 8)))
		LIBCOR_ASSERT_ERR();

	memcpy(&(nr->data.write.buf[0]), (char *) &be_cmd, 4);
	memcpy(&(nr->data.write.buf[4]), (char *) &be_datalen, 4);
	memcpy(&(nr->data.write.buf[8]), data, datalen);
	nr->data.write.len = datalen + 8;

	return resume_send(fd, nr);
}

int send_rdsock_version_nonblock(int fd,
		struct libcor_nonblock_resumeinfo *nr,
		__u32 version)
{
	char data[4];

	version = htonl(version);

	memcpy(&(data[0]), (char *) &version, 4);

	return send_rdsock_cmd(fd, nr, CRD_UTK_VERSION, &(data[0]), 4);
}

int send_rdsock_version(int fd, __u32 version)
{
	int rc = RC_WOULDBLOCK;
	struct libcor_nonblock_resumeinfo nr;
	bzero(&nr, sizeof(struct libcor_nonblock_resumeinfo));

	rc = send_rdsock_version_nonblock(fd, &nr, version);
	if (unlikely(rc == RC_WOULDBLOCK)) {
		LIBCOR_ASSERT_ERR();
		return bzero_nr_iffinished(&nr, RC_CONNBROKEN);
	}
	return rc;
}

int send_rdsock_up_nonblock(int fd, struct libcor_nonblock_resumeinfo *nr,
		int has_addr, __be64 addr)
{
	char data[16];

	__u64 flags = 0;

	__u32 addrlenbe;

	if (has_addr == 0 && addr != 0)
		return RC_CONNBROKEN;

	if (has_addr) {
		flags |= CRD_UTK_UP_FLAGS_ADDR;

		flags = htonll(flags);

		memcpy(&(data[0]), (char *) &flags, 8);
		memcpy(&(data[8]), (char *) &addr, 8);

		return send_rdsock_cmd(fd, nr, CRD_UTK_UP, &(data[0]), 16);
	} else {
		flags = htonll(flags);

		memcpy(&(data[0]), (char *) &flags, 8);

		return send_rdsock_cmd(fd, nr, CRD_UTK_UP, &(data[0]), 8);
	}
}

int send_rdsock_up(int fd, int has_addr, __be64 addr)
{
	int rc = RC_WOULDBLOCK;
	struct libcor_nonblock_resumeinfo nr;
	bzero(&nr, sizeof(struct libcor_nonblock_resumeinfo));

	rc = send_rdsock_up_nonblock(fd, &nr, has_addr, addr);
	if (unlikely(rc == RC_WOULDBLOCK)) {
		LIBCOR_ASSERT_ERR();
		return bzero_nr_iffinished(&nr, RC_CONNBROKEN);
	}
	return rc;
}

int send_rdsock_connecterror_nonblock(int fd,
		struct libcor_nonblock_resumeinfo *nr,
		__u64 cookie, __u32 error)
{
	char data[12];

	error = htonl(error);

	memcpy(&(data[0]), (char *) &cookie, 8);
	memcpy(&(data[8]), (char *) &error, 4);

	return send_rdsock_cmd(fd, nr, CRD_UTK_CONNECTERROR, &(data[0]), 12);
}

int send_rdsock_connecterror(int fd, __u64 cookie, __u32 error)
{
	int rc = RC_WOULDBLOCK;
	struct libcor_nonblock_resumeinfo nr;
	bzero(&nr, sizeof(struct libcor_nonblock_resumeinfo));

	rc = send_rdsock_connecterror_nonblock(fd, &nr, cookie, error);
	if (unlikely(rc == RC_WOULDBLOCK)) {
		LIBCOR_ASSERT_ERR();
		return bzero_nr_iffinished(&nr, RC_CONNBROKEN);
	}
	return rc;
}

int parse_rdsock_supported_versions(struct rdsock_cmd *cmd,
		__u32 *versionmin, __u32 *versionmax)
{
	if (cmd->cmddatalen != 8)
		return 1;

	memcpy((char *) versionmin, cmd->cmddata, 4);
	*versionmin = htonl(*versionmin);
	memcpy((char *) versionmax, cmd->cmddata + 4, 4);
	*versionmax = htonl(*versionmax);

	return 0;
}

int parse_rdsock_connect(void *ptr, struct rdsock_cmd *cmd,
		int (*proc_connect)(void *ptr, __u64 cookie,
		struct cor_sockaddr *addr, __u32 tos))
{
	__u64 cookie;
	struct cor_sockaddr addr;
	__u32 tos;

	if ((sizeof(struct cor_sockaddr) + 12) != cmd->cmddatalen)
		return 1;

	memcpy((char *) &cookie, cmd->cmddata, 8);

	memcpy((char *) &addr, cmd->cmddata + 8, sizeof(struct cor_sockaddr));

	memcpy((char *) &tos, cmd->cmddata + 8 + sizeof(struct cor_sockaddr),
			4);
	tos = htonl(tos);

	return proc_connect(ptr, cookie, &addr, tos);
}

int read_rdsock_cmd_nonblock(int fd,
		struct libcor_nonblock_resumeinfo *nr,
		struct rdsock_cmd *cmd)
{
	int rc;

	struct libcor_nonblock_resumeinfo_rdsockcmd *nr_rd =
			&(nr->data.read.funcdata.rdsock_cmd);

	bzero(cmd, sizeof(struct rdsock_cmd));

	if (nr->type == RESUME_TYPE_NONE) {
		bzero(nr, sizeof(struct libcor_nonblock_resumeinfo));
		nr->type = RESUME_TYPE_READ;
		nr->data.read.functype = RESUME_READ_FUNC_RDSOCK_CMD;
		nr_rd->fd = fd;
	} else if (unlikely(nr->type != RESUME_TYPE_READ ||
			nr->data.read.functype != RESUME_READ_FUNC_RDSOCK_CMD)){
		LIBCOR_ASSERT_ERR();
	} else if (unlikely(nr_rd->fd != fd)) {
		LIBCOR_ASSERT_ERR();
	} else {
		__u8 state = nr_rd->state;

		if (state == 1) {
			goto state_1;
		} else if (state == 2) {
			goto state_2;
		} else if (unlikely(state != 0)) {
			LIBCOR_ASSERT_ERR();
		}
	}

	if (sizeof(nr_rd->buf) != 8)
		LIBCOR_ASSERT_ERR();

	rc = read_fully(fd, nr, (char *) &(nr_rd->buf[0]), 8, 0);
	if (rc != RC_OK)
		return bzero_nr_iffinished(nr, rc);

	((char *) &(nr_rd->cmd))[0] = nr_rd->buf[0];
	((char *) &(nr_rd->cmd))[1] = nr_rd->buf[1];
	((char *) &(nr_rd->cmd))[2] = nr_rd->buf[2];
	((char *) &(nr_rd->cmd))[3] = nr_rd->buf[3];
	((char *) &(nr_rd->cmddatalen))[0] = nr_rd->buf[4];
	((char *) &(nr_rd->cmddatalen))[1] = nr_rd->buf[5];
	((char *) &(nr_rd->cmddatalen))[2] = nr_rd->buf[6];
	((char *) &(nr_rd->cmddatalen))[3] = nr_rd->buf[7];

	nr_rd->cmd = ntohl(nr_rd->cmd);
	nr_rd->cmddatalen = ntohl(nr_rd->cmddatalen);

	if (nr_rd->cmddatalen > 65536)
		goto discard;

	nr_rd->cmddata = malloc(nr_rd->cmddatalen);
	if (nr_rd->cmddata == 0)
		goto discard;

	nr_rd->state = 1;
state_1:
	rc = read_fully(fd, nr, nr_rd->cmddata, nr_rd->cmddatalen, 0);
	if (rc != RC_OK) {
		if (rc != RC_WOULDBLOCK) {
			free(nr_rd->cmddata);
			nr_rd->cmddata = 0;
		}
		return bzero_nr_iffinished(nr, rc);
	}

	cmd->cmd = nr_rd->cmd;
	cmd->cmddata = nr_rd->cmddata;
	cmd->cmddatalen = nr_rd->cmddatalen;

	if (0) {
discard:
		nr_rd->state = 2;
state_2:
		rc = read_discard(fd, nr, nr_rd->cmddatalen);
		if (rc != RC_OK)
			return bzero_nr_iffinished(nr, rc);
	}
	return bzero_nr_iffinished(nr, rc);
}

int read_rdsock_cmd(int fd, struct rdsock_cmd *cmd)
{
	int rc = RC_WOULDBLOCK;
	struct libcor_nonblock_resumeinfo nr;
	bzero(&nr, sizeof(struct libcor_nonblock_resumeinfo));

	rc = read_rdsock_cmd_nonblock(fd, &nr, cmd);
	if (unlikely(rc == RC_WOULDBLOCK)) {
		LIBCOR_ASSERT_ERR();
		return bzero_nr_iffinished(&nr, RC_CONNBROKEN);
	}
	return rc;
}
