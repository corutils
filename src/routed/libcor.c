/**
 *	Connection oriented routing user space utils
 *	Copyright (C) 2009-2019
 *	Authors:
 * 		Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#include <linux/types.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <arpa/inet.h>
#include <unistd.h>

#include "../utils.h"
#include "../cor.h"
#include "libcor.h"


static int bzero_nr_iffinished(struct libcor_nonblock_resumeinfo *nr, int rc)
{
	/*if (rc == RC_WOULDBLOCK) {
		printf("wouldblock\n");
	}*/

	if (rc != RC_WOULDBLOCK) {
		bzero(nr, sizeof(struct libcor_nonblock_resumeinfo));
	}
	return rc;
}


static const __u32 log_64_11_table[] = {0,
		64, 68, 73, 77, 82, 88, 93, 99, 106, 113, 120,
		128, 136, 145, 155, 165, 175, 187, 199, 212, 226, 240,
		256, 273, 290, 309, 329, 351, 374, 398, 424, 451, 481,
		512, 545, 581, 619, 659, 702, 747, 796, 848, 903, 961,
		1024, 1091, 1162, 1237, 1318, 1403, 1495, 1592, 1695, 1805,
				1923,
		2048, 2181, 2323, 2474, 2635, 2806, 2989, 3183, 3390, 3611,
				3846,
		4096, 4362, 4646, 4948, 5270, 5613, 5978, 6367, 6781, 7222,
				7692,
		8192, 8725, 9292, 9897, 10540, 11226, 11956, 12734, 13562,
				14444, 15383,
		16384, 17450, 18585, 19793, 21081, 22452, 23912, 25467, 27124,
				28888, 30767,
		32768, 34899, 37169, 39587, 42161, 44904, 47824, 50935, 54248,
				57776, 61534,
		65536, 69799, 74338, 79173, 84323, 89807, 95648, 101870, 108495,
				115552, 123068,
		131072, 139597, 148677, 158347, 168646, 179615, 191297, 203739,
				216991, 231104, 246135,
		262144, 279194, 297353, 316693, 337291, 359229, 382594, 407478,
				433981, 462208, 492270,
		524288, 558388, 594706, 633387, 674583, 718459, 765188, 814957,
				867962, 924415, 984540,
		1048576, 1116777, 1189413, 1266774, 1349166, 1436917, 1530376,
				1629913, 1735924, 1848831, 1969081,
		2097152, 2233553, 2378826, 2533547, 2698332, 2873834, 3060752,
				3259826, 3471849, 3697662, 3938162,
		4194304, 4467106, 4757652, 5067094, 5396664, 5747669, 6121503,
				6519652, 6943698, 7395323, 7876323,
		8388608, 8934212, 9515303, 10134189, 10793327, 11495337,
				12243006, 13039305, 13887396, 14790647,
				15752647,
		16777216, 17868424, 19030606, 20268378, 21586655, 22990674,
				24486013, 26078610, 27774791, 29581294,
				31505293,
		33554432, 35736849, 38061212, 40536755, 43173310, 45981349,
				48972026, 52157220, 55549582, 59162588,
				63010587,
		67108864, 71473698, 76122425, 81073510, 86346620, 91962698,
				97944052, 104314440, 111099165, 118325175,
				126021174,
		134217728, 142947395, 152244850, 162147020, 172693239,
				183925396, 195888104, 208628880, 222198329,
				236650351, 252042347,
		268435456, 285894791, 304489699, 324294041, 345386479,
				367850791, 391776208, 417257759, 444396658,
				473300701, 504084694,
		536870912, 571789581};

static inline __u32 __attribute__((const)) cor_dec_log_64_11(__u8 value)
{
	ASSERT(log_64_11_table[255] == 571789581);
	return log_64_11_table[value];
}

int resume_send(int fd, struct libcor_nonblock_resumeinfo *nr)
{
	int flags = 0;

	if (unlikely(nr->type != RESUME_TYPE_WRITE || nr->data.write.fd != fd))
		ASSERT_ERR();

	if (sizeof(nr->data.write.buf) != WRITE_BUF_SIZE ||
			unlikely(nr->data.write.len > WRITE_BUF_SIZE))
		ASSERT_ERR();

	if (nr->data.write.flush == RESUME_WRITE_FLUSH_NO) {
		flags = flags | MSG_MORE;
	} else if (nr->data.write.flush != RESUME_WRITE_FLUSH_YES) {
		ASSERT_ERR();
	}

	while (nr->data.write.totalsent < nr->data.write.len) {
		char *buf = &(nr->data.write.buf[0]);
		#warning todo use ssize_t (other places too?)
		int sent = send(fd, buf + nr->data.write.totalsent,
				nr->data.write.len - nr->data.write.totalsent,
				flags);

		if (sent < 0 && (errno == EAGAIN ||
				errno == EWOULDBLOCK))
			return bzero_nr_iffinished(nr, RC_WOULDBLOCK);

		if (sent <= 0) {
			if (errno == EINTR)
				continue;

			perror("send");
			return bzero_nr_iffinished(nr, RC_CONNBROKEN);
		}

		nr->data.write.totalsent += sent;
	}

	return bzero_nr_iffinished(nr, RC_OK);
}

static int read_fully(int fd, struct libcor_nonblock_resumeinfo *nr,
		char *buf, __u32 len, __u32 *maxread)
{
	int rc = RC_OK;

	if (unlikely(nr->type != RESUME_TYPE_READ))
		ASSERT_ERR();

	if (unlikely(nr->data.read.read_fully.state != 0 && (
			nr->data.read.read_fully.fd != fd ||
			nr->data.read.read_fully.buf != buf ||
			nr->data.read.read_fully.len != len ||
			nr->data.read.read_fully.maxread != maxread)))
		ASSERT_ERR();

	if (nr->data.read.read_fully.state == 1) {
		goto state_1;
	} else if (unlikely(nr->data.read.read_fully.state != 0)) {
		ASSERT_ERR();
	}

	nr->data.read.read_fully.fd = fd;
	nr->data.read.read_fully.buf = buf;
	nr->data.read.read_fully.len = len;
	nr->data.read.read_fully.maxread = maxread;
	nr->data.read.read_fully.totalread = 0;

	nr->data.read.read_fully.state = 1;
state_1:

	if (maxread != 0) {
		if (len > (*maxread)) {
			printf("error in read_fully: maxread reached\n");
			rc = RC_CONNBROKEN;
			goto out;
		}
		(*maxread) -= len;
	}

	while (len > nr->data.read.read_fully.totalread) {
		int rcvd = recv(fd, buf + nr->data.read.read_fully.totalread,
				len - nr->data.read.read_fully.totalread, 0);
		int u;

		if (rcvd < 0 && (errno == EAGAIN ||
				errno == EWOULDBLOCK)) {
			/* printf("wouldblock\n"); */
			rc = RC_WOULDBLOCK;
			goto out;
		}

		if (rcvd <= 0) {
			if (errno == EINTR)
				continue;

			perror("recv");
			rc = RC_CONNBROKEN;
			goto out;
		}

		/*printf("rcvd: %d:", rcvd);
		for(u=0;u<rcvd;u++) {
			printf(" %d, ", (__s32) ((__u8) buf[totalread+u]));
		}
		printf("\n");*/

		nr->data.read.read_fully.totalread += (__u32) rcvd;
	}

out:
	if (rc != RC_WOULDBLOCK) {
		bzero(&(nr->data.read.read_fully),
				sizeof(nr->data.read.read_fully));
	}

	return rc;
}

static int read_discard(int fd, struct libcor_nonblock_resumeinfo *nr,
		__u32 len)
{
	char buf[128];
	int rc = RC_OK;

	if (unlikely(nr->type != RESUME_TYPE_READ))
		ASSERT_ERR();

	if (unlikely(nr->data.read.read_discard.state != 0 && (
			nr->data.read.read_discard.fd != fd ||
			nr->data.read.read_discard.len != len)))
		ASSERT_ERR();

	if (nr->data.read.read_discard.state == 1) {
		goto state_1;
	} else if (unlikely(nr->data.read.read_discard.state != 0)) {
		ASSERT_ERR();
	}

	nr->data.read.read_discard.fd = fd;
	nr->data.read.read_discard.len = len;
	nr->data.read.read_discard.discarded = 0;


	nr->data.read.read_discard.state = 1;
state_1:

	while (len > 0) {
		int rcvd;

		__u32 rcvlen = len - nr->data.read.read_discard.discarded;
		if (rcvlen > 128)
			rcvlen = 128;

		rcvd = recv(fd, buf, rcvlen, 0);

		if (rcvd < 0 && (errno == EAGAIN ||
				errno == EWOULDBLOCK)) {
			rc = RC_WOULDBLOCK;
			break;
		}

		if (rcvd <= 0) {
			if (errno == EINTR)
				continue;

			perror("recv");
			rc = RC_CONNBROKEN;
			goto out;
		}

		nr->data.read.read_discard.discarded -= rcvd;
	}

out:
	if (rc != RC_WOULDBLOCK) {
		bzero(&(nr->data.read.read_discard),
				sizeof(nr->data.read.read_discard));
	}

	return rc;
}

int encode_len(char *buf, int buflen, __u32 len)
{
	if (unlikely(buf == 0))
		ASSERT_ERR();
	if (unlikely(buflen < 4))
		ASSERT_ERR();

	if (len < 128) {
		buf[0] = (__u8) len;
		return 1;
	} else if (len < 16512) {
		__u16 len_be = htons(len - 128);
		char *len_p = (char *) &len_be;

		buf[0] = len_p[0] + 128;
		buf[1] = len_p[1];
		return 2;
	} else if (len < 1073758336) {
		__u32 len_be = htonl(len - 16512);
		char *len_p = (char *) &len_be;

		buf[0] = len_p[0] + 192;
		buf[1] = len_p[1];
		buf[2] = len_p[2];
		buf[3] = len_p[3];
		return 4;
	} else {
		return -1;
	}
}

int decode_len(char *buf, int buflen, __u32 *len)
{
	__u8 b0 = (__u8) buf[0];

	*len = 0;

	if (buflen >= 1 && b0 < 128) {
		*len = (__u8) buf[0];
		return 1;
	} else if (buflen >= 2 && b0 >= 128 && b0 < 192) {
		((char *) len)[0] = buf[0] - 128;
		((char *) len)[1] = buf[1];

		*len = ntohs(*len) + 128;
		return 2;
	} else if (buflen >= 4 && b0 >= 192) {
		((char *) len)[0] = buf[0] - 192;
		((char *) len)[1] = buf[1];
		((char *) len)[2] = buf[2];
		((char *) len)[3] = buf[3];

		*len = ntohl(*len) + 16512;
		return 4;
	} else {
		return 0;
	}
}

static int read_len(int fd, struct libcor_nonblock_resumeinfo *nr,
		__u32 *len, __u32 *maxread)
{
	int rc = RC_CONNBROKEN;

	if (unlikely(nr->type != RESUME_TYPE_READ))
		ASSERT_ERR();

	if (unlikely(nr->data.read.read_len.state != 0 && (
			nr->data.read.read_len.fd != fd ||
			nr->data.read.read_len.len != len ||
			nr->data.read.read_len.maxread != maxread)))
		ASSERT_ERR();

	if (sizeof(nr->data.read.read_len.buf) != 4)
		ASSERT_ERR();

	if (nr->data.read.read_len.state == 1) {
		goto state_1;
	} else if (unlikely(nr->data.read.read_len.state != 0)) {
		ASSERT_ERR();
	}

	nr->data.read.read_len.fd = fd;
	nr->data.read.read_len.len = len;
	nr->data.read.read_len.maxread = maxread;
	bzero(&(nr->data.read.read_len.buf[0]), 4);
	nr->data.read.read_len.read = 0;


	while(1) {
		nr->data.read.read_len.state = 1;
state_1:
		if (nr->data.read.read_len.read >= 4) {
			printf("error in readlen: read too high\n");
			rc = RC_CONNBROKEN;
			goto out;
		}

		rc = read_fully(fd, nr, &(nr->data.read.read_len.buf[0]) +
				nr->data.read.read_len.read, 1, maxread);
		if (rc != RC_OK)
			return rc;

		nr->data.read.read_len.read++;

		rc = decode_len(&(nr->data.read.read_len.buf[0]),
				nr->data.read.read_len.read, len);
		if (rc > 0) {
			if (unlikely(rc < nr->data.read.read_len.read)) {
				printf("error in readlen: decode_len has not "
						"consumed the whole buffer\n");
				rc = RC_CONNBROKEN;
				goto out;
			}
			rc = RC_OK;
			break;
		}
	}

out:
	if (rc != RC_WOULDBLOCK) {
		bzero(&(nr->data.read.read_len),
				sizeof(nr->data.read.read_len));
	}

	return rc;
}

static int _send_cmd(int fd, struct libcor_nonblock_resumeinfo *nr, __u16 cmd,
		int flush)
{
	char buf[6];
	int rc;
	__u32 hdrlen = 0;


	if (unlikely(nr->type != RESUME_TYPE_WRITE || nr->data.write.fd != fd))
		ASSERT_ERR();

	if (unlikely((cmd & CD_NOPARAM_FLAG) != 0))
		ASSERT_ERR();

	if (nr->data.write.len == 0) {
		cmd = cmd | CD_NOPARAM_FLAG;

		cmd = htons(cmd);
		buf[0] = ((char *) &cmd)[0];
		buf[1] = ((char *) &cmd)[1];

		hdrlen = 2;
	} else {
		cmd = htons(cmd);
		buf[0] = ((char *) &cmd)[0];
		buf[1] = ((char *) &cmd)[1];

		rc = encode_len(&(buf[2]), 4, nr->data.write.len);
		if (rc <= 0 || rc > 4)
			ASSERT_ERR();

		hdrlen = 2 + ((__u32) rc);
	}

	if (sizeof(nr->data.write.buf) != WRITE_BUF_SIZE)
		ASSERT_ERR();

	if (unlikely(nr->data.write.len + hdrlen < nr->data.write.len ||
			nr->data.write.len + hdrlen > WRITE_BUF_SIZE))
		ASSERT_ERR();

	memmove(&(nr->data.write.buf[hdrlen]), &(nr->data.write.buf[0]),
			nr->data.write.len);
	memcpy(&(nr->data.write.buf[0]), &(buf[0]), hdrlen);
	nr->data.write.len += hdrlen;

	if (flush)
		nr->data.write.flush = RESUME_WRITE_FLUSH_YES;
	else
		nr->data.write.flush = RESUME_WRITE_FLUSH_NO;

	return resume_send(fd, nr);
}

static int send_cmd(int fd, struct libcor_nonblock_resumeinfo *nr,
		__u16 cmd, char *buf, __u32 len, int flush)
{
	if (unlikely(nr->type != RESUME_TYPE_NONE))
		ASSERT_ERR();

	bzero(nr, sizeof(struct libcor_nonblock_resumeinfo));
	nr->type = RESUME_TYPE_WRITE;
	nr->data.write.fd = fd;

	if (sizeof(nr->data.write.buf) != WRITE_BUF_SIZE)
		ASSERT_ERR();

	if (unlikely(WRITE_BUF_SIZE < len))
		ASSERT_ERR();

	if (len != 0) {
		memcpy(&(nr->data.write.buf[0]), buf, len);
		nr->data.write.len = len;
	}

	return _send_cmd(fd, nr, cmd, flush);
}

int read_resp_nonblock(int fd, int expect_bindata,
		struct libcor_nonblock_resumeinfo *nr,
		__u8 *reasoncode, __u8 *reasoncode_set, __u8 *bindata_noresp)
{
	int rc;

	struct libcor_nonblock_resumeinfo_resp *nr_resp =
			&(nr->data.read.funcdata.resp);

	if (nr->type == RESUME_TYPE_NONE) {
		bzero(nr, sizeof(struct libcor_nonblock_resumeinfo));
		nr->type = RESUME_TYPE_READ;
		nr->data.read.functype = RESUME_READ_FUNC_RESP;
		nr_resp->fd = fd;
	} else if (unlikely(nr->type != RESUME_TYPE_READ ||
			nr->data.read.functype != RESUME_READ_FUNC_RESP)) {
		ASSERT_ERR();
	} else if (unlikely(nr_resp->fd != fd)) {
		ASSERT_ERR();
	} else {
		__u8 state = nr_resp->state;

		if (state == 1) {
			goto state_1;
		} else if (unlikely(state != 0)) {
			ASSERT_ERR();
		}
	}

	nr_resp->respcode = 0;
	rc = read_fully(fd, nr, (char *) &(nr_resp->respcode), 1, 0);

	if (rc != RC_OK)
		return bzero_nr_iffinished(nr, rc);

	//printf("read_resp: respcode = %d\n", nr_resp->respcode);

	if (nr_resp->respcode == CDR_EXECFAILED) {
		/* printf("crd_execfailed\n"); */

		nr_resp->reasoncode = 0;

		nr_resp->state = 1;
state_1:
		rc = read_fully(fd, nr, (char *) &(nr_resp->reasoncode), 2, 0);
		if (rc != RC_OK)
			return bzero_nr_iffinished(nr, rc);

		nr_resp->reasoncode = ntohs(nr_resp->reasoncode);

		if (reasoncode != 0)
			*reasoncode = nr_resp->reasoncode;
		if (reasoncode_set != 0)
			*reasoncode_set = 1;

		printf("execfailed: reasoncode = %d\n", (__s32)
				nr_resp->reasoncode);
	}

	if (expect_bindata) {
		if (nr_resp->respcode == CDR_EXECOK) {
			printf("execfailed: received execok, expected execok_bindata\n");
		} else if (nr_resp->respcode == CDR_EXECOK_BINDATA) {
			*bindata_noresp = 0;
			return bzero_nr_iffinished(nr, RC_OK);
		} else if (nr_resp->respcode == CDR_EXECOK_BINDATA_NORESP) {
			*bindata_noresp = 1;
			return bzero_nr_iffinished(nr, RC_OK);
		}
	} else {
		if (nr_resp->respcode == CDR_EXECOK)
			return bzero_nr_iffinished(nr, RC_OK);
		else if (nr_resp->respcode == CDR_EXECOK_BINDATA ||
				nr_resp->respcode == CDR_EXECOK_BINDATA_NORESP)
			printf("execfailed: received execok_bindata, expected execok\n");
	}

	return bzero_nr_iffinished(nr, RC_CONNBROKEN);
}

int read_resp(int fd, int expect_bindata, __u8 *bindata_noresp)
{
	int rc = RC_WOULDBLOCK;
	struct libcor_nonblock_resumeinfo nr;
	bzero(&nr, sizeof(struct libcor_nonblock_resumeinfo));

	rc = read_resp_nonblock(fd, expect_bindata, &nr, 0, 0, bindata_noresp);
	if (unlikely(rc == RC_WOULDBLOCK)) {
		ASSERT_ERR();
		return bzero_nr_iffinished(&nr, RC_CONNBROKEN);
	}
	return rc;
}

int read_resp_bin_nonblock(int fd, struct libcor_nonblock_resumeinfo *nr,
		char **resp_buf, __u32 *resp_len)
{
	int rc;

	struct libcor_nonblock_resumeinfo_resp_bin *nr_rb =
			&(nr->data.read.funcdata.resp_bin);

	if (nr->type == RESUME_TYPE_NONE) {
		bzero(nr, sizeof(struct libcor_nonblock_resumeinfo));
		nr->type = RESUME_TYPE_READ;
		nr->data.read.functype = RESUME_READ_FUNC_RESP_BIN;
		nr_rb->fd = fd;
	} else if (unlikely(nr->type != RESUME_TYPE_READ ||
			nr->data.read.functype !=
			RESUME_READ_FUNC_RESP_BIN)) {
		ASSERT_ERR();
	} else if (unlikely(nr_rb->fd != fd)) {
		ASSERT_ERR();
	} else {
		__u8 state = nr_rb->state;
		if (state == 1) {
			goto state_1;
		} else if (unlikely(state != 0)) {
			ASSERT_ERR();
		}
	}

	rc = read_len(fd, nr, &(nr_rb->resp_len), 0);
	if (rc != RC_OK)
		return bzero_nr_iffinished(nr, rc);

	nr_rb->resp_buf = malloc(nr_rb->resp_len);
	bzero(nr_rb->resp_buf, nr_rb->resp_len);

	nr_rb->state = 1;
state_1:
	rc = read_fully(fd, nr, nr_rb->resp_buf, nr_rb->resp_len, 0);
	if (rc != RC_OK)
		return bzero_nr_iffinished(nr, rc);


	*resp_buf = nr_rb->resp_buf;
	*resp_len = nr_rb->resp_len;

	return bzero_nr_iffinished(nr, RC_OK);
}

int read_resp_bin(int fd, char **resp_buf, __u32 *resp_len)
{
	int rc = RC_WOULDBLOCK;
	struct libcor_nonblock_resumeinfo nr;
	bzero(&nr, sizeof(struct libcor_nonblock_resumeinfo));

	rc = read_resp_bin_nonblock(fd, &nr, resp_buf, resp_len);
	if (unlikely(rc == RC_WOULDBLOCK)) {
		ASSERT_ERR();
		return bzero_nr_iffinished(&nr, RC_CONNBROKEN);
	}
	return rc;
}


int send_connect_neigh_nonblock(int fd, struct libcor_nonblock_resumeinfo *nr,
		__be64 addr, int flush)
{
	__u32 hdrlen = 0;
	int rc;

	if (unlikely(nr->type != RESUME_TYPE_NONE))
		ASSERT_ERR();

	bzero(nr, sizeof(struct libcor_nonblock_resumeinfo));
	nr->type = RESUME_TYPE_WRITE;
	nr->data.write.fd = fd;

	if (sizeof(nr->data.write.buf) != WRITE_BUF_SIZE)
		ASSERT_ERR();

	if (unlikely(WRITE_BUF_SIZE < 8))
		ASSERT_ERR();

	memcpy(&(nr->data.write.buf[0]), (char *) &addr, 8);
	nr->data.write.len = 8;

	if (unlikely(nr->data.write.len > WRITE_BUF_SIZE))
		ASSERT_ERR();

	return _send_cmd(fd, nr, CD_CONNECT_NB, flush);
}

int send_connect_neigh(int fd, __be64 addr, int flush)
{
	int rc = RC_WOULDBLOCK;
	struct libcor_nonblock_resumeinfo nr;
	bzero(&nr, sizeof(struct libcor_nonblock_resumeinfo));

	rc = send_connect_neigh_nonblock(fd, &nr, addr, flush);
	if (unlikely(rc == RC_WOULDBLOCK)) {
		ASSERT_ERR();
		return bzero_nr_iffinished(&nr, RC_CONNBROKEN);
	}
	return rc;
}

int send_connect_port_nonblock(int fd,
                struct libcor_nonblock_resumeinfo *nr, __be32 port, int flush)
{
	char *p_port = (char *) &port;
	char cmd[6];
	cmd[0] = L4PROTO_STREAM / 256;
	cmd[1] = L4PROTO_STREAM % 256;
	cmd[2] = p_port[0];
	cmd[3] = p_port[1];
	cmd[4] = p_port[2];
	cmd[5] = p_port[3];
	return send_cmd(fd, nr, CD_CONNECT_PORT, &(cmd[0]), 6, flush);
}

int send_connect_port(int fd, __be32 port, int flush)
{
	int rc = RC_WOULDBLOCK;
	struct libcor_nonblock_resumeinfo nr;
	bzero(&nr, sizeof(struct libcor_nonblock_resumeinfo));

	rc = send_connect_port_nonblock(fd, &nr, port, flush);
	if (unlikely(rc == RC_WOULDBLOCK)) {
		ASSERT_ERR();
		return bzero_nr_iffinished(&nr, RC_CONNBROKEN);
	}
	return rc;
}

int send_list_services_nonblock(int fd, struct libcor_nonblock_resumeinfo *nr,
		int flush)
{
	return send_cmd(fd, nr, CD_LIST_SERVICES | CD_CONTINUE_ON_ERROR_FLAG,
			0, 0, flush);
}

int send_list_services(int fd, int flush)
{
	int rc = RC_WOULDBLOCK;
	struct libcor_nonblock_resumeinfo nr;
	bzero(&nr, sizeof(struct libcor_nonblock_resumeinfo));

	rc = send_list_services_nonblock(fd, &nr, flush);
	if (unlikely(rc == RC_WOULDBLOCK)) {
		ASSERT_ERR();
		return bzero_nr_iffinished(&nr, RC_CONNBROKEN);
	}
	return rc;
}

static int _parse_service_list(char *buf, __u32 len,
		void *ptr,
		void (*init)(void *ptr, __u32 numservices),
		void (*next_service)(void *ptr, __be32 port))
{
	int rc;

	__u32 numservices;

	rc = decode_len(buf, len, &numservices);
	ASSERT(rc >= 0);
	ASSERT(rc <= len);
	if (unlikely(rc <= 0))
		return RC_CONNBROKEN;
	buf += rc;
	len -= rc;

	if (init != 0)
		init(ptr, numservices);

	for (;numservices > 0;numservices--) {
		__be32 port;

		if (unlikely(len < 4))
			return RC_CONNBROKEN;

		memcpy((char *) &port, buf, 4);

		buf += 2;
		len -= 2;

		if (next_service != 0)
			next_service(ptr, port);
	}

	return RC_OK;
}

int parse_service_list(char *buf, __u32 len,
		void *ptr,
		void (*init)(void *ptr, __u32 numservices),
		void (*next_service)(void *ptr, __be32 port))
{
	int rc;

	if (unlikely(_parse_service_list(buf, len, 0, 0, 0) != RC_OK))
		return RC_CONNBROKEN;

	rc = _parse_service_list(buf, len, ptr, init, next_service);
	ASSERT(rc == RC_OK);
	return rc;
}


int send_list_neigh_nonblock(int fd, struct libcor_nonblock_resumeinfo *nr,
		int flush)
{
	return send_cmd(fd, nr, CD_LIST_NEIGH | CD_CONTINUE_ON_ERROR_FLAG,
			0, 0, flush);
}

int send_list_neigh(int fd, int flush)
{
	int rc = RC_WOULDBLOCK;
	struct libcor_nonblock_resumeinfo nr;
	bzero(&nr, sizeof(struct libcor_nonblock_resumeinfo));

	rc = send_list_neigh_nonblock(fd, &nr, flush);
	if (unlikely(rc == RC_WOULDBLOCK)) {
		ASSERT_ERR();
		return bzero_nr_iffinished(&nr, RC_CONNBROKEN);
	}
	return rc;
}

static int field_present(struct listneigh_field *fields, __u32 numfields,
		__u16 field)
{
	__u64 u;
	for (u=0;u<numfields;u++) {
		if (fields[u].field == field)
			return 1;
	}
	return 0;
}

static int _parse_neigh_list(char *buf, __u32 len, __u32 default_latency_us,
		void *ptr,
		void (*init)(void *ptr, __u32 numneigh),
		void (*next_neigh)(void *ptr, __be64 addr, __u32 latency_us))
{
	int rc;
	__u32 u;

	__u32 numneighs;
	__u32 numfields;
	struct listneigh_field fields[NEIGHLIST_MAX_FIELDS];


	rc = decode_len(buf, len, &numneighs);
	ASSERT(rc >= 0);
	ASSERT(rc <= len);
	if (unlikely(rc <= 0))
		return RC_CONNBROKEN;
	buf += rc;
	len -= rc;


	rc = decode_len(buf, len, &numfields);
	ASSERT(rc >= 0);
	ASSERT(rc <= len);
	if (unlikely(rc <= 0))
		return RC_CONNBROKEN;
	buf += rc;
	len -= rc;


	if (unlikely(numfields > NEIGHLIST_MAX_FIELDS || numfields > 65536))
		return RC_CONNBROKEN;

	for (u=0;u<numfields;u++) {
		__u32 fieldlen;

		if (unlikely(len < 2))
			return RC_CONNBROKEN;

		memcpy((char *) &(fields[u].field), buf, 2);
		buf += 2;
		len -= 2;
		fields[u].field = ntohs(fields[u].field);


		rc = decode_len(buf, len, &fieldlen);
		ASSERT(rc >= 0);
		ASSERT(rc <= len);
		if (unlikely(rc <= 0))
			return RC_CONNBROKEN;
		buf += rc;
		len -= rc;

		if (unlikely(fieldlen > 65535))
			return RC_CONNBROKEN;
		fields[u].len = (__u16) fieldlen;
	}

	if (unlikely(field_present(fields, numfields,
			LIST_NEIGH_FIELD_ADDR) == 0))
		return RC_CONNBROKEN;

	if (init != 0)
		init(ptr, numneighs);

	for (;numneighs>0;numneighs--) {
		__be64 addr = 0;
		__u32 latency_us = 0;

		__u32 v;

		for (v=0;v<numfields;v++) {
			__u32 fieldlen = fields[v].len;
			if (fieldlen == 0) {
				rc = decode_len(buf, len, &fieldlen);
				ASSERT(rc >= 0);
				ASSERT(rc <= len);
				if (unlikely(rc <= 0))
					return RC_CONNBROKEN;
				buf += rc;
				len -= rc;

				if (unlikely(fieldlen > 65535))
					return RC_CONNBROKEN;
			}

			if (unlikely(fieldlen > len))
				return RC_CONNBROKEN;

			if (unlikely(field_present(fields, v,
					fields[v].field))) {
			} else if (fields[v].field == LIST_NEIGH_FIELD_ADDR) {
				if (unlikely(fieldlen != 8))
					return RC_CONNBROKEN;

				memcpy((char *) &addr, buf, 8);

				if (unlikely(ntohll(addr) == 0))
					return RC_CONNBROKEN;
			} else if (fields[v].field ==
					LIST_NEIGH_FIELD_LATENCY) {
				latency_us = cor_dec_log_64_11(*((__u8 *) buf));
			}

			buf += fieldlen;
			len -= fieldlen;
		}

		if (next_neigh != 0)
			next_neigh(ptr, addr, latency_us);
	}

	return RC_OK;
}

int parse_neigh_list(char *resp, __u32 len, __u32 default_latency_us,
		void *ptr,
		void (*init)(void *ptr, __u32 numneigh),
		void (*next_neigh)(void *ptr, __be64 addr, __u32 latency_us))
{
	int rc;

	if (unlikely(_parse_neigh_list(resp, len, 0, 0, 0, 0) != RC_OK))
		return RC_CONNBROKEN;

	rc = _parse_neigh_list(resp, len, default_latency_us,
			ptr, init, next_neigh);
	ASSERT(rc == RC_OK);
	return rc;
}


int pass_socket(int fd, __u64 cookie)
{
	int rc;

	rc = setsockopts_pass_on_close(fd, cookie);
	if (rc != 0) {
		perror("pass_socket");
		return RC_CONNBROKEN;
	}

	close(fd);

	return RC_OK;
}

static int send_rdsock_cmd(int fd, struct libcor_nonblock_resumeinfo *nr,
		__u32 cmd, char *data, __u32 datalen)
{
	__u32 be_cmd = htonl(cmd);
	__u32 be_datalen = htonl(datalen);

	if (unlikely(nr->type != RESUME_TYPE_NONE))
		ASSERT_ERR();

	bzero(nr, sizeof(struct libcor_nonblock_resumeinfo));
	nr->type = RESUME_TYPE_WRITE;
	nr->data.write.fd = fd;

	if (sizeof(nr->data.write.buf) != WRITE_BUF_SIZE)
		ASSERT_ERR();

	if (unlikely(datalen + 8 < datalen || WRITE_BUF_SIZE < (datalen + 8)))
		ASSERT_ERR();

	memcpy(&(nr->data.write.buf[0]), (char *) &be_cmd, 4);
	memcpy(&(nr->data.write.buf[4]), (char *) &be_datalen, 4);
	memcpy(&(nr->data.write.buf[8]), data, datalen);
	nr->data.write.len = datalen + 8;

	nr->data.write.flush = RESUME_WRITE_FLUSH_YES;

	return resume_send(fd, nr);
}

int send_rdsock_version_nonblock(int fd,
		struct libcor_nonblock_resumeinfo *nr,
		__u32 version)
{
	char data[4];

	version = htonl(version);

	memcpy(&(data[0]), (char *) &version, 4);

	return send_rdsock_cmd(fd, nr, CRD_UTK_VERSION, &(data[0]), 4);
}

int send_rdsock_version(int fd, __u32 version)
{
	int rc = RC_WOULDBLOCK;
	struct libcor_nonblock_resumeinfo nr;
	bzero(&nr, sizeof(struct libcor_nonblock_resumeinfo));

	rc = send_rdsock_version_nonblock(fd, &nr, version);
	if (unlikely(rc == RC_WOULDBLOCK)) {
		ASSERT_ERR();
		return bzero_nr_iffinished(&nr, RC_CONNBROKEN);
	}
	return rc;
}

static __u32 _send_rdsock_up_interfacelen(char **interfaces,
		__u32 num_interfaces)
{
	__u32 len = 0;
	__u32 i;

	for (i=0;i<num_interfaces;i++) {
		len += strlen(interfaces[i]);
	}

	return 4 + num_interfaces*4 + len;
}

static __u32 _send_rdsock_up_interfaces(char **interfaces,
		__u32 num_interfaces, char *buf, __u32 buflen)
{
	__u32 offset = 0;
	__u32 i;

	__u32 num_interfaces_be = htonl(num_interfaces);

	ASSERT(offset + 4 <= buflen);
	memcpy(buf + offset, (char *) &num_interfaces_be, 4);
	offset += 4;


	for (i=0;i<num_interfaces;i++) {
		__u32 len = strlen(interfaces[i]);
		__u32 len_be = htonl(len);

		ASSERT(offset + 4 <= buflen);
		memcpy(buf + offset, (char *) &len_be, 4);
		offset += 4;

		ASSERT(offset + len <= buflen);
		memcpy(buf + offset, interfaces[i], len);
		offset += len;
	}

	ASSERT(offset == buflen);

	return offset;
}


int send_rdsock_up_nonblock(int fd, struct libcor_nonblock_resumeinfo *nr,
		int has_addr, __be64 addr,
		char **interfaces, __u32 num_interfaces, int all_interfaces)
{
	int rc;

	__u32 interface_len = 0;
	__u32 interface_len2;

	char *buf;
	__u32 buflen;
	__u32 offset = 0;

	__u64 flags = 0;

	if (has_addr == 0 && addr != 0)
		return RC_CONNBROKEN;

	if (has_addr != 0)
		flags |= CRD_UTK_UP_FLAGS_ADDR;

	if (!all_interfaces) {
		flags |= CRD_UTK_UP_FLAGS_INTERFACES;

		interface_len = _send_rdsock_up_interfacelen(interfaces,
				num_interfaces);

		ASSERT(interface_len <= 1024*1024*1024);
	}

	flags = htonll(flags);

	buflen = 8 + 8 + interface_len;
	buf = malloc(8 + 8 + interface_len);

	memcpy(buf + offset, (char *) &flags, 8);
	offset += 8;

	if (has_addr != 0) {
		memcpy(buf + offset, (char *) &addr, 8);
		offset += 8;
	}

	if (!all_interfaces) {
		interface_len2 = _send_rdsock_up_interfaces(interfaces,
				num_interfaces,
				buf + offset, interface_len);
		ASSERT(interface_len == interface_len2);
		offset += interface_len2;
	}

	ASSERT(offset <= buflen);

	rc = send_rdsock_cmd(fd, nr, CRD_UTK_UP, buf, offset);

	free(buf);
	buf = 0;
	return rc;
}

int send_rdsock_up(int fd,
		int has_addr, __be64 addr,
		char **interfaces, __u32 num_interfaces, int all_interfaces)
{
	int rc = RC_WOULDBLOCK;
	struct libcor_nonblock_resumeinfo nr;
	bzero(&nr, sizeof(struct libcor_nonblock_resumeinfo));

	rc = send_rdsock_up_nonblock(fd, &nr, has_addr, addr,
			interfaces, num_interfaces, all_interfaces);
	if (unlikely(rc == RC_WOULDBLOCK)) {
		ASSERT_ERR();
		return bzero_nr_iffinished(&nr, RC_CONNBROKEN);
	}
	return rc;
}

int send_rdsock_connecterror_nonblock(int fd,
		struct libcor_nonblock_resumeinfo *nr,
		__u64 cookie, __u32 error)
{
	char data[12];

	error = htonl(error);

	memcpy(&(data[0]), (char *) &cookie, 8);
	memcpy(&(data[8]), (char *) &error, 4);

	return send_rdsock_cmd(fd, nr, CRD_UTK_CONNECTERROR, &(data[0]), 12);
}

int send_rdsock_connecterror(int fd, __u64 cookie, __u32 error)
{
	int rc = RC_WOULDBLOCK;
	struct libcor_nonblock_resumeinfo nr;
	bzero(&nr, sizeof(struct libcor_nonblock_resumeinfo));

	rc = send_rdsock_connecterror_nonblock(fd, &nr, cookie, error);
	if (unlikely(rc == RC_WOULDBLOCK)) {
		ASSERT_ERR();
		return bzero_nr_iffinished(&nr, RC_CONNBROKEN);
	}
	return rc;
}

int parse_rdsock_supported_versions(struct rdsock_cmd *cmd,
		__u32 *versionmin, __u32 *versionmax)
{
	if (cmd->cmddatalen != 8)
		return 1;

	memcpy((char *) versionmin, cmd->cmddata, 4);
	*versionmin = htonl(*versionmin);
	memcpy((char *) versionmax, cmd->cmddata + 4, 4);
	*versionmax = htonl(*versionmax);

	return 0;
}

int parse_rdsock_connect(void *ptr, struct rdsock_cmd *cmd,
		int (*proc_connect)(void *ptr, __u64 cookie,
		struct cor_sockaddr *addr, __u32 tos))
{
	__u64 cookie;
	struct cor_sockaddr addr;
	__u32 tos;

	if ((sizeof(struct cor_sockaddr) + 12) != cmd->cmddatalen)
		return 1;

	memcpy((char *) &cookie, cmd->cmddata, 8);

	memcpy((char *) &addr, cmd->cmddata + 8, sizeof(struct cor_sockaddr));

	memcpy((char *) &tos, cmd->cmddata + 8 + sizeof(struct cor_sockaddr),
			4);
	tos = htonl(tos);

	return proc_connect(ptr, cookie, &addr, tos);
}

int read_rdsock_cmd_nonblock(int fd,
		struct libcor_nonblock_resumeinfo *nr,
		struct rdsock_cmd *cmd)
{
	int rc;

	struct libcor_nonblock_resumeinfo_rdsockcmd *nr_rd =
			&(nr->data.read.funcdata.rdsock_cmd);

	bzero(cmd, sizeof(struct rdsock_cmd));

	if (nr->type == RESUME_TYPE_NONE) {
		bzero(nr, sizeof(struct libcor_nonblock_resumeinfo));
		nr->type = RESUME_TYPE_READ;
		nr->data.read.functype = RESUME_READ_FUNC_RDSOCK_CMD;
		nr_rd->fd = fd;
	} else if (unlikely(nr->type != RESUME_TYPE_READ ||
			nr->data.read.functype != RESUME_READ_FUNC_RDSOCK_CMD)){
		ASSERT_ERR();
	} else if (unlikely(nr_rd->fd != fd)) {
		ASSERT_ERR();
	} else {
		__u8 state = nr_rd->state;

		if (state == 1) {
			goto state_1;
		} else if (state == 2) {
			goto state_2;
		} else if (unlikely(state != 0)) {
			ASSERT_ERR();
		}
	}

	if (sizeof(nr_rd->buf) != 8)
		ASSERT_ERR();

	rc = read_fully(fd, nr, (char *) &(nr_rd->buf[0]), 8, 0);
	if (rc != RC_OK)
		return bzero_nr_iffinished(nr, rc);

	((char *) &(nr_rd->cmd))[0] = nr_rd->buf[0];
	((char *) &(nr_rd->cmd))[1] = nr_rd->buf[1];
	((char *) &(nr_rd->cmd))[2] = nr_rd->buf[2];
	((char *) &(nr_rd->cmd))[3] = nr_rd->buf[3];
	((char *) &(nr_rd->cmddatalen))[0] = nr_rd->buf[4];
	((char *) &(nr_rd->cmddatalen))[1] = nr_rd->buf[5];
	((char *) &(nr_rd->cmddatalen))[2] = nr_rd->buf[6];
	((char *) &(nr_rd->cmddatalen))[3] = nr_rd->buf[7];

	nr_rd->cmd = ntohl(nr_rd->cmd);
	nr_rd->cmddatalen = ntohl(nr_rd->cmddatalen);

	if (nr_rd->cmddatalen > 65536)
		goto discard;

	nr_rd->cmddata = malloc(nr_rd->cmddatalen);
	bzero(nr_rd->cmddata, nr_rd->cmddatalen);

	nr_rd->state = 1;
state_1:
	rc = read_fully(fd, nr, nr_rd->cmddata, nr_rd->cmddatalen, 0);
	if (rc != RC_OK) {
		if (rc != RC_WOULDBLOCK) {
			free(nr_rd->cmddata);
			nr_rd->cmddata = 0;
		}
		return bzero_nr_iffinished(nr, rc);
	}

	cmd->cmd = nr_rd->cmd;
	cmd->cmddata = nr_rd->cmddata;
	cmd->cmddatalen = nr_rd->cmddatalen;

	if (0) {
discard:
		nr_rd->state = 2;
state_2:
		rc = read_discard(fd, nr, nr_rd->cmddatalen);
		if (rc != RC_OK)
			return bzero_nr_iffinished(nr, rc);
	}
	return bzero_nr_iffinished(nr, rc);
}

int read_rdsock_cmd(int fd, struct rdsock_cmd *cmd)
{
	int rc = RC_WOULDBLOCK;
	struct libcor_nonblock_resumeinfo nr;
	bzero(&nr, sizeof(struct libcor_nonblock_resumeinfo));

	rc = read_rdsock_cmd_nonblock(fd, &nr, cmd);
	if (unlikely(rc == RC_WOULDBLOCK)) {
		ASSERT_ERR();
		return bzero_nr_iffinished(&nr, RC_CONNBROKEN);
	}
	return rc;
}
