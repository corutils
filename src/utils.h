#include <linux/types.h>
#include <stdlib.h>
#include <assert.h>

/**
 *	Connection oriented routing user space utils
 *	Copyright (C) 2009-2019
 *	Authors:
 * 		Michael Blizek
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License
 *	as published by the Free Software Foundation; either version 2
 *	of the License, or (at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 *	02110-1301, USA.
 */

#define likely(x) __builtin_expect((x), 1)
#define unlikely(x) __builtin_expect((x), 0)

#define ASSERT_ERR()	\
	do {					\
		assert(0);			\
		exit(1);			\
		while (1) {			\
		}				\
	} while(0)				\


#define ASSERT(a)	\
	do {					\
		if (unlikely(!(a))) { ASSERT_ERR(); }	\
	} while(0)


static __u64 htonll(__u64 in)
{
	__u64 out = 0;
	char *p_out = (char *) &out;

	p_out[7] = in & 255;
	in = in >> 8;
	p_out[6] = in & 255;
	in = in >> 8;
	p_out[5] = in & 255;
	in = in >> 8;
	p_out[4] = in & 255;
	in = in >> 8;
	p_out[3] = in & 255;
	in = in >> 8;
	p_out[2] = in & 255;
	in = in >> 8;
	p_out[1] = in & 255;
	in = in >> 8;
	p_out[0] = in & 255;

	return out;
}

static __u64 ntohll(__u64 in)
{
	__u64 out = 0;
	char *p_in = (char *) &in;

	out += p_in[7];
	out << 8;
	out += p_in[6];
	out << 8;
	out += p_in[5];
	out << 8;
	out += p_in[4];
	out << 8;
	out += p_in[3];
	out << 8;
	out += p_in[2];
	out << 8;
	out += p_in[1];
	out << 8;
	out += p_in[0];

	return out;
}

extern int parse_cor_addr(__be64 *addr, char *saddr, char terminator);
